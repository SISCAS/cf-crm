<?php
/**
 * CRM - Contact
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Contact class
  */
 class cCrmContact{

  /** Properties */
  protected $id;
  protected $fkCustomer;
  protected $fkDivision;
  protected $firstname;
  protected $lastname;
  protected $role;
  protected $mail;
  protected $phone;
  protected $note;
  protected $acquisition;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  /**
   * Contact class
   *
   * @param integer $contact Contact object or ID
   * @return boolean
   */
  public function __construct($contact){
   // get object
   if(is_numeric($contact)){$contact=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers__contacts` WHERE `id`='".$contact."'");}
   if(!$contact->id){return false;}
   // set properties
   $this->id=(int)$contact->id;
   $this->fkCustomer=(int)$contact->fkCustomer;
   $this->fkDivision=(int)$contact->fkDivision;
   $this->firstname=stripslashes($contact->firstname);
   $this->lastname=stripslashes($contact->lastname);
   $this->role=stripslashes($contact->role);
   $this->mail=stripslashes($contact->mail);
   $this->phone=stripslashes($contact->phone);
   $this->note=stripslashes($contact->note);
   $this->acquisition=$contact->acquisition;
   $this->addTimestamp=(int)$contact->addTimestamp;
   $this->addFkUser=(int)$contact->addFkUser;
   $this->updTimestamp=(int)$contact->updTimestamp;
   $this->updFkUser=(int)$contact->updFkUser;
   $this->deleted=(int)$contact->deleted;
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

  /**
   * Get full name
   */
  public function getFullname(){return $this->lastname." ".$this->firstname;}

 }

?>