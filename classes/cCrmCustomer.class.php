<?php
/**
 * CRM - Customer
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * CRM Customer class
 */
class cCrmCustomer{

	/** Properties */
	protected $id;
	protected $code;
	protected $name;
	protected $fiscalName;
	protected $address;
	protected $latitude;
	protected $longitude;
	protected $classification;
	protected $potentialQuantity;
	protected $note;
	protected $addTimestamp;
	protected $addFkUser;
	protected $updTimestamp;
	protected $updFkUser;
	protected $deleted;

	/**
	 * Customer class
	 *
	 * @param integer $customer Customer object or ID
	 * @param integer $code Customer code
	 * @return boolean
	 */
	public function __construct($customer,$code=null){
		// get object
		if(strlen($code)){$customer=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers` WHERE `code`='".str_pad($code,10,"0",STR_PAD_LEFT)."'");}
		if(is_numeric($customer)){$customer=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers` WHERE `id`='".$customer."'");}
		if(!$customer->id){return false;}
		// set properties
		$this->id=(int)$customer->id;
		$this->code=stripslashes($customer->code);
		$this->name=stripslashes($customer->name);
		$this->fiscalName=stripslashes($customer->fiscalName);
		$this->address=stripslashes($customer->address);
		$this->latitude=(double)(string)$customer->latitude;
		$this->longitude=(double)$customer->longitude;
		$this->classification=stripslashes($customer->classification);
		$this->potentialQuantity=(int)$customer->potentialQuantity;
		$this->note=stripslashes($customer->note);
		$this->addTimestamp=(int)$customer->addTimestamp;
		$this->addFkUser=(int)$customer->addFkUser;
		$this->updTimestamp=(int)$customer->updTimestamp;
		$this->updFkUser=(int)$customer->updFkUser;
		$this->deleted=(int)$customer->deleted;
	}

	/**
	 * Get
	 *
	 * @param string $property Property name
	 * @return string Property value
	 */
	public function __get($property){return $this->$property;}

	/**
	 * Get Events
	 */
	public function getEvents($limit=null){
		// definitions
		$events_array=array();
		// make query where
		if(DEBUG){$query_where="1";}else{$query_where="`level`<>'debug'";}
		// get customer events
		$events_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__events` WHERE ".$query_where." AND `fkCustomer`='".$this->id."' ORDER BY `timestamp` DESC".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($events_results as $event){$events_array[$event->id]=new cEvent($event,"crm");}
		// return
		return $events_array;
	}

	/**
	 * Get Tags
	 */
	public function getTags(){
		// definitions
		$tags_array=array();
		// make query where
		$query_where="WHERE `fkCustomer`='".$this->id."'";
		// get customer tags
		$tags_results=$GLOBALS['database']->queryObjects("SELECT DISTINCT(tag) as tag FROM `crm__customers__tags` ".$query_where." ORDER BY `tag`");
		foreach($tags_results as $tag){$tags_array[$tag->tag]=$tag->tag;}
		// return
		return $tags_array;
	}

	/**
	 * Get Contacts
	 */
	public function getContacts($limit=null){
		// definitions
		$contacts_array=array();
		// make query where
		$query_where="WHERE `fkCustomer`='".$this->id."' ".api_crm_authorizedDivisionsQuery();
		// get customer contacts
		$contacts_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__contacts` ".$query_where." ORDER BY `lastname`,`firstname`".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($contacts_results as $contact){$contacts_array[$contact->id]=new cCrmContact($contact);}
		// return
		return $contacts_array;
	}

	/**
	 * Get Relationships
	 */
	public function getRelationships($limit=null){
		// definitions
		$relationships_array=array();
		// make query where
		$query_where="WHERE `fkCustomer`='".$this->id."' ".api_crm_authorizedDivisionsQuery();
		// get customer relationships
		$relationships_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__relationships` ".$query_where." ORDER BY `timestamp` DESC".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($relationships_results as $relationship){$relationships_array[$relationship->id]=new cCrmRelationship($relationship);}
		// return
		return $relationships_array;
	}

	/**
	 * Get Attachments
	 */
	public function getAttachments(){
		// definitions
		$attachments_array=array();
		// get attachments
		$attachments_results=$GLOBALS['database']->queryObjects("SELECT `crm__customers__attachments`.* FROM `crm__customers__attachments` LEFT JOIN `framework__attachments` ON `framework__attachments`.`id`=`crm__customers__attachments`.`fkAttachment` WHERE `crm__customers__attachments`.`fkCustomer`='".$this->id."' ORDER BY `framework__attachments`.`name` ASC");
		// cycle all results
		foreach($attachments_results as $result_f){
			// get attachment
			$attachment_obj=new cAttachment($result_f->fkAttachment);
			// add attachment to attachments array
			if($attachment_obj->id){$attachments_array[$attachment_obj->id]=$attachment_obj;}
		}
		// return
		return $attachments_array;
	}

	/**
	 * Get Specifications
	 */
	public function getSpecifications($limit=null){
		// definitions
		$specifications_array=array();
		// get customer specifications
		$specifications_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__specifications` WHERE `fkCustomer`='".$this->id."' ORDER BY `code` ASC".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($specifications_results as $specification){$specifications_array[$specification->id]=new cCrmSpecification($specification);}
		// return
		return $specifications_array;
	}

	/**
	 * Get Products
	 */
	public function getProducts($limit=null){
		// definitions
		$products_array=array();
		// make query where
		$query_where="WHERE `fkCustomer`='".$this->id."' ".api_crm_authorizedDivisionsQuery();
		// get customer products
		$products_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__products` ".$query_where." ORDER BY `quantity` DESC,`grade` ASC,`execution` ASC".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($products_results as $product){$products_array[$product->id]=new cCrmProduct($product);}
		// return
		return $products_array;
	}

	/**
	 * Get Offers
	 *
	 * @param boolean $activeOnly Offers not expired and not ordered
	 * @param integer $limit Limit query results
	 * @return array of offerts
	 */
	public function getOffers($activeOnly=false,$limit=null){
		// definitions
		$offers_array=array();
		$today_range=api_timestamp_dayRange(time());
		$yesterday_date=api_date(strtotime("-1 day"));
		// make query where
		$query_where="WHERE `fkCustomer`='".$this->id."'";
		if($activeOnly){
			$query_where.="\n AND (";
			$query_where.="\n  `status`='inserted'"; // inserted offers (not expired and ordered)
			$query_where.="\n  OR ( `updTimestamp` BETWEEN '".implode("' AND '",$today_range)."' )"; // updated today
			$query_where.="\n  OR ( `status`='expired' AND `expirationDate`>='".$yesterday_date."' )"; // expired yesterday
			$query_where.="\n )";
		}
		$query_where.=api_crm_authorizedDivisionsQuery();
		// debug
		//api_dump($query_where);
		// get customer offers
		$offers_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers__offers` ".$query_where." ORDER BY `id` ASC".(is_int($limit)?" LIMIT 0,".$limit:null));
		foreach($offers_results as $offer){$offers_array[$offer->id]=new cCrmOffer($offer);}
		// return
		return $offers_array;
	}

}

?>