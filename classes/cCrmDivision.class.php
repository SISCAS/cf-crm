<?php
/**
 * CRM - Division
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Division class
  */
 class cCrmDivision{

  /** Properties */
  protected $id;
  protected $name;
  protected $description;
  protected $company;
  protected $fiscal_name;
  protected $fiscal_address;
  protected $fiscal_code;
  protected $fiscal_vat;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  protected $label;

  /**
   * Division class
   *
   * @param integer $division Division object or ID
   * @return boolean
   */
  public function __construct($division){
   // get object
   if(is_numeric($division)){$division=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__divisions` WHERE `id`='".$division."'");}
   if(!$division->id){return false;}
   // set properties
   $this->id=(int)$division->id;
   $this->name=stripslashes($division->name);
   $this->description=stripslashes($division->description);
   $this->company=stripslashes($division->company);
   $this->fiscal_name=stripslashes($division->fiscal_name);
   $this->fiscal_address=stripslashes($division->fiscal_address);
   $this->fiscal_code=stripslashes($division->fiscal_code);
   $this->fiscal_vat=stripslashes($division->fiscal_vat);
   $this->addTimestamp=(int)$division->addTimestamp;
   $this->addFkUser=(int)$division->addFkUser;
   $this->updTimestamp=(int)$division->updTimestamp;
   $this->updFkUser=(int)$division->updFkUser;
   $this->deleted=(bool)$division->deleted;
   // make label
   $this->label=$this->name;
   if($this->description){$this->label.=" (".$this->description.")";}
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

  /**
   * Get Events
   */
  public function getEvents($debug=false){
   // definitions
   $events_array=array();
   // make query where
   if(DEBUG || $debug){$query_where="1";}else{$query_where="`level`<>'debug'";}
   // get customer events
   $events_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__divisions__events` WHERE ".$query_where." AND `fkDivision`='".$this->id."' ORDER BY `timestamp` DESC");
   foreach($events_results as $event){$events_array[$event->id]=new cEvent($event,"crm");}
   // return
   return $events_array;
  }

  /**
   * Get Salesareas
   */
  public function getSalesareas(){
   // definitions
   $salesareas_array=array();
   // get customer salesareas
   $salesareas_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__divisions__salesareas` WHERE `fkDivision`='".$this->id."' ORDER BY `name` ASC");
   foreach($salesareas_results as $salesarea){$salesareas_array[$salesarea->id]=new cCrmSalesarea($salesarea);}
   // return
   return $salesareas_array;
  }

  /**
   * Get Warehouses
   */
  public function getWarehouses(){
   // definitions
   $warehouses_array=array();
   // get customer warehouses
   $warehouses_results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__divisions__warehouses` WHERE `fkDivision`='".$this->id."' ORDER BY `name` ASC");
   foreach($warehouses_results as $warehouse){$warehouses_array[$warehouse->id]=new cCrmWarehouse($warehouse);}
   // return
   return $warehouses_array;
  }

  /**
   * Get Authorizations
   */
  public function getAuthorizations(){
   // definitions
   $authorizations_array=array();
   // get authorizations
   $authorizations_results=$GLOBALS['database']->queryObjects("SELECT `crm__divisions__authorizations`.* FROM `crm__divisions__authorizations` LEFT JOIN `framework__groups` ON `framework__groups`.`id`=`crm__divisions__authorizations`.`fkGroup` WHERE `crm__divisions__authorizations`.`fkDivision`='".$this->id."' ORDER BY `framework__groups`.`name` ASC");
   // cycle all authorizations
   foreach($authorizations_results as $result_f){
    // build authorization
    $authorization_obj=new stdClass();
    $authorization_obj->fkGroup=$result_f->fkGroup;
    $authorization_obj->level=$result_f->level;
    // add authorization to authorizations array
    $authorizations_array[]=$authorization_obj;
   }
   // return
   return $authorizations_array;
  }

  /**
   * Check Authorizations for current user
   *
   * @return boolean
   */
  public function checkAuthorizations(){
   // cycle all authorized groups
   foreach($this->getAuthorizations() as $authorization_fobj){
    // check if current user is authorized
    if(array_key_exists($authorization_fobj->fkGroup,$GLOBALS['session']->user->getAssignedGroups())){
     // check for current user level
     if($GLOBALS['session']->user->level<=$authorization_fobj->level){
      // current user is authorized
      return true;
     }
    }
   }
   // unauthorized
   return false;
  }

 }

?>