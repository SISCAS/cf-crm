<?php
/**
 * CRM - Offer
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Offer class
  */
 class cCrmOffer{

  /** Properties */
  protected $id;
  protected $fkCustomer;
  protected $fkDivision;
  protected $fkSalesarea;
  protected $timestamp;  /** @todo serve? */
  protected $customer_name;
  protected $request_number;
  protected $request_date;
  protected $request_material;
  protected $request_grade;
  protected $request_wn;
  protected $material;
  protected $grade;
  protected $execution;
  protected $profile;
  protected $heattreath;
  protected $tollerance;
  protected $size;
  protected $length;
  protected $length_cutted;
  protected $length_tollerance;
  protected $quantity_pieces;
  protected $quantity_weight;
  protected $price_list;
  protected $price_offer;
  protected $price_alloy;
  protected $price_cutting;
  protected $availability;
  protected $expirationDate;
  protected $important;
  protected $salesorder_number;
  protected $salesorder_position;
  protected $confirmation;
  protected $note;
  protected $texts;
  protected $status;
  protected $issue;
  protected $addTimestamp;
  protected $addFkUser;
	protected $updTimestamp;
	protected $updFkUser;
	protected $cnfTimestamp;
	protected $cnfFkUser;
  protected $deleted;

  /**
   * Offer class
   *
   * @param integer $offer Offer object or ID
   * @return boolean
   */
  public function __construct($offer){
   // get object
   if(is_numeric($offer)){$offer=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers__offers` WHERE `id`='".$offer."'");}
   if(!$offer->id){return false;}
   // set properties
   $this->id=(int)$offer->id;
   $this->fkCustomer=(int)$offer->fkCustomer;
   $this->fkDivision=(int)$offer->fkDivision;
   $this->fkSalesarea=(int)$offer->fkSalesarea;
   $this->timestamp=(int)$offer->timestamp;
   $this->customer_name=stripslashes($offer->customer_name);
   $this->request_number=stripslashes($offer->request_number);
   $this->request_date=$offer->request_date;
   $this->request_material=stripslashes($offer->request_material);
   $this->request_grade=stripslashes($offer->request_grade);
   $this->request_wn=stripslashes($offer->request_wn);
   $this->material=stripslashes($offer->material);
   $this->grade=stripslashes($offer->grade);
   $this->execution=stripslashes($offer->execution);
   $this->profile=stripslashes($offer->profile);
   $this->heattreath=stripslashes($offer->heattreath);
   $this->tollerance=stripslashes($offer->tollerance);
   $this->size=(double)$offer->size;
   $this->length=stripslashes($offer->length);
   $this->length_cutted=(double)$offer->length_cutted;
   $this->length_tollerance=stripslashes($offer->length_tollerance);
   $this->quantity_pieces=(double)$offer->quantity_pieces;
   $this->quantity_weight=(double)$offer->quantity_weight;
   $this->price_list=(double)$offer->price_list;
   $this->price_offer=(double)$offer->price_offer;
   $this->price_alloy=(boolean)$offer->price_alloy;
   $this->price_cutting=(double)$offer->price_cutting;
   $this->availability=stripslashes($offer->availability);
   $this->expirationDate=$offer->expirationDate;
   $this->important=(boolean)$offer->important;
   $this->salesorder_number=stripslashes($offer->salesorder_number);
   $this->salesorder_position=stripslashes($offer->salesorder_position);
   $this->note=stripslashes($offer->note);
   $this->texts=stripslashes($offer->texts);
   $this->status=stripslashes($offer->status);
   $this->issue=stripslashes($offer->issue);
   $this->addTimestamp=(int)$offer->addTimestamp;
   $this->addFkUser=(int)$offer->addFkUser;
	 $this->updTimestamp=(int)$offer->updTimestamp;
	 $this->updFkUser=(int)$offer->updFkUser;
	 $this->cnfTimestamp=(int)$offer->cnfTimestamp;
	 $this->cnfFkUser=(int)$offer->cnfFkUser;
   $this->deleted=(int)$offer->deleted;
   // make confirmation
   if(!$this->salesorder_number){$this->confirmation=null;}
   else{$this->confirmation=api_tag("samp",ltrim($this->salesorder_number,"0")."-".ltrim($this->salesorder_position,"0"));}
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

  /**
   * Get Status
   *
   * @param boolean $showIcon Return icon
   * @param boolean $showText Return text
   * @return string Status icon and/or text
   */
  public function getStatus($showIcon=true,$showText=true){
   // get status
   $status=api_crm_offers_availableStatus()[$this->status];
   // make return
   if($showIcon){$return=$status->icon;}
   if($showText){$return=$status->text;}
   if($showIcon && $showText){$return=$status->icon." ".$status->text;}
   // return
   return $return;
  }

  /**
   * Get Issue
   *
   * @param boolean $showIcon Return icon
   * @param boolean $showText Return text
   * @return string Issue icon and/or text
   */
  public function getIssue($showIcon=true,$showText=true){
   // get issue
   $issue=api_crm_offers_availableIssues()[$this->issue];
   // make return
   if($showIcon){$return=$issue->icon;}
   if($showText){$return=$issue->text;}
   if($showIcon && $showText){$return=$issue->icon." ".$issue->text;}
   // return
   return $return;
  }

 }

?>