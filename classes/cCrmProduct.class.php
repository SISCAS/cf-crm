<?php
/**
 * CRM - Product
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Product class
  */
 class cCrmProduct{

  /** Properties */
  protected $id;
  protected $fkCustomer;
  protected $fkDivision;
  protected $grade;
  protected $execution;
  protected $profile;
  protected $size;
  protected $quantity;
  protected $addTimestamp;
  protected $addFkUser;
	protected $updTimestamp;
	protected $updFkUser;
  protected $deleted;

  /**
   * Product class
   *
   * @param integer $product Product object or ID
   * @return boolean
   */
  public function __construct($product){
   // get object
   if(is_numeric($product)){$product=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers__products` WHERE `id`='".$product."'");}
   if(!$product->id){return false;}
   // set properties
   $this->id=(int)$product->id;
   $this->fkCustomer=(int)$product->fkCustomer;
   $this->fkDivision=(int)$product->fkDivision;
   $this->grade=stripslashes($product->grade);
   $this->execution=stripslashes($product->execution);
   $this->profile=stripslashes($product->profile);
   $this->size=(double)$product->size;
   $this->quantity=(double)$product->quantity;
   $this->addTimestamp=(int)$product->addTimestamp;
   $this->addFkUser=(int)$product->addFkUser;
	 $this->updTimestamp=(int)$product->updTimestamp;
	 $this->updFkUser=(int)$product->updFkUser;
	 $this->cnfTimestamp=(int)$product->cnfTimestamp;
	 $this->cnfFkUser=(int)$product->cnfFkUser;
   $this->deleted=(int)$product->deleted;
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

 }

?>