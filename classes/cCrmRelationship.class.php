<?php
/**
 * CRM - Relationship
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Relationship class
  */
 class cCrmRelationship{

  /** Properties */
  protected $id;
  protected $fkCustomer;
  protected $fkDivision;
  protected $timestamp;
  protected $typology;
  protected $contacts;
  protected $description;
  protected $feeling;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  /**
   * Relationship class
   *
   * @param integer $relationship Relationship object or ID
   * @return boolean
   */
  public function __construct($relationship){
   // get object
   if(is_numeric($relationship)){$relationship=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers__relationships` WHERE `id`='".$relationship."'");}
   if(!$relationship->id){return false;}
   // set properties
   $this->id=(int)$relationship->id;
   $this->fkCustomer=(int)$relationship->fkCustomer;
   $this->fkDivision=(int)$relationship->fkDivision;
   $this->timestamp=(int)$relationship->timestamp;
   $this->typology=stripslashes($relationship->typology);
   $this->contacts=stripslashes($relationship->contacts);
   $this->description=stripslashes($relationship->description);
   $this->feeling=stripslashes($relationship->feeling);
   $this->addTimestamp=(int)$relationship->addTimestamp;
   $this->addFkUser=(int)$relationship->addFkUser;
   $this->updTimestamp=(int)$relationship->updTimestamp;
   $this->updFkUser=(int)$relationship->updFkUser;
   $this->deleted=(int)$relationship->deleted;
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

  /**
   * Get Typology
   *
   * @param boolean $showIcon Return icon
   * @param boolean $showText Return text
   * @return string Typology icon and/or text
   */
  public function getTypology($showIcon=true,$showText=true){
   // get typology
   $typology=api_crm_relationships_availableTypologies()[$this->typology];
   // make return
   if($showIcon){$return=$typology->icon;}
   if($showText){$return=$typology->text;}
   if($showIcon && $showText){$return=$typology->icon." ".$typology->text;}
   // return
   return $return;
  }

  /**
   * Get Feeling
   *
   * @param boolean $showIcon Return icon
   * @param boolean $showText Return text
   * @return string Feeling icon and/or text
   */
  public function getFeeling($showIcon=true,$showText=true){
   // get feeling
   $feeling=api_crm_relationships_availableFeelings()[$this->feeling];
   // make return
   if($showIcon){$return=$feeling->icon;}
   if($showText){$return=$feeling->text;}
   if($showIcon && $showText){$return=$feeling->icon." ".$feeling->text;}
   // return
   return $return;
  }

 }

?>