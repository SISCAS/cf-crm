<?php
/**
 * CRM - Salesarea
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Salesarea class
  */
 class cCrmSalesarea{

  /** Properties */
  protected $id;
  protected $fkDivision;
  protected $code;
  protected $name;
  protected $description;
  protected $label;
  protected $orderTypology;
  protected $commercialOrganization;
  protected $distributionChannel;
  protected $productSector;
  protected $salesOffice;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  /**
   * Salesarea class
   *
   * @param integer $salesarea Salesarea object or ID
   * @return boolean
   */
  public function __construct($salesarea){
   // get object
   if(is_numeric($salesarea)){$salesarea=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__divisions__salesareas` WHERE `id`='".$salesarea."'");}
   if(!$salesarea->id){return false;}
   // set properties
   $this->id=(int)$salesarea->id;
   $this->fkDivision=(int)$salesarea->fkDivision;
   $this->name=stripslashes($salesarea->name);
   $this->description=stripslashes($salesarea->description);
   $this->orderTypology=stripslashes($salesarea->orderTypology);
   $this->commercialOrganization=stripslashes($salesarea->commercialOrganization);
   $this->distributionChannel=stripslashes($salesarea->distributionChannel);
   $this->productSector=stripslashes($salesarea->productSector);
   $this->salesOffice=stripslashes($salesarea->salesOffice);
   $this->addTimestamp=(int)$salesarea->addTimestamp;
   $this->addFkUser=(int)$salesarea->addFkUser;
   $this->updTimestamp=(int)$salesarea->updTimestamp;
   $this->updFkUser=(int)$salesarea->updFkUser;
   $this->deleted=(int)$salesarea->deleted;
   // make code
   $this->code=implode("|",array($this->orderTypology,$this->commercialOrganization,$this->distributionChannel,$this->productSector,$this->salesOffice));
   // make label
   $this->label=$this->name;
   if($this->description){$this->label.=" (".$this->description.")";}
   $this->label.=" [".$this->code."]";
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

 }

?>