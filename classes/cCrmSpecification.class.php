<?php
/**
 * CRM - Specification
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Specification class
  */
 class cCrmSpecification{

  /** Properties */
  protected $id;
  protected $fkCustomer;
  protected $code;
  protected $description;
  protected $date;
  protected $compatibility;
  protected $note;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  /**
   * Specification class
   *
   * @param integer $specification Specification object or ID
   * @return boolean
   */
  public function __construct($specification){
   // get object
   if(is_numeric($specification)){$specification=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__customers__specifications` WHERE `id`='".$specification."'");}
   if(!$specification->id){return false;}
   // set properties
   $this->id=(int)$specification->id;
   $this->fkCustomer=(int)$specification->fkCustomer;
   $this->code=stripslashes($specification->code);
   $this->description=stripslashes($specification->description);
   $this->date=stripslashes($specification->date);
   $this->compatibility=stripslashes($specification->compatibility);
   $this->note=stripslashes($specification->note);
   $this->addTimestamp=(int)$specification->addTimestamp;
   $this->addFkUser=(int)$specification->addFkUser;
   $this->updTimestamp=(int)$specification->updTimestamp;
   $this->updFkUser=(int)$specification->updFkUser;
   $this->deleted=(int)$specification->deleted;
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

  /**
   * Get Attachments
   */
  public function getAttachments(){
   // definitions
   $attachments_array=array();
   // get attachments
   $attachments_results=$GLOBALS['database']->queryObjects("SELECT `crm__customers__specifications__attachments`.* FROM `crm__customers__specifications__attachments` LEFT JOIN `framework__attachments` ON `framework__attachments`.`id`=`crm__customers__specifications__attachments`.`fkAttachment` WHERE `crm__customers__specifications__attachments`.`fkSpecification`='".$this->id."' ORDER BY `framework__attachments`.`name` ASC");
   // cycle all results
   foreach($attachments_results as $result_f){
    // get attachment
    $attachment_obj=new cAttachment($result_f->fkAttachment);
    // add attachment to attachments array
    if($attachment_obj->id){$attachments_array[$attachment_obj->id]=$attachment_obj;}
   }
   // return
   return $attachments_array;
  }

  /**
   * Get Compatibility
   *
   * @param boolean $showIcon Return icon
   * @param boolean $showText Return text
   * @return string Typology icon and/or text
   */
  public function getCompatibility($showIcon=true,$showText=true){
   // get result
   $compatibility=api_crm_specifications_availableCompatibilities()[$this->compatibility];
   // make return
   if($showIcon){$return=$compatibility->icon;}
   if($showText){$return=$compatibility->text;}
   if($showIcon && $showText){$return=$compatibility->icon." ".$compatibility->text;}
   // return
   return $return;
  }

 }

?>