<?php
/**
 * CRM - Warehouse
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 /**
  * CRM Warehouse class
  */
 class cCrmWarehouse{

  /** Properties */
  protected $id;
  protected $name;
  protected $description;
  protected $label;
  protected $codes_array;
  protected $addTimestamp;
  protected $addFkUser;
  protected $updTimestamp;
  protected $updFkUser;
  protected $deleted;

  /**
   * Warehouse class
   *
   * @param integer $warehouse Warehouse object or ID
   * @return boolean
   */
  public function __construct($warehouse){
   // get object
   if(is_numeric($warehouse)){$warehouse=$GLOBALS['database']->queryUniqueObject("SELECT * FROM `crm__divisions__warehouses` WHERE `id`='".$warehouse."'");}
   if(!$warehouse->id){return false;}
   // set properties
   $this->id=(int)$warehouse->id;
   $this->name=stripslashes($warehouse->name);
   $this->description=stripslashes($warehouse->description);
   $this->codes_array=json_decode($warehouse->codes_json);
   $this->addTimestamp=(int)$warehouse->addTimestamp;
   $this->addFkUser=(int)$warehouse->addFkUser;
   $this->updTimestamp=(int)$warehouse->updTimestamp;
   $this->updFkUser=(int)$warehouse->updFkUser;
   $this->deleted=(bool)$warehouse->deleted;
   // make label
   $this->label=$this->name;
   if($this->description){$this->label.=" (".$this->description.")";}
  }

  /**
   * Get
   *
   * @param string $property Property name
   * @return string Property value
   */
  public function __get($property){return $this->$property;}

 }

?>