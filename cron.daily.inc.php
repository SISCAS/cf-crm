<?php
/**
 * CRM - Cron
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // definitions
 $logs=array();
 // stop manual expired plannings
 $updated_rows=$GLOBALS['database']->queryExecute("UPDATE `crm__customers__offers` SET `status`='expired' WHERE `status`='inserted' AND `expirationDate`<'".api_date()."'");
 // log
 $logs[]=intval($updated_rows)." expired offers checked";
 // debug
 api_dump($logs,"cmr");

?>