<?php
/**
 * CRM - Customers Edit
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 */

// check authorization
api_checkAuthorization("crm-customers_manage","dashboard");
// get objects
$customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
// include module template
require_once(MODULE_PATH."template.inc.php");
// set application title
$app->setTitle(($customer_obj->id?api_text("customers_edit",$customer_obj->name):api_text("customers_edit-add")));
// check for customer and code
if(!$customer_obj->id && $_REQUEST['code']){
	// try to get customer from code
	$customer_obj=new cCrmCustomer(null,$_REQUEST['code']);
	// check for customer
	if(!$customer_obj->id){
		// try to get customer from sap
		$customer_obj=api_crm_rfc_customer_informations($_REQUEST['code']);
		$customer_obj->fiscalName=$customer_obj->name;
	}
}
// import alert
if(!$customer_obj->id){
	api_alerts_add(api_text("customers_edit-alert-import"),"warning");
	api_alerts_add(api_text("customers_edit-alert-new"),"info");
}
// build customer form
$form=new strForm("?mod=".MODULE."&scr=submit&act=customer_save&idCustomer=".$customer_obj->id."&return_scr=".$_REQUEST['return_scr'],"POST",null,null,"customers_edit");
$form->addField("text","code",api_text("customers_edit-ff-code"),$customer_obj->code,api_text("customers_edit-ff-code-placeholder"),null,null,null,"required");
$form->addField("text","name",api_text("customers_edit-ff-name"),$customer_obj->name,api_text("customers_edit-ff-name-placeholder"),null,null,null,"required");
$form->addField("text","fiscalName",api_text("customers_edit-ff-fiscalName"),$customer_obj->fiscalName,api_text("customers_edit-ff-fiscalName-placeholder"),null,null,null,"required");
$form->addField("text","address",api_text("customers_edit-ff-address"),$customer_obj->address,api_text("customers_edit-ff-address-placeholder"),null,null,null,"required");
$form->addField("number","latitude",api_text("customers_edit-ff-coordinates"),$customer_obj->latitude,api_text("customers_edit-ff-coordinates-latitude-placeholder"),null,null,null,"step='0.00001'");
$form->addField("number","longitude","&nbsp;",$customer_obj->longitude,api_text("customers_edit-ff-coordinates-longitude-placeholder"),null,null,null,"step='0.00001'");
$form->addField("select","classification",api_text("customers_edit-ff-classification"),$customer_obj->classification,api_text("customers_edit-ff-classification-select"));
$form->addFieldOption("A","A");
$form->addFieldOption("B","B");
$form->addFieldOption("C","C");
$form->addFieldOption("N","N");
$form->addField("number","potentialQuantity",api_text("customers_edit-ff-potentialQuantity"),($customer_obj->potentialQuantity/1000),api_text("customers_edit-ff-potentialQuantity-placeholder"));
$form->addFieldAddon(api_text("customers_edit-ff-potentialQuantity-append"));
$form->addField("select","tags[]",api_text("customers_edit-ff-tags"),($customer_obj->id?$customer_obj->getTags():null),null,null,null,null,"multiple");
foreach(api_crm_availableTags() as $tag_f){$form->addFieldOption($tag_f,$tag_f);}
$form->addField("textarea","note",api_text("customers_edit-ff-note"),$customer_obj->note,api_text("customers_edit-ff-note-placeholder"),null,null,null,"rows='5'");
// controls
$form->addControl("submit",api_text("form-fc-save"));
if($customer_obj->id){
	$form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=".api_return_script("customers_view")."&idCustomer=".$customer_obj->id);
	if(!$customer_obj->deleted){
		$form->addControl("button",api_text("form-fc-delete"),"?mod=".MODULE."&scr=submit&act=customer_delete&idCustomer=".$customer_obj->id,"btn-danger",api_text("customers_edit-fc-delete-confirm"));
	}else{
		$form->addControl("button",api_text("form-fc-undelete"),"?mod=".MODULE."&scr=submit&act=customer_undelete&idCustomer=".$customer_obj->id,"btn-warning");
		//$form->addControl("button",api_text("form-fc-remove"),"?mod=".MODULE."&scr=submit&act=customer_remove&idCustomer=".$customer_obj->id,"btn-danger",api_text("customers_edit-fc-remove-confirm"));
	}
}else{$form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_list");}
// modal scripts
$app->addScript("$(document).ready(function(){\$('select[name=\"tags[]\"]').select2({width:'100%',tags:true,allowClear:true});});");
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($form->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($customer_obj,"customer");
