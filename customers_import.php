<?php
/**
 * CRM - Customers Import
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorizations
 api_checkAuthorization("crm-customers_manage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("customers_import"));
 // acquire variables
 $r_search=$_REQUEST['search'];
 // build customer search form
 $form=new strForm("?mod=".MODULE."&scr=customers_import&act=customer_search&return_scr=".$_REQUEST['return_scr'],"POST",null,null,"customers_import");
 $form->addField("text","search",api_text("customers_import-ff-search"),$r_search,api_text("customers_import-ff-search-placeholder"),null,null,null,"required");
 $form->addFieldAddonButton("#",api_text("form-fc-search"),"btn btn-primary customers_import-btn-search");
 // check for search
 if(strlen($r_search)){
  // build table
  $table=new strTable(api_text("customers_import-tr-unvalued"));
  $table->addHeader("&nbsp;",null,16);
  $table->addHeader(api_text("customers_import-th-code"),"nowrap");
  $table->addHeader(api_text("customers_import-th-name"),null,"100%");
  $table->addHeader(api_text("customers_import-th-group"),"nowrap text-right");
  // cycle all customers
  foreach(api_crm_rfc_customer_search($r_search) as $customer_fobj){
   // make tr class
   if($customer_fobj->deleted){$tr_class="deleted";}else{$tr_class=null;}
   // make customer row
   $table->addRow($tr_class);
   $table->addRowFieldAction("?mod=".MODULE."&scr=customers_edit&code=".$customer_fobj->code,"fa-plus-square",api_text("customers_import-td-import"));
   $table->addRowField(api_tag("samp",ltrim($customer_fobj->code,"0")),"nowrap");
   $table->addRowField($customer_fobj->name,"truncate-ellipsis");
   $table->addRowField(api_tag("samp",$customer_fobj->group),"nowrap text-right");
  }
 }
 // jQuery scripts
 $app->addScript("/* Search submit */\n$('.customers_import-btn-search').click(function(){\$('#form_customers_import').submit();});");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 if(is_object($table)){$grid->addCol($table->render(),"col-xs-12");}
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();

?>