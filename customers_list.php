<?php
/**
 * CRM - Customers List
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

// check authorizations
api_checkAuthorization("crm-customers_view","dashboard");
// include module template
require_once(MODULE_PATH."template.inc.php");
// definitions
$users_array=array();
// set application title
$app->setTitle(api_text("customers_list"));
// definitions
$customers_array=array();
// build filter
$filter=new strFilter();
$filter->addSearch(array("code","name","fiscalName","address"));
// build query object
$query=new cQuery("crm__customers",$filter->getQueryWhere());
$query->addQueryOrderField("name");
// build pagination object
$pagination=new strPagination($query->getRecordsCount());
// cycle all results
foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$customers_array[$result_f->id]=new cCrmCustomer($result_f);}
// build table
$table=new strTable(api_text("customers_list-tr-unvalued"));
$table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
$table->addHeader(api_text("customers_list-th-code"),"nowrap");
$table->addHeader(api_text("customers_list-th-name"),"nowrap");
$table->addHeader(api_text("customers_list-th-fiscalName"),null,"100%");
$table->addHeader("&nbsp;",null,16);
// cycle all customers
foreach($customers_array as $customer_obj){
	// build operation button
	$ob=new strOperationsButton();
	$ob->addElement("?mod=".MODULE."&scr=customers_edit&idCustomer=".$customer_obj->id."&return_scr=customers_list","fa-pencil",api_text("customers_list-td-edit"),(api_checkAuthorization("crm-customers_manage")));
	if($customer_obj->deleted){$ob->addElement("?mod=".MODULE."&scr=submit&act=customer_undelete&idCustomer=".$customer_obj->id,"fa-trash-o",api_text("customers_list-td-undelete"),true,api_text("customers_list-td-undelete-confirm"));}
	else{$ob->addElement("?mod=".MODULE."&scr=submit&act=customer_delete&idCustomer=".$customer_obj->id,"fa-trash",api_text("customers_list-td-delete"),true,api_text("customers_list-td-delete-confirm"));}
	// make table row class
	$tr_class_array=array();
	if($customer_obj->id==$_REQUEST['idCustomer']){$tr_class_array[]="currentrow";}
	if($customer_obj->deleted){$tr_class_array[]="deleted";}
	// make credit check
	if(is_numeric($customer_obj->code)){$creditCheck="&act=credit_check";}else{$creditCheck="";}
	// make customer row
	$table->addRow(implode(" ",$tr_class_array));
	$table->addRowFieldAction("?mod=".MODULE."&scr=customers_view&idCustomer=".$customer_obj->id.$creditCheck,"fa-search",api_text("customers_list-td-view"));
	$table->addRowField(api_tag("samp",ltrim($customer_obj->code,"0")),"nowrap");
	$table->addRowField($customer_obj->name,"nowrap");
	$table->addRowField($customer_obj->fiscalName,"truncate-ellipsis");
	$table->addRowField($ob->render(),"text-right");
}
// build grid object
$grid=new strGrid();
$grid->addRow();
$grid->addCol($filter->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($table->render(),"col-xs-12");
$grid->addRow();
$grid->addCol($pagination->render(),"col-xs-12");
// add content to application
$app->addContent($grid->render());
// renderize application
$app->render();
// debug
api_dump($query,"query");
