<?php
/**
 * CRM - Customers Offers Export
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */
// check authorizations
api_checkAuthorization("crm-customers_manage","dashboard");
// get objects
$customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
$division_obj=new cCrmDivision($_REQUEST['division']);
// check objects
if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
// acquire variables
$r_offers_array=$_REQUEST['offers'];
$r_introduction=$_REQUEST['introduction'];
$r_paymentMethod=$_REQUEST['paymentMethod'];
$r_paymentCondition=$_REQUEST['paymentCondition'];
$r_transport=$_REQUEST['transport'];
$r_various=$_REQUEST['various'];
$r_conclusion=$_REQUEST['conclusion'];
// check variables
if(!is_array($r_offers_array) || !count($r_offers_array)){api_alerts_add(api_text("crm_alert-customerOfferNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);}
// make random hash
$hash=md5(time());
// include pdf library
require_once(DIR."helpers/tcpdf/php/tcpdf.php");
// create new pdf document
$pdf=new TCPDF("L","mm","A4",true,"UTF-8",false,true);
// set properties
$pdf->SetCreator("TCPDF");
$pdf->SetAuthor($settings->owner);
$pdf->SetTitle($settings->owner." - Offerta di vendita");
$pdf->SetSubject($settings->owner." - Offerta di vendita #".$hash);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetFont("helvetica","",10);
$pdf->SetDefaultMonospacedFont("freemono");
$pdf->SetMargins(10,10);
$pdf->SetAutoPageBreak(true,10);
$pdf->setImageScale(1.25);
$pdf->setFontSubsetting(true);
$pdf->SetTextColor(0);
$pdf->SetFillColor(245);
$pdf->SetDrawColor(90);
$pdf->AddPage();
// definitions
$fill=false;
$border="";
// check for division logo
$logo_dir=DIR."uploads/crm/divisions/logo_".$division_obj->id.".png";
if(file_exists($logo_dir)){
	$logo_size=getimagesize($logo_dir);
	$logo_x=$logo_size[0];
	$logo_y=$logo_size[1];
	$x_padding=round($logo_x*13/$logo_y)+12;
	$pdf->Image($logo_dir,10,10,"",13,"PNG","http://wwww.cogne.com","T",false,300,"",false,false,0,false,false,false);
}else{
	$x_padding=10;
}
// check for customer code for customer name
if($customer_obj->code!="9999999999"){$offers_customer_name=$customer_obj->name;}
else{$offers_customer_name=(new cCrmOffer(end($r_offers_array)))->customer_name;}
// header
$pdf->SetFont("helvetica","B",14);
$pdf->MultiCell(0,0,$division_obj->fiscal_name,$border,"L",false,0,$x_padding,9,true,0,false,false,0,"T");
$pdf->SetFont("helvetica","",8);
$pdf->MultiCell(0,0,$division_obj->fiscal_address,$border,"L",false,0,$x_padding,15.5);
$pdf->MultiCell(0,0,"P.IVA: ".$division_obj->fiscal_vat." - C.F: ".$division_obj->fiscal_code,$border,"L",false,0,$x_padding,20);
$pdf->ln(7);
// introduction
$pdf->SetFont("helvetica","",10);
$pdf->MultiCell(0,0,"Spettabile",$border,"L",false,1);
$pdf->SetFont("helvetica","B",10);
$pdf->MultiCell(0,0,$offers_customer_name,$border,"L",false,1);
if($customer_obj->code!="9999999999"){
	$pdf->SetFont("helvetica","",10);
	$pdf->MultiCell(0,0,$customer_obj->address,$border,"L",false,1);
}
$pdf->ln(5);
$pdf->SetFont("helvetica","",10);
$pdf->MultiCell(0,0,$r_introduction."\n",$border,"J",false,1);
$pdf->ln(5);
// table header
$pdf->SetFont("helvetica","B",7);
//$pdf->Cell(5,4,"",$border,0,"L",false);
$pdf->Cell(20,4,"OFFERTA",$border,0,"L",false);
$pdf->Cell(50,4,"MARCA ( CLIENTE / W.N. / COGNE )",$border,0,"L",false);
$pdf->Cell(19,4,"ESECUZIONE",$border,0,"L",false);
$pdf->Cell(15,4,"PROFILO",$border,0,"L",false);
$pdf->Cell(8,4,"TT",$border,0,"L",false);
$pdf->Cell(15,4,"DIM. (mm)",$border,0,"R",false);
$pdf->Cell(13,4,"TOLL.",$border,0,"L",false);
$pdf->Cell(22,4,"LUNG. (mm)",$border,0,"R",false);
$pdf->Cell(15,4,"PEZZI (nr)",$border,0,"R",false);
$pdf->Cell(15,4,"PESO (kg)",$border,0,"R",false);
$pdf->Cell(20,4,"PREZZO (€/kg)",$border,0,"R",false);
$pdf->Cell(20,4,"EXTRA LEGA",$border,0,"L",false);
$pdf->Cell(20,4,"TAGLIO (€/tot)",$border,0,"R",false);
$pdf->Cell(25,4,"DISPONIBILE DAL",$border,0,"R",false);
$pdf->ln(5);
// table rows
$pdf->SetFont("helvetica","",10);
$border="1";
// cycle all selected offers
foreach($r_offers_array as $offer_f){
	// get object
	$offer_obj=new cCrmOffer($offer_f);
	// check object
	if(!$offer_obj->id){continue;}
	// invert fill
	$fill=!$fill;
	// make grade
	$grade_td=$offer_obj->request_grade." / ".$offer_obj->request_wn." / ".$offer_obj->grade;
	// make subrow
	$subrow_td=null;
	if(strlen($offer_obj->note)){$subrow_td.="Note: ".nl2br($offer_obj->note);}
	// table fields
	$pdf->SetFont("helvetica","",10);
	$pdf->Cell(20,5,$offer_obj->id,"TL",0,"L",$fill);
	$pdf->Cell(50,5,$grade_td,"TL",0,"L",$fill);
	$pdf->Cell(19,5,api_crm_offers_availableExecutions()[$offer_obj->execution]->exp,"TL",0,"L",$fill);
	$pdf->Cell(15,5,api_crm_offers_availableProfiles()[$offer_obj->profile]->exp,"TL",0,"L",$fill);
	$pdf->Cell(8,5,api_crm_offers_availableHeatTreatments()[$offer_obj->heattreath]->code,"TL",0,"L",$fill);
	$pdf->Cell(15,5,$offer_obj->size,"TL",0,"R",$fill);
	$pdf->Cell(13,5,api_crm_offers_availableTollerances()[$offer_obj->tollerance]->text,"TL",0,"L",$fill);
	$pdf->Cell(22,5,($offer_obj->length_cutted?$offer_obj->length_cutted.($offer_obj->length_tollerance?" (".$offer_obj->length_tollerance.")":null):$offer_obj->length),"TL",0,"R",$fill);
	$pdf->Cell(15,5,api_number_format($offer_obj->quantity_pieces,0,null,false,false,"-"),"TL",0,"R",$fill);
	$pdf->Cell(15,5,api_number_format($offer_obj->quantity_weight,0),"TL",0,"R",$fill);
	$pdf->Cell(20,5,api_number_format($offer_obj->price_offer,2),"TL",0,"R",$fill);
	$pdf->Cell(20,5,($offer_obj->price_alloy?"Esclusa":"Inclusa"),"TL",0,"L",$fill);
	//$pdf->Cell(20,5,($offer_obj->price_cutting>0?api_number_format($offer_obj->price_cutting,0):"-"),"TL",0,"R",$fill);
	$pdf->Cell(20,5,api_number_format($offer_obj->price_cutting,0,null,false,false,"-"),"TL",0,"R",$fill);
	$pdf->Cell(25,5,$offer_obj->availability,"TLR",0,"R",$fill);
	// check for subrow
	if($subrow_td){
		$pdf->ln(5);
		$pdf->SetFont("helvetica","",9);
		$pdf->writeHTMLCell('','','','',$subrow_td,"TBLR",1,"L",$fill);
		$pdf->ln(2);
	}else{
		$pdf->ln(5);
		$pdf->Cell(0,2,"","T",0,"L",false);
		$pdf->ln(2);
	}
}
$pdf->ln(5);
// reset border, fill and font
$border="";
$fill=false;
$pdf->SetFont("helvetica","",10);
// check for payment
if(strlen($r_paymentMethod)){
	$pdf->MultiCell(0,0,"Metodo di pagamento: \n".api_crm_availablePaymentsMethods()[$r_paymentMethod]->name." - ".api_crm_availablePaymentsConditions()[$r_paymentCondition]->name."\n",$border,"J",false,1);
	$pdf->ln(5);
}
$pdf->SetFont("helvetica","",10);
// check for transport note
if(strlen($r_transport)){
	$pdf->MultiCell(0,0,"Note di trasporto: \n".$r_transport."\n",$border,"J",false,1);
	$pdf->ln(5);
}
// check for various note
if(strlen($r_various)){
	$pdf->MultiCell(0,0,"Note accessorie: \n".$r_various."\n",$border,"J",false,1);
	$pdf->ln(5);
}
// conclusion
$pdf->MultiCell(0,0,$r_conclusion."\n",$border,"J",false,1);
// legends
$pdf->ln(10);
$pdf->SetFont("helvetica","B",7);
$pdf->Cell(0,5,"LEGENDA","T",1,"L");
$pdf->SetFont("helvetica","",8);
// heattreaths
$heattreaths=array();
foreach(api_crm_offers_availableHeatTreatments() as $heattreath_fobj){$heattreaths[]=$heattreath_fobj->text;}
$pdf->MultiCell(0,0,"Trattamento Termico: ".implode(", ",$heattreaths),$border,"L",false,1);
// generate pdf to browser
//$pdf->Output($hash.".pdf", "I");
$pdf->Output("offerta_cogne_".date("Ymd_Hm").".pdf", "I");
?>