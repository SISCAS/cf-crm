<?php
/**
 * CRM - Customers View (Attachments)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build attachments table
 $attachments_table=new strTable(api_text("customers_view-attachments-tr-unvalued"));
 $attachments_table->addHeader("&nbsp;",null,16);
 $attachments_table->addHeader(api_text("customers_view-attachments-th-description"),null,"100%");
 $attachments_table->addHeader("&nbsp;",null,16);
 // cycle customer attachments
 foreach($customer_obj->getAttachments() as $attachment_obj){
  // make table row class
  $tr_class_array=array();
  if($attachment_obj->id==$_REQUEST['idAttachment']){$tr_class_array[]="currentrow";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=attachments&act=attachment_edit&idCustomer=".$customer_obj->id."&idAttachment=".$attachment_obj->id,"fa-pencil",api_text("customers_view-attachments-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_attachment_remove&tab=attachments&idCustomer=".$customer_obj->id."&idAttachment=".$attachment_obj->id,"fa-trash",api_text("customers_view-attachments-td-remove"),true,api_text("customers_view-attachments-td-remove-confirm"));
  // add attachment row
  $attachments_table->addRow(implode(" ",$tr_class_array));
  $attachments_table->addRowField(api_link($attachment_obj->url,api_icon("fa-paperclip",api_text("customers_view-attachments-td-download")),null,"hidden-link",null,null,null,null,"_blank"),"nowrap");
  $attachments_table->addRowField($attachment_obj->description,"truncate-ellipsis");
  $attachments_table->addRowField($ob_obj->render(),"text-right");
 }

 // attachment add or edit action
 if(in_array(ACTION,array("attachment_add","attachment_edit"))){
  // get selected attachment
  $selected_attachment_obj=new cAttachment($_REQUEST['idAttachment']);
  // build attachment add form
  $attachment_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_attachment_save&idCustomer=".$customer_obj->id."&idAttachment=".$selected_attachment_obj->id,"POST",null,null,"customers_view-attachments");
  $attachment_form->addField("textarea","description",api_text("customers_view-attachments_modal-ff-description"),$selected_attachment_obj->description,api_text("customers_view-attachments_modal-ff-description-placeholder"),null,null,null,"required rows='3'");
  if(!$selected_attachment_obj->id){$attachment_form->addField("file","file",api_text("customers_view-attachments_modal-ff-file"),null,null,null,null,null,"required accept='.pdf'");}
  $attachment_form->addControl("submit",api_text("form-fc-save"));
  $attachment_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build attachment add modal window
  $attachment_modal=new strModal(($selected_attachment_obj->id?api_text("customers_view-attachments_modal-title-edit",$customer_obj->name):api_text("customers_view-attachments_modal-title-add",$customer_obj->name)),null,"customers_view-attachments_modal");
  $attachment_modal->setBody($attachment_form->render(2));
  // add modal to application
  $app->addModal($attachment_modal);
  // jQuery scripts
  $app->addScript("$(function(){\$('#modal_customers_view-attachments_modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>