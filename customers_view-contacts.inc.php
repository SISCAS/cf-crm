<?php
/**
 * CRM - Customers View (Contacts)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build contacts table
 $contacts_table=new strTable(api_text("customers_view-contacts-tr-unvalued"));
 $contacts_table->addHeader(api_text("customers_view-contacts-th-contact"),"nowrap");
 $contacts_table->addHeader(api_text("customers_view-contacts-th-role"),"nowrap");
 $contacts_table->addHeader(api_text("customers_view-contacts-th-phone"),"nowrap");
 $contacts_table->addHeader(api_text("customers_view-contacts-th-mail"),null,"100%");
 $contacts_table->addHeader(api_text("customers_view-contacts-th-division"),"nowrap text-right");
 $contacts_table->addHeader("&nbsp;",null,16);
 // cycle customer contacts
 foreach($customer_obj->getContacts() as $contact_fobj){
  // make table row class
  $tr_class_array=array();
  if($contact_fobj->id==$_REQUEST['idContact']){$tr_class_array[]="currentrow";}
  if($contact_fobj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=contacts&act=contact_view&idCustomer=".$customer_obj->id."&idContact=".$contact_fobj->id,"fa-info-circle",api_text("customers_view-contacts-td-view"));
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=contacts&act=contact_edit&idCustomer=".$customer_obj->id."&idContact=".$contact_fobj->id,"fa-pencil",api_text("customers_view-contacts-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_contact_remove&tab=contacts&idCustomer=".$customer_obj->id."&idContact=".$contact_fobj->id,"fa-trash",api_text("customers_view-contacts-td-remove"),true,api_text("customers_view-contacts-td-remove-confirm"));
  // add contact row
  $contacts_table->addRow(implode(" ",$tr_class_array));
  $contacts_table->addRowField($contact_fobj->getFullname(),"nowrap");
  $contacts_table->addRowField($contact_fobj->role,"nowrap");
  $contacts_table->addRowField($contact_fobj->phone,"nowrap");
  $contacts_table->addRowField($contact_fobj->mail,"truncate-ellipsis");
  $contacts_table->addRowField((new cCrmDivision($contact_fobj->fkDivision))->name,"nowrap text-right");
  $contacts_table->addRowField($ob_obj->render(),"text-right");
 }

// check contact actions
if(ACTION=="contact_view" && $_REQUEST['idContact']){
	// get selected contact
	$selected_contact_obj=new cCrmContact($_REQUEST['idContact']);
	//api_dump($selected_contact_obj);
	// build contact description list
	$contact_dl=new strDescriptionList("br","dl-horizontal");
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-fkDivision"),(new cCrmDivision($selected_contact_obj->fkDivision))->name);
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-contact"),api_tag('strong',$selected_contact_obj->getFullname()));
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-role"),$selected_contact_obj->role);
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-phone"),$selected_contact_obj->phone);
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-mail"),$selected_contact_obj->mail);
	if($selected_contact_obj->note){$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-note"),nl2br($selected_contact_obj->note));}
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-acquisition"),api_date_format($selected_contact_obj->acquisition,api_text("date")));
	$contact_dl->addSeparator("hr");
	$contact_dl->addElement(api_text("customers_view-contacts_modal-dt-user"),(new cUser($selected_contact_obj->addFkUser))->fullname);
	// build contact view modal window
	$contact_modal=new strModal(api_text("customers_view-contacts_modal-title-view",$customer_obj->name),null,"customers_view-contacts_modal");
	$contact_modal->setBody($contact_dl->render());
	// add modal to application
	$app->addModal($contact_modal);
	// jQuery scripts
	$app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-contacts_modal').modal('show');});");
}

 // check contact actions
 if(in_array(ACTION,array("contact_add","contact_edit"))){
  // get selected contact
  $selected_contact_obj=new cCrmContact($_REQUEST['idContact']);
  // build contact add form
  $contact_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_contact_save&idCustomer=".$customer_obj->id."&idContact=".$selected_contact_obj->id,"POST",null,null,"customers_view-contacts");
  $contact_form->addField("select","fkDivision",api_text("customers_view-contacts_modal-ff-fkDivision"),($selected_contact_obj->fkDivision?$selected_contact_obj->fkDivision:api_parameter_default("division")),api_text("customers_view-contacts_modal-ff-fkDivision-placeholder"),null,null,null,"required");
  foreach(api_crm_availableDivisions(true) as $division_fobj){$contact_form->addFieldOption($division_fobj->id,$division_fobj->name);}
  $contact_form->addField("text","lastname",api_text("customers_view-contacts_modal-ff-lastname"),$selected_contact_obj->lastname,api_text("customers_view-contacts_modal-ff-lastname-placeholder"),null,null,null,"required");
  $contact_form->addField("text","firstname",api_text("customers_view-contacts_modal-ff-firstname"),$selected_contact_obj->firstname,api_text("customers_view-contacts_modal-ff-firstname-placeholder"),null,null,null,"required");
  $contact_form->addField("text","role",api_text("customers_view-contacts_modal-ff-role"),$selected_contact_obj->role,api_text("customers_view-contacts_modal-ff-role-placeholder"));
  $contact_form->addField("text","mail",api_text("customers_view-contacts_modal-ff-mail"),$selected_contact_obj->mail,api_text("customers_view-contacts_modal-ff-mail-placeholder"));
  $contact_form->addField("text","phone",api_text("customers_view-contacts_modal-ff-phone"),$selected_contact_obj->phone,api_text("customers_view-contacts_modal-ff-phone-placeholder"));
  $contact_form->addField("textarea","note",api_text("customers_view-contacts_modal-ff-note"),$selected_contact_obj->note,api_text("customers_view-contacts_modal-ff-note-placeholder"));
	$contact_form->addField("date","acquisition",api_text("customers_view-contacts_modal-ff-acquisition"),$selected_contact_obj->acquisition,null,null,null,null,"required");
	$contact_form->addControl("submit",api_text("form-fc-save"));
  $contact_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build contact add modal window
  $contact_modal=new strModal(($selected_contact_obj->id?api_text("customers_view-contacts_modal-title-edit",$customer_obj->name):api_text("customers_view-contacts_modal-title-add",$customer_obj->name)),null,"customers_view-contacts_modal");
  $contact_modal->setBody($contact_form->render(1));
  // add modal to application
  $app->addModal($contact_modal);
  // jQuery scripts
  $app->addScript("$(function(){\$('#modal_customers_view-contacts_modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>