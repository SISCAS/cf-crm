<?php
/**
 * CRM - Customers View (Informations)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cCrmCustomer $customer_obj
 */

// make specifications counters
$specifications_counters_array=array();
$specifications_counter=(int)$GLOBALS['database']->queryCount("crm__customers__specifications","`fkCustomer`='".$customer_obj->id."'");
$specifications_counters_array[]=api_icon("fa-file-code-o",api_text("customers_view-informations-dd-specifications-total"))." ".$specifications_counter;
// make relationships counters
$relationships_counters_array=array();
$relationships_counter=(int)$GLOBALS['database']->queryCount("crm__customers__relationships","`fkCustomer`='".$customer_obj->id."'");
$relationships_counters_array[]=api_icon("fa-comments-o",api_text("customers_view-informations-dd-relationships-total"))." ".$relationships_counter;
// cycle all typologies
foreach(api_crm_relationships_availableTypologies() as $typology_fobj){
	$counter=(int)$GLOBALS['database']->queryCount("crm__customers__relationships","`fkCustomer`='".$customer_obj->id."' AND `typology`='".$typology_fobj->code."'");
	if($counter){$relationships_counters_array[]=$typology_fobj->icon." ".$counter;}
}
// make offers counters
$offers_counters_array=array();
$offers_counter=(int)$GLOBALS['database']->queryCount("crm__customers__offers","`fkCustomer`='".$customer_obj->id."'");
$offers_counters_array[]=api_icon("fa-clipboard",api_text("customers_view-informations-dd-offers-total"))." ".$offers_counter;
// cycle all status
foreach(api_crm_offers_availableStatus() as $status_fobj){
	$counter=(int)$GLOBALS['database']->queryCount("crm__customers__offers","`fkCustomer`='".$customer_obj->id."' AND `status`='".$status_fobj->code."'");
	if($counter){$offers_counters_array[]=$status_fobj->icon." ".$counter;}
}
// make informations description list
$informations_dl=new strDescriptionList("br","dl-horizontal");
if($customer_obj->classification){$informations_dl->addElement(api_text("customers_view-informations-dt-classification"),$customer_obj->classification);}
if($customer_obj->potentialQuantity){$informations_dl->addElement(api_text("customers_view-informations-dt-potentialQuantity"),api_text("customers_view-informations-dd-potentialQuantity",api_number_format(($customer_obj->potentialQuantity/1000),0)));}
if(count($customer_obj->getTags())){$informations_dl->addElement(api_text("customers_view-informations-dt-tags"),implode(", ",$customer_obj->getTags()));}
$informations_dl->addElement(api_text("customers_view-informations-dt-contacts"),api_tag("samp",api_icon("fa-users",api_text("customers_view-informations-dd-specifications-total"))." ".count($customer_obj->getContacts())));
$informations_dl->addElement(api_text("customers_view-informations-dt-specifications"),api_tag("samp",implode("&nbsp;&nbsp;&nbsp;",$specifications_counters_array)));
$informations_dl->addElement(api_text("customers_view-informations-dt-relationships"),api_tag("samp",implode("&nbsp;&nbsp;&nbsp;",$relationships_counters_array)));
$informations_dl->addElement(api_text("customers_view-informations-dt-offers"),api_tag("samp",implode("&nbsp;&nbsp;&nbsp;",$offers_counters_array)));
// notes
if($customer_obj->note){
	if($_GET['note']){$informations_dl->addElement(api_text("customers_view-informations-dt-note"),nl2br($customer_obj->note),null,"text-justify");}
	else{$informations_dl->addElement(api_text("customers_view-informations-dt-note"),api_link("?mod=".MODULE."&scr=customers_view&tab=informations&note=1&idCustomer=".$customer_obj->id,api_text("customers_view-informations-dd-note")));}
}

// credit check action
if(ACTION=="credit_check"){
	// get customer informations from sap
	$customer_informations=api_crm_rfc_customer_informations($customer_obj->code);
	$credits_obj=$customer_informations->credits_array['0001'];
	//api_dump($credits_obj);
	// build credits description list
	$credits_dl=new strDescriptionList("br","dl-horizontal");
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-horizon"),api_timestamp_format($credits_obj->horizon,api_text("date")));
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-limit"),api_number_format($credits_obj->limit,2,"&euro;"));
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-totals"),api_number_format($credits_obj->totals,2,"&euro;"));
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-percentage"),api_number_format($credits_obj->percentage,2)."&percnt;");
	$credits_dl->addSeparator("hr");
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-credits"),api_number_format($credits_obj->credits,2,"&euro;"));
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-engagements"),api_number_format($credits_obj->engagements,2,"&euro;"));
	$credits_dl->addElement(api_text("customers_view-informations-credit_modal-dt-orders"),api_number_format($credits_obj->orders,2,"&euro;"));
	// unsolved invoices table
	$unsolveds_table=new strTable(api_text("customers_view-informations-credit_modal-tr-unvalued"));
	$unsolveds_table->addHeader(api_text("customers_view-informations-credit_modal-th-unsolveds"),null,"100%",null,"colspan='6'");
	// make authorized companies
	$authorized_companies_array=array();
	foreach(api_crm_availableDivisions(true) as $division_fobj){$authorized_companies_array[$division_fobj->company]=$division_fobj->company;}
	// cycle all customers
	foreach($customer_informations->unsolveds_array as $unsolved_obj){
		// skip unexpired
		if($unsolved_obj->expiration>time()){continue;}
		// skip unauthorized companies
		if(!in_array($unsolved_obj->company,$authorized_companies_array)){continue;}
		// make table row class
		$tr_class_array=array();
		if($unsolved_obj->id==$_REQUEST['idCustomer']){$tr_class_array[]="currentrow";}
		if($unsolved_obj->deleted){$tr_class_array[]="deleted";}
		// make customer row
		$unsolveds_table->addRow(implode(" ",$tr_class_array));
		$unsolveds_table->addRowField($unsolved_obj->company,"nowrap");
		$unsolveds_table->addRowField($unsolved_obj->fiscalYear,"nowrap");
		$unsolveds_table->addRowField($unsolved_obj->fiscalDocument,"truncate-ellipsis");
		//$unsolveds_table->addRowField($unsolved_obj->fiscalElement,"nowrap");
		$unsolveds_table->addRowField(api_number_format($unsolved_obj->importInternal,2,"€",true),"nowrap text-right");
		$unsolveds_table->addRowField(api_timestamp_format($unsolved_obj->expiration,api_text("date")),"nowrap");
		$unsolveds_table->addRowField($unsolved_obj->paymentModality,"nowrap");
	}
	// build credits view modal window
	$credits_modal=new strModal(api_text("customers_view-informations-credit_modal-title-view",$customer_obj->name),null,"customers_view-informations-credit_modal");
	$credits_modal->setBody($credits_dl->render()."<hr>".$unsolveds_table->render());
	// add modal to application
	$app->addModal($credits_modal);
	// jQuery scripts
	$app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-informations-credit_modal').modal('show');});");
}
