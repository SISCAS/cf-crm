<?php
/**
 * CRM - Customers View (Maps)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check actions
 if(ACTION=="map_view"){
  $map_script=<<<EOS
function loadMap(){
 var latlng=L.latLng($customer_obj->latitude,$customer_obj->longitude);
 var mymap=L.map('customers_view-maps_modal_map').setView(latlng,14);
 L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{attribution:"&copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a> contributors",}).addTo(mymap);
 L.marker(latlng).addTo(mymap).bindPopup("$customer_obj->name");
}
// load map after modal show
$('#modal_customers_view-maps_modal').on('shown.bs.modal',function(){loadMap();});
EOS;
  // build operator add modal window
  $operator_modal=new strModal(api_text("customers_view-maps_modal-title",$customer_obj->name),null,"customers_view-maps_modal");
  $operator_modal->setBody("    <div id=\"customers_view-maps_modal_map\" style=\"height:256px;\"></div>\n");
  // add modal to application
  $app->addModal($operator_modal);
  // Leaf Let CSS
  $app->addStylesheet(PATH."helpers/leaflet/css/leaflet-1.3.4.css");
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-maps_modal').modal('show');});");
  $app->addScript(PATH."helpers/leaflet/js/leaflet-1.3.4.min.js",true);
  $app->addScript($map_script);
 }

?>