<?php
/**
 * CRM - Customers View (Offers - Customer)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // get selected offer
 $selected_offer_obj=new cCrmOffer($_REQUEST['idOffer']);
 // make product
 $product_dd=$selected_offer_obj->grade." ".$selected_offer_obj->execution." ".$selected_offer_obj->profile." ".$selected_offer_obj->size." mm";
 if($selected_offer_obj->material){$product_dd=ltrim($selected_offer_obj->material,"0")." &rarr; ".$product_dd;}
 // build offer description list
 $offer_dl=new strDescriptionList("br","dl-horizontal");
 $offer_dl->addElement(api_text("customers_view-offers_customer-modal-dt-division"),(new cCrmDivision($selected_offer_obj->fkDivision))->name);
 $offer_dl->addElement(api_text("customers_view-offers_customer-modal-dt-request"),$selected_offer_obj->request_number." - ".api_date_format($selected_offer_obj->request_date,api_text("date")));
 $offer_dl->addElement(api_text("customers_view-offers_customer-modal-dt-product"),$product_dd);
 // build export form
 $customer_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_offer_customer&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,"POST");
 // fields
 $customer_form->addField("select","fkCustomer",api_text("customers_view-offers_customer-modal-ff-fkCustomer"),$customer_obj->id,null,null,null,"width:100%;","required");
 foreach(api_crm_availableCustomers() as $customer_f){$customer_form->addFieldOption($customer_f->id,$customer_f->code." - ".$customer_f->name);}
 // controls
 $customer_form->addControl("submit",api_text("form-fc-save"));
 $customer_form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);
 // build offer view modal window
 $offer_modal=new strModal(api_text("customers_view-offers_customer-modal-title",array($selected_offer_obj->id,api_timestamp_format($selected_offer_obj->timestamp,api_text("date")))),null,"customers_view-offers_customer-modal");
 $offer_modal->setBody($offer_dl->render().$customer_form->render(2));
 $offer_modal->setFooter($operation_buttons);
 // add modal to application
 $app->addModal($offer_modal);
 // jQuery scripts
 $app->addScript("$(function(){\$('#modal_customers_view-offers_customer-modal').modal('show');});");
 $app->addScript("$(document).ready(function(){\$('select[name=\"fkCustomer\"]').select2({allowClear:true,placeholder:\"".api_text("customers_view-offers_customer-modal-ff-fkCustomer-select")."\",dropdownParent:\$('#modal_customers_view-offers_customer-modal')});});");

?>