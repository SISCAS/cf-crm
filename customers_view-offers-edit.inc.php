<?php
/**
 * CRM - Customers View (Offers - Edit)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

// definitions
$modal_body=null;
// get selected offer
$selected_offer_obj=new cCrmOffer($_REQUEST['idOffer']);
// build filters object
$filters=new stdClass();
$filters->customer_name=(isset($_REQUEST['customer_name'])?$_REQUEST['customer_name']:($selected_offer_obj->customer_name?$selected_offer_obj->customer_name:$_COOKIE["crm-customer_name"]));
$filters->division=(isset($_REQUEST['division'])?$_REQUEST['division']:$selected_offer_obj->fkDivision);
$filters->grade=(isset($_REQUEST['grade'])?$_REQUEST['grade']:$selected_offer_obj->grade);
$filters->execution=(isset($_REQUEST['execution'])?$_REQUEST['execution']:$selected_offer_obj->execution);
$filters->profile=(isset($_REQUEST['profile'])?$_REQUEST['profile']:$selected_offer_obj->profile);
$filters->size=(isset($_REQUEST['size'])?$_REQUEST['size']:$selected_offer_obj->size);
$filters->size_max=(isset($_REQUEST['size_max'])?$_REQUEST['size_max']:$selected_offer_obj->size_max);
$filters->material=(isset($_REQUEST['material'])?$_REQUEST['material']:$selected_offer_obj->material);
// check for save
if(!$_REQUEST['save']){
	// build search form
	$offer_form=new strForm("?","GET",null,"customers_view-offers_edit");
	$offer_form->addField("hidden","mod",null,"crm");
	$offer_form->addField("hidden","scr",null,"customers_view");
	$offer_form->addField("hidden","tab",null,"offers");
	$offer_form->addField("hidden","act",null,"offer_add");
	$offer_form->addField("hidden","idCustomer",null,$customer_obj->id);
	$offer_form->addField("hidden","idOffer",null,$selected_offer_obj->id);
	if($customer_obj->code=="9999999999"){$offer_form->addField("text","customer_name",api_text("customers_view-offers_edit-modal-ff-customer_name"),$filters->customer_name,api_text("customers_view-offers_edit-modal-ff-customer_name-placeholder"),null,null,null,"required");}
	$offer_form->addField("select","division",api_text("customers_view-offers_edit-modal-ff-division"),($filters->division?$filters->division:api_parameter_default("division")),api_text("customers_view-offers_edit-modal-ff-division-placeholder"),null,null,null,"required");
	foreach(api_crm_availableDivisions(true) as $division_f){$offer_form->addFieldOption($division_f->id,$division_f->name);}
	$offer_form->addField("select","grade",api_text("customers_view-offers_edit-modal-ff-grade"),$filters->grade,api_text("customers_view-offers_edit-modal-ff-grade-placeholder"),null,null,null,"required");
	foreach(api_crm_offers_availableGrades() as $grade_f){$offer_form->addFieldOption($grade_f->code,$grade_f->text);}
	$offer_form->addField("select","execution",api_text("customers_view-offers_edit-modal-ff-execution"),$filters->execution,api_text("customers_view-offers_edit-modal-ff-execution-placeholder"),null,null,null,"required");
	foreach(api_crm_offers_availableExecutions() as $execution_f){$offer_form->addFieldOption($execution_f->code,$execution_f->code." ".$execution_f->text);}
	$offer_form->addField("select","profile",api_text("customers_view-offers_edit-modal-ff-profile"),$filters->profile,api_text("customers_view-offers_edit-modal-ff-profile-placeholder"),null,null,null,"required");
	foreach(api_crm_offers_availableProfiles() as $profile_f){$offer_form->addFieldOption($profile_f->code,$profile_f->text);}
	$offer_form->addField("number","size",api_text("customers_view-offers_edit-modal-ff-size"),$filters->size,api_text("customers_view-offers_edit-modal-ff-size-placeholder"),null,null,null,"step='0.01' required");
	$offer_form->addFieldAddon("mm");
	$offer_form->addField("number","size_max","&nbsp;",$filters->size_max,api_text("customers_view-offers_edit-modal-ff-size_max-placeholder"),null,null,null,"step='0.01'");
	$offer_form->addFieldAddon("mm");
	$offer_form->addControl("submit",api_text("form-fc-search"),null,null,null,null,"name='search' value='1'");
}elseif($_REQUEST['save']){
	// get material information
  if($filters->material){$material_obj=api_crm_rfc_material("0001",$filters->material);}
  //var_dump($material_obj);
  //var_dump($filters);
  // build offer add form
	$offer_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_offer_save&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,"POST",null,null,"customers_view-offers_edit");
	$offer_form->addField("hidden","customer_name",null,$filters->customer_name);
	$offer_form->addField("hidden","fkDivision",null,$filters->division);
	//foreach(api_crm_availableDivisions(true) as $division_fobj){$offer_form->addFieldOption($division_fobj->id,$division_fobj->name);}
	$offer_form->addField("select","fkSalesarea",api_text("customers_view-offers_edit-modal-ff-salesarea"),api_parameter_default("salesarea"),api_text("customers_view-offers_edit-modal-ff-salesarea-select"),null,null,null,"required");
	foreach((new cCrmDivision($filters->division))->getSalesareas() as $salesarea_fobj){$offer_form->addFieldOption($salesarea_fobj->id,$salesarea_fobj->label);}
	$offer_form->addField("text","request_number",api_text("customers_view-offers_edit-modal-ff-request_number"),($selected_offer_obj->request_number?$selected_offer_obj->request_number:$_COOKIE["crm-request_number"]),api_text("customers_view-offers_edit-modal-ff-request_number-placeholder"),null,null,null,"required");
	$offer_form->addField("date","request_date",api_text("customers_view-offers_edit-modal-ff-request_date"),($selected_offer_obj->request_date?$selected_offer_obj->request_date:$_COOKIE["crm-request_date"]),null,null,null,null,"required");
  $offer_form->addField("text","request_material",api_text("customers_view-offers_edit-modal-ff-request_material"),($selected_offer_obj->request_material?$selected_offer_obj->request_material:$_COOKIE["crm-request_material"]),api_text("customers_view-offers_edit-modal-ff-request_material-placeholder"));
  $offer_form->addField("text","request_grade",api_text("customers_view-offers_edit-modal-ff-request_grade"),($selected_offer_obj->request_grade?$selected_offer_obj->request_grade:$_COOKIE["crm-request_grade"]),api_text("customers_view-offers_edit-modal-ff-request_grade-placeholder"),null,null,null,"required");
  $offer_form->addField("select","request_wn",api_text("customers_view-offers_edit-modal-ff-request_wn"),($selected_offer_obj->request_wn?$selected_offer_obj->request_wn:$material_obj->euronorm),api_text("customers_view-offers_edit-modal-ff-request_wn-placeholder"));
	foreach(api_crm_offers_availableEuronorms() as $grade_f){$offer_form->addFieldOption($grade_f->code,$grade_f->text);}
	if(!strlen($material_obj->material)){$offer_form->addField("static","product",api_text("customers_view-offers_edit-modal-ff-product"),$filters->grade." ".$filters->execution." ".$filters->profile." ".$filters->size." mm ");}
	else{$offer_form->addField("static","product",api_text("customers_view-offers_edit-modal-ff-product"),ltrim($material_obj->material,"0")." &rarr; ".$material_obj->grade." ".$material_obj->execution." ".$material_obj->profile." ".$material_obj->size." mm ".$material_obj->heattreath.($material_obj->length_min?" ".$material_obj->length_min."-".$material_obj->length_max." mm ".$material_obj->tollerance:null));}
	$offer_form->addField("hidden","grade",null,$filters->grade);
	$offer_form->addField("hidden","execution",null,$filters->execution);
	$offer_form->addField("hidden","profile",null,$filters->profile);
	$offer_form->addField("hidden","size",null,$filters->size);
	if($filters->material){
		$offer_form->addField("hidden","material",null,$filters->material);
		$offer_form->addField("hidden","length",null,$material_obj->length_min."-".$material_obj->length_max);
	}
  $offer_form->addField("select","heattreath",api_text("customers_view-offers_edit-modal-ff-heattreath"),($selected_offer_obj->heattreath?$selected_offer_obj->heattreath:$material_obj->heattreath),api_text("customers_view-offers_edit-modal-ff-heattreath-placeholder"));
	foreach(api_crm_offers_availableHeatTreatments() as $heattreath_fobj){$offer_form->addFieldOption($heattreath_fobj->code,$heattreath_fobj->text);}
	$offer_form->addField("select","tollerance",api_text("customers_view-offers_edit-modal-ff-tollerance"),($selected_offer_obj->tollerance?$selected_offer_obj->tollerance:$material_obj->tollerance),api_text("customers_view-offers_edit-modal-ff-tollerance-placeholder"));
	foreach(api_crm_offers_availableTollerances() as $tollerance_fobj){$offer_form->addFieldOption($tollerance_fobj->code,$tollerance_fobj->text);}
	$offer_form->addField("number","length_cutted",api_text("customers_view-offers_edit-modal-ff-length_cutted"),$selected_offer_obj->length_cutted,api_text("customers_view-offers_edit-modal-ff-length_cutted-placeholder"));
	$offer_form->addFieldAddon("mm");
	$offer_form->addField("select","length_tollerance",api_text("customers_view-offers_edit-modal-ff-length_tollerance"),$selected_offer_obj->length_tollerance,api_text("customers_view-offers_edit-modal-ff-length_tollerance-placeholder"));
	foreach(array("-0/+3","-/+1,5") as $tollerance_f){$offer_form->addFieldOption($tollerance_f,$tollerance_f);}
	$offer_form->addField("number","quantity_pieces",api_text("customers_view-offers_edit-modal-ff-quantity_pieces"),$selected_offer_obj->quantity_pieces,api_text("customers_view-offers_edit-modal-ff-quantity_pieces-placeholder"));
	$offer_form->addField("number","quantity_weight",api_text("customers_view-offers_edit-modal-ff-quantity_weight"),$selected_offer_obj->quantity_weight,api_text("customers_view-offers_edit-modal-ff-quantity_weight-placeholder"),null,null,null,"required");
	if(!in_array($filters->profile,array("TONDI","ESAGONI"))){$offer_form->addFieldAddon("kg");}
	else{$offer_form->addFieldAddonButton("#","kg","btn-primary",null,"title=\"".api_text("customers_view-offers_edit-modal-ff-quantity_weight-calculate")."\"");}
	$offer_form->addField("number","price_list",api_text("customers_view-offers_edit-modal-ff-price_list"),number_format($selected_offer_obj->price_list,2),api_text("customers_view-offers_edit-modal-ff-price_list-placeholder"),null,null,null,"step='0.01' required");
	$offer_form->addFieldAddon("&euro;");
	$offer_form->addField("number","price_offer",api_text("customers_view-offers_edit-modal-ff-price_offer"),number_format($selected_offer_obj->price_offer,2),api_text("customers_view-offers_edit-modal-ff-price_offer-placeholder"),null,null,null,"step='0.01' min='0.01' max='99.99' required");
	$offer_form->addFieldAddon("&euro;");
	$offer_form->addField("radio","price_alloy",api_text("customers_view-offers_edit-modal-ff-price_alloy"),(int)$selected_offer_obj->price_alloy,null,null,"radio-inline",null,"required");
	$offer_form->addFieldOption(0,api_text("customers_view-offers_edit-modal-ff-price_alloy-no"));
	$offer_form->addFieldOption(1,api_text("customers_view-offers_edit-modal-ff-price_alloy-yes"));
	$offer_form->addField("number","price_cutting",api_text("customers_view-offers_edit-modal-ff-price_cutting"),number_format($selected_offer_obj->price_cutting,0),api_text("customers_view-offers_edit-modal-ff-price_cutting-placeholder"),null,null,null,"step='1'");
	$offer_form->addField("text","availability",api_text("customers_view-offers_edit-modal-ff-availability"),$selected_offer_obj->availability,null,null,null,null,"required");
	$offer_form->addField("textarea","note",api_text("customers_view-offers_edit-modal-ff-note"),$selected_offer_obj->note,api_text("customers_view-offers_edit-modal-ff-note-placeholder"));
	$offer_form->addField("textarea","texts",api_text("customers_view-offers_edit-modal-ff-texts"),$selected_offer_obj->texts,api_text("customers_view-offers_edit-modal-ff-texts-placeholder"));
	$offer_form->addField("date","expirationDate",api_text("customers_view-offers_edit-modal-ff-expirationDate"),($selected_offer_obj->expirationDate?$selected_offer_obj->expirationDate:api_timestamp_format(api_timestampDifferenceFrom(time(),"+5 days"),"Y-m-d")),null,null,null,null,"min='".api_date()."' required");
	$offer_form->addField("checkbox","important","&nbsp;",($selected_offer_obj->id?$selected_offer_obj->important:($customer_obj->classification=="A"?true:false)));
	$offer_form->addFieldOption(1,api_text("customers_view-offers_edit-modal-ff-important"));
	$offer_form->addControl("submit",api_text("form-fc-save"));
	// scripts
	$app->addScript("$('#form_customers_view-offers_edit_input_length_cutted').change(function(){if($(this).val()>0){\$('#form_customers_view-offers_edit_input_length_tollerance').prop('required',true);}else{\$('#form_customers_view-offers_edit_input_length_tollerance').prop('required',false);}});");
	$app->addScript("$('#form_customers_view-offers_edit_input_quantity_weight').change(function(){if($(this).val()>999){\$('#form_customers_view-offers_edit_input_important_option_0').attr('checked',true);}});");
	$calculator_constant=($filters->profile=="TONDI"?"0.00616":($filters->profile=="ESAGONI"?"0.0068":0));
  $app->addScript("/* Weight calculator */
$('#form_customers_view-offers_edit_input_quantity_weight_button').click(function(){
 // acquire
 size=parseInt($('#form_customers_view-offers_edit_input_size').val());
 length_cutted=parseInt($('#form_customers_view-offers_edit_input_length_cutted').val());
 tollerance=parseFloat((($('#form_customers_view-offers_edit_input_length_tollerance').val()=='-0/+3')?3:0.1));
 pieces=parseInt($('#form_customers_view-offers_edit_input_quantity_pieces').val());
 // check
 if(!size||!length_cutted||!tollerance||!pieces){alert('Parametri insufficienti');exit;}
 // calculate
 length_meter=(length_cutted+tollerance)/1000;
 piece_weight=size*size*length_meter*".$calculator_constant.";
 total_weight=Math.round(piece_weight*pieces);
 // debug
 //alert('dimensione: '+size+' lunghezza: '+length_cutted+' toleranza: '+tollerance+' lunghezza(m): '+length_meter+' pezzi: '+pieces+ ' peso: '+piece_weight+' totale: '+total_weight);
 // set
 $('#form_customers_view-offers_edit_input_quantity_weight').val(total_weight);
});");
}
// cancel button
$offer_form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id);
// check for search
if($_REQUEST['search']){
	// definitions
	$materials_array=array();
	$materials_distinct_array=array();
	// build modal tab
	$modal_tab=new strTab();
	$modal_tab->addItem(api_text("customers_view-offers-modal-materials-warehouse-select"),null,"active");
	// cycle all available warehouses
	foreach((new cCrmDivision($_REQUEST['division']))->getWarehouses() as $warehouse_f){
		// build materials search table
		$offer_order_search_table=new strTable(api_text("customers_view-offers-modal-materials-tr-unvalued"));
		$offer_order_search_table->addHeader("&nbsp;",null,16);
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-division"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-warehouse"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-code"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-grade"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-execution"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-profile"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-size"),"nowrap text-right");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-length"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-heattreath"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-tollerance"),"nowrap");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-specification"),null,"100%");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-lot"),"nowrap text-right");
    $offer_order_search_table->addHeader(api_text("customers_view-offers-modal-materials-th-stock"),"nowrap");
    $offer_order_search_table->addHeader("&nbsp;",null,16);
    // reset materials array
		$materials_array=array();
		// make size array
		$size_array=array($_REQUEST['size']);
		if($_REQUEST['size_max']){$size_array[]=$_REQUEST['size_max'];}
		//api_dump($size_array,"array");
		// get materials from stock
		foreach(api_crm_rfc_lookup_stock(null,$warehouse_f->codes_array,null,null,null,null,null,$_REQUEST['execution'],$_REQUEST['profile'],$_REQUEST['grade'],$size_array/*$_REQUEST['size']*/) as $material_fobj){
			// add material to materials array
			$materials_array[]=$material_fobj;
			// add material to materials distinct array
			if(!array_key_exists($material_fobj->material,$materials_distinct_array)){$materials_distinct_array[$material_fobj->material]=ltrim($material_fobj->material,"0");}
		}
		// get materials from registry
		foreach(api_crm_rfc_lookup_material($warehouse_f->codes_array,null,null,null,$_REQUEST['execution'],$_REQUEST['profile'],$_REQUEST['grade'],$size_array/*$_REQUEST['size']*/) as $material_fobj){
			// add material to materials array
			$materials_array[]=$material_fobj;
			// add material to materials distinct array
			if(!array_key_exists($material_fobj->material,$materials_distinct_array)){$materials_distinct_array[$material_fobj->material]=ltrim($material_fobj->material,"0");}
		}
		//api_dump($materials_array);
		// cycle all materials
		foreach($materials_array as $material_fobj){
			//foreach(api_crm_rfc_offer_materials_search(array("stock","materials"),$warehouse_f->codes_array,$_REQUEST['grade'],$_REQUEST['execution'],$_REQUEST['profile'],$_REQUEST['size']) as $material_fobj){
			// make url
			$url="?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_add&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id."&material=".$material_fobj->material;
			// make links
			$material_link=api_link("#",api_tag("samp",ltrim($material_fobj->material,"0")),null,null,false,null,null,"onClick=\"crm_customer_offer_material_select('".$material_fobj->material."','".$material_fobj->size."');\"");
			$size_link=api_link("#",api_tag("samp",str_replace(".",",",$material_fobj->size)),null,null,false,null,null,"onClick=\"crm_customer_offer_size_update('".$material_fobj->size."');\"");
			// make typology
			switch($material_fobj->typology){
				case "stock":$typology_td=api_icon("fa-cube",api_text("Stock"));break;
				case "materials":$typology_td=api_icon("fa-list-ul",api_text("Anagrafica"));break;
				default:$typology_td=null;
			}
			// make length
			$length_td=$material_fobj->length_min;
			if($material_fobj->length_max){$length_td.="-".$material_fobj->length_max;}
      // make locks
      $stock_lock=[];
      $stock_icon='&nbsp;'.api_icon("fa-square-o");
      $stock_lock['Totale']=api_number_format($material_fobj->weight_total,0);
      if($material_fobj->weight_engaged){$stock_lock['Impegnati']=api_number_format($material_fobj->weight_engaged,0);}
      if($material_fobj->weight_quality){$stock_lock['Blocco_Qualità']=api_number_format($material_fobj->weight_quality,0);}
      if($material_fobj->weight_locked){$stock_lock['Altri_Blocchi']=api_number_format($material_fobj->weight_locked,0);}
      if($material_fobj->weight_unusable){$stock_lock['Non_Usabile']=api_number_format($material_fobj->weight_unusable,0);}
      if($material_fobj->weight_return){$stock_lock['Da_Rendere']=api_number_format($material_fobj->weight_return,0);}
      if(count($stock_lock)>1){$stock_icon='&nbsp;'.api_link('#',api_icon("fa-square"),http_build_query($stock_lock,'',', '),null,true);}
			// build material row
			$offer_order_search_table->addRow();
			$offer_order_search_table->addRowField($typology_td);
			$offer_order_search_table->addRowField($material_fobj->division,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->warehouse,"nowrap");
			$offer_order_search_table->addRowField($material_link,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->grade,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->execution,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->profile,"nowrap");
			$offer_order_search_table->addRowField($size_link,"nowrap text-right");
			$offer_order_search_table->addRowField($length_td,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->heattreath,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->tollerance,"nowrap");
			$offer_order_search_table->addRowField($material_fobj->specification,"truncate-ellipsis");
			$offer_order_search_table->addRowField(api_tag("samp",ltrim($material_fobj->lot,0)),"nowrap text-right");
			$offer_order_search_table->addRowField(api_number_format($material_fobj->weight_total,0),"nowrap text-right");
			$offer_order_search_table->addRowField($stock_icon,"nowrap text-right");
		}
		// add stock table to tabs
		$modal_tab->addItem($warehouse_f->name,$offer_order_search_table->render());
	}
	// add material form input
	$offer_form->addField("select","material",api_text("customers_view-offers_edit-modal-ff-material"),$filters->material,api_text("customers_view-offers_edit-modal-ff-material-any"));
	foreach($materials_distinct_array as $material_fkey=>$material_flabel){$offer_form->addFieldOption($material_fkey,$material_flabel);}
	// add submit form control
	$offer_form->addControl("submit",api_text("customers_view-offers_edit-modal-fc-submit"),null,"btn-success",null,null,"name='save' value='1'");
	// material select jQuery script
	$app->addScript("function crm_customer_offer_material_select(material,size){\$('select[name=material]').val(material);\$('input[name=size]').val(size);}");
	$app->addScript("function crm_customer_offer_size_update(size){\$('input[name=size]').val(size);}");
}
// add form to modal body
$modal_body.=$offer_form->render(1);
// check for search and add search tabs to modal body
if($_REQUEST['search']){$modal_body.=$modal_tab->render();}
if($_REQUEST['save']){
	$customer_informations=api_crm_rfc_customer_informations($customer_obj->code,true);
}
// build offer modal window
$offer_modal=new strModal(api_text("customers_view-offers_edit-modal-title-".($selected_offer_obj->id?"edit":"add"),array($customer_obj->name,$selected_offer_obj->id)),null,"customers_view-offers_edit-modal");
$offer_modal->setBody($alert.$modal_body);
$offer_modal->setSize("large");
// add modal to application
$app->addModal($offer_modal);
// modal window opener jQuery scripts
$app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-offers_edit-modal').modal({show:true,backdrop:'static',keyboard:false});});");
