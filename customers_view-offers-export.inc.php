<?php
/**
 * CRM - Customers View (Offers - Export)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var strApplication $app
 * @var cCrmCustomer $customer_obj
 */
// definitions
$request_key=0;
$requests_array=array();
$first_division_salesarea_code=null;
// get customer informations
$customer_informations=api_crm_rfc_customer_informations($customer_obj->code);
//api_dump($customer_informations);
// build export form
$export_form=new strForm("?mod=".MODULE."&scr=customers_offers_export&idCustomer=".$customer_obj->id,"POST",null,"target='_blank'","customers_view-offers_export");
// fields
$export_form->addField("select","division",api_text("customers_view-offers_export-modal-ff-division"),api_parameter_default("division"),api_text("customers_view-offers_export-modal-ff-division-select"),null,null,null,"required");
foreach(api_crm_availableDivisions(true) as $division_f){$export_form->addFieldOption($division_f->id,$division_f->name);}
$export_form->addField("textarea","introduction",api_text("customers_view-offers_export-modal-ff-introduction"),api_text("customers_view-offers_export-modal-ff-introduction-default",array((new cCrmDivision(api_parameter_default("division")))->name,api_timestamp_format(time(),api_text("date")))),null,null,null,null,"rows='3' required");
$export_form->addField("checkbox","offers[]",api_text("customers_view-offers_export-modal-ff-offers"),null,api_text("customers_view-offers_export-modal-ff-offers-placeholder"));
// cycle all requests
foreach($customer_obj->getOffers(true) as $offer_obj){
	if($offer_obj->request_number.$offer_obj->request_date!=end($requests_array)){
		$request_key++;
		$requests_array[$request_key]=$offer_obj->request_number.$offer_obj->request_date;
		$export_form->addFieldOption("#",api_text("customers_view-offers_export-modal-fo-offers",array($offer_obj->request_number,api_date_format($offer_obj->request_date,api_text("date")))),"offers_select_all_".$request_key);
		$app->addScript("$('.offers_select_all_".$request_key."').change(function(){if($(this).prop('checked')==true){\$('.offers_select_".$request_key."').attr('checked',true);}});");
	}
	$export_form->addFieldOption($offer_obj->id,$offer_obj->id." - ".$offer_obj->grade." ".$offer_obj->profile." ".$offer_obj->size." ".$offer_obj->length,"offers_select_".$request_key);
	if(!$first_division_salesarea_code){
		$salesarea_obj=(new cCrmDivision($offer_obj->fkDivision))->getSalesareas()[$offer_obj->fkSalesarea];
		//api_dump($salesarea_obj);
		$first_division_salesarea_code=$salesarea_obj->commercialOrganization.'|'.$salesarea_obj->distributionChannel.'|'.$salesarea_obj->productSector;
		//api_dump($first_division_salesarea_code);
	}
}
$export_form->addField("select","paymentMethod",api_text("customers_view-offers_order-modal-ff-paymentMethod"),substr(reset($customer_informations->companies_array)->paymentMethod,0,1),api_text("customers_view-offers_order-modal-ff-paymentMethod-select"),null,null,null,"required");
foreach(api_crm_availablePaymentsMethods() as $paymentMethod_fobj){$export_form->addFieldOption($paymentMethod_fobj->id,$paymentMethod_fobj->label);}
$export_form->addField("select","paymentCondition",api_text("customers_view-offers_order-modal-ff-paymentCondition"),reset($customer_informations->companies_array)->paymentCondition,api_text("customers_view-offers_order-modal-ff-paymentCondition-select"),null,null,null,"required");
foreach(api_crm_availablePaymentsConditions() as $paymentCondition_fobj){$export_form->addFieldOption($paymentCondition_fobj->id,$paymentCondition_fobj->label);}
$export_form->addField("textarea","transport",api_text("customers_view-offers_export-modal-ff-transport"),$customer_informations->salesareas_array[$first_division_salesarea_code]->transportNote,api_text("customers_view-offers_export-modal-ff-transport-placeholder"));
$export_form->addField("textarea","various",api_text("customers_view-offers_export-modal-ff-various"),null,api_text("customers_view-offers_export-modal-ff-various-placeholder"));
$export_form->addField("textarea","conclusion",api_text("customers_view-offers_export-modal-ff-conclusion"),api_text("customers_view-offers_export-modal-ff-conclusion-default",$session->user->fullname),null,null,null,null,"rows='4' required");
// controls
$export_form->addControl("submit",api_text("form-fc-export"));
$export_form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);
// build offer modal window
$offer_modal=new strModal(api_text("customers_view-offers_export-modal-title",$customer_obj->name),null,"customers_view-offers_export-modal");
$offer_modal->setBody($export_form->render());
$offer_modal->setSize("large");
// add modal to application
$app->addModal($offer_modal);
// modal script
$app->addScript("$(function(){\$('#modal_customers_view-offers_export-modal').modal({show:true,backdrop:'static',keyboard:false});});");
$app->addScript("$(function(){\$('#form_customers_view-offers_export').submit(function(){\$('#modal_customers_view-offers_export-modal').modal('hide');});});");
