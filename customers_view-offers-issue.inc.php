<?php
/**
 * CRM - Customers View (Offers - Issue)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // get selected offer
 $selected_offer_obj=new cCrmOffer($_REQUEST['idOffer']);
 // make product
 $product_dd=$selected_offer_obj->grade." ".$selected_offer_obj->execution." ".$selected_offer_obj->profile." ".$selected_offer_obj->size." mm";
 if($selected_offer_obj->material){$product_dd=ltrim($selected_offer_obj->material,"0")." &rarr; ".$product_dd;}
 // build offer description list
 $offer_dl=new strDescriptionList("br","dl-horizontal");
 $offer_dl->addElement(api_text("customers_view-offers_issue-modal-dt-division"),(new cCrmDivision($selected_offer_obj->fkDivision))->name);
 $offer_dl->addElement(api_text("customers_view-offers_issue-modal-dt-request"),$selected_offer_obj->request_number." - ".api_date_format($selected_offer_obj->request_date,api_text("date")));
 $offer_dl->addElement(api_text("customers_view-offers_issue-modal-dt-product"),$product_dd);
 // build export form
 $issue_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_offer_issue&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,"POST");
 // fields
 $issue_form->addField("select","issue",api_text("customers_view-offers_issue-modal-ff-issue"),null,api_text("customers_view-offers_issue-modal-ff-issue-select"),null,null,null,"required");
 foreach(api_crm_offers_availableIssues() as $issue_f){$issue_form->addFieldOption($issue_f->code,$issue_f->text);}
 // controls
 $issue_form->addControl("submit",api_text("form-fc-save"));
 $issue_form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);
 // build offer view modal window
 $offer_modal=new strModal(api_text("customers_view-offers_issue-modal-title",array($selected_offer_obj->id,api_timestamp_format($selected_offer_obj->timestamp,api_text("date")))),null,"customers_view-offers_issue-modal");
 $offer_modal->setBody($offer_dl->render().$issue_form->render(2));
 $offer_modal->setFooter($operation_buttons);
 // add modal to application
 $app->addModal($offer_modal);
 // jQuery scripts
 $app->addScript("$(function(){\$('#modal_customers_view-offers_issue-modal').modal('show');});");

?>