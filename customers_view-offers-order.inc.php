<?php
/**
 * CRM - Customers View (Offers - Order)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

// get selected offer
$selected_offer_obj=new cCrmOffer($_REQUEST['idOffer']);
// check selected offer
if($selected_offer_obj->id){
  // get selected offer division
  $selected_offer_division_obj=new cCrmDivision($selected_offer_obj->fkDivision);
  // build offer add modal window
  $offer_modal=new strModal(api_text("customers_view-offers_order-modal-title",array($customer_obj->name,$selected_offer_obj->id)),null,"customers_view-offers_order-modal");
  // check for material
  if(!$_REQUEST['material']){
    // definitions
    $materials_array=array();
    // build modal tab
    $modal_tab=new strTab();
    $modal_tab->addItem(api_text("customers_view-offers-modal-materials-warehouse-select"),null,"active");
    // cycle all available locations
    foreach((new cCrmDivision($selected_offer_obj->fkDivision))->getWarehouses() as $warehouse_f){
      $offer_order_stock_table=new strTable(api_text("customers_view-offers-modal-materials-tr-unvalued"),null,api_text("customers_view-offers-modal-materials-caption"));
      $offer_order_stock_table->addHeader("&nbsp;",null,16);
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-division"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-warehouse"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-code"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-grade"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-execution"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-profile"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-size"),"nowrap text-right");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-length"),"nowrap");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-specification"),null,"100%");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-lot"),"nowrap text-right");
      $offer_order_stock_table->addHeader(api_text("customers_view-offers-modal-materials-th-stock"),"nowrap text-right");
      $offer_order_stock_table->addHeader("&nbsp;",null,16);
      // reset materials array
      $materials_array=array();
      // get materials from stock
      foreach(api_crm_rfc_lookup_stock(null,$warehouse_f->codes_array,null,null,$selected_offer_obj->material,null,null,$selected_offer_obj->execution,$selected_offer_obj->profile,$selected_offer_obj->grade,$selected_offer_obj->size) as $material_fobj){
        // add material to materials array
        $materials_array[]=$material_fobj;
      }
      // get materials from registry
      foreach(api_crm_rfc_lookup_material($warehouse_f->codes_array,null,$selected_offer_obj->material,null,$selected_offer_obj->execution,$selected_offer_obj->profile,$selected_offer_obj->grade,$selected_offer_obj->size) as $material_fobj){
        // add material to materials array
        $materials_array[]=$material_fobj;
      }
      //api_dump($materials_array);
      // cycle all materials
      foreach($materials_array as $material_fobj){
        // make url
        $url="?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_order&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id."&division=".$material_fobj->division."&warehouse=".$material_fobj->warehouse."&material=".$material_fobj->material."&lot=".$material_fobj->lot;
        // make length
        $length_td=$material_fobj->length_min;
        if($material_fobj->length_max){$length_td.="-".$material_fobj->length_max;}
        // make locks
        $stock_lock=[];
        $stock_icon='&nbsp;'.api_icon("fa-square-o");
        $stock_lock['Totale']=api_number_format($material_fobj->weight_total,0);
        if($material_fobj->weight_engaged){$stock_lock['Impegnati']=api_number_format($material_fobj->weight_engaged,0);}
        if($material_fobj->weight_quality){$stock_lock['Blocco_Qualità']=api_number_format($material_fobj->weight_quality,0);}
        if($material_fobj->weight_locked){$stock_lock['Altri_Blocchi']=api_number_format($material_fobj->weight_locked,0);}
        if($material_fobj->weight_unusable){$stock_lock['Non_Usabile']=api_number_format($material_fobj->weight_unusable,0);}
        if($material_fobj->weight_return){$stock_lock['Da_Rendere']=api_number_format($material_fobj->weight_return,0);}
        if(count($stock_lock)>1){$stock_icon='&nbsp;'.api_link('#',api_icon("fa-square"),http_build_query($stock_lock,'',', '),null,true);}
        // build material row
        $offer_order_stock_table->addRow();
        $offer_order_stock_table->addRowFieldAction($url,"fa-plus-square",api_text("customers_view-offers-modal-materials-td-select"));
        $offer_order_stock_table->addRowField(api_tag("samp",$material_fobj->division),"nowrap");
        $offer_order_stock_table->addRowField(api_tag("samp",$material_fobj->warehouse),"nowrap");
        $offer_order_stock_table->addRowField(api_tag("samp",ltrim($material_fobj->material,"0")),"nowrap");
        $offer_order_stock_table->addRowField($material_fobj->grade,"nowrap");
        $offer_order_stock_table->addRowField($material_fobj->execution,"nowrap");
        $offer_order_stock_table->addRowField($material_fobj->profile,"nowrap");
        $offer_order_stock_table->addRowField($material_fobj->size,"nowrap text-right");
        $offer_order_stock_table->addRowField($length_td,"nowrap");
        $offer_order_stock_table->addRowField($material_fobj->specification,"truncate-ellipsis");
        $offer_order_stock_table->addRowField(api_tag("samp",ltrim($material_fobj->lot,0)),"nowrap text-right");
        $offer_order_stock_table->addRowField(api_number_format($material_fobj->weight_total,0),"nowrap text-right");
        $offer_order_stock_table->addRowField($stock_icon,"nowrap text-right");
      }
      // add stock table to tabs
      $modal_tab->addItem($warehouse_f->name,$offer_order_stock_table->render());
    }
    // renderize tab into modal
    $offer_modal->setBody($modal_tab->render());
    $offer_modal->setSize("large");
  }else{
    // check for order
    if(!$_REQUEST['order']){
      // definitions
      $selected_offer_division_salesareas_array=array();
      // get selected offer division salesareas codes
      foreach($selected_offer_division_obj->getSalesareas() as $salesarea_fobj){$selected_offer_division_salesareas_array[]=$salesarea_fobj->code;}
      // build offer add form
      $offer_form=new strForm("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_order&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,"POST",null,null,"customers_view-offers");
      $offer_form->addField("hidden","division",null,$_REQUEST['division']);
      $offer_form->addField("hidden","warehouse",null,$_REQUEST['warehouse']);
      $offer_form->addField("hidden","material",null,$_REQUEST['material']);
      $offer_form->addField("hidden","lot",null,$_REQUEST['lot']);
      $offer_form->addField("radio","order",api_text("customers_view-offers_order-modal-ff-order"),null,api_text("customers_view-offers_order-modal-ff-order-placeholder"),null,null,null,"required");
      $offer_form->addFieldOption("new",api_text("customers_view-offers_order-modal-ff-order-new"));
      foreach(api_crm_rfc_customer_orders($customer_obj->code,$selected_offer_division_salesareas_array) as $order_fobj){$offer_form->addFieldOption($order_fobj->number,api_tag("samp",ltrim($order_fobj->number,"0"))." del ".api_timestamp_format($order_fobj->timestamp,api_text("date"))/*." ".api_tag("small","[".api_tag("samp",$order_fobj->salesarea_code)."]")*/);}
      $offer_form->addControl("submit",api_text("customers_view-offers_order-modal-fc-continue"));
      $offer_form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id);
      // add offer form to offer modal
      $offer_modal->setBody($offer_form->render(2));
    }else{
      // get customer information
      $customer_informations=api_crm_rfc_customer_informations($customer_obj->code,true);
      //var_dump($customer_informations);
      // get default customer supplier
      $customer_supplier=end($customer_informations->suppliers_array)->code;
      // build offer add form
      $offer_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_offer_order&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,"POST",null,null,"customers_view-offers");
      // check for order
      if($_REQUEST['order']=="new"){
        // new order
        $offer_form->addField("hidden","order",null,"new");
        /* se dovesse servire farglieli mettere singolarmente a mano ma non credo per ora..
        $offer_form->addField("text","AUART",api_text("customers_view-offers_order-modal-ff-material"),null,api_text("customers_view-offers_order-modal-ff-material-placeholder"),null,null,null,"required");
        $offer_form->addField("text","VKORG",api_text("customers_view-offers_order-modal-ff-material"),null,api_text("customers_view-offers_order-modal-ff-material-placeholder"),null,null,null,"required");
        $offer_form->addField("text","VTWEG",api_text("customers_view-offers_order-modal-ff-material"),null,api_text("customers_view-offers_order-modal-ff-material-placeholder"),null,null,null,"required");
        $offer_form->addField("text","SPART",api_text("customers_view-offers_order-modal-ff-material"),null,api_text("customers_view-offers_order-modal-ff-material-placeholder"),null,null,null,"required");
        $offer_form->addField("text","VKBUR",api_text("customers_view-offers_order-modal-ff-material"),null,api_text("customers_view-offers_order-modal-ff-material-placeholder"),null,null,null,"required");
        // jQuery scripts
        $app->addScript("/* Salesarea selection /\n$(function(){\$('#form_customers_view-offers_input_variant').on('change',function(){
          alert(this.value);
          $('#form_customers_view-offers_input_AUART').val(this.value.substr(0,4));
          $('#form_customers_view-offers_input_VKORG').val(this.value.substr(5,4));
          $('#form_customers_view-offers_input_VTWEG').val(this.value.substr(9,2));
          $('#form_customers_view-offers_input_SPART').val(this.value.substr(11,2));
          $('#form_customers_view-offers_input_VKBUR').val(this.value.substr(13,4));
        });});");
        */
        $offer_form->addField("static","division",api_text("customers_view-offers_order-modal-ff-division"),(new cCrmDivision($selected_offer_obj->fkDivision))->name);
        $offer_form->addField("select","salesarea",api_text("customers_view-offers_order-modal-ff-salesarea"),(new cCrmSalesarea($selected_offer_obj->fkSalesarea))->code,api_text("customers_view-offers_order-modal-ff-salesarea-select"),null,null,null,"required");
        foreach($selected_offer_division_obj->getSalesareas() as $salesarea_fobj){$offer_form->addFieldOption($salesarea_fobj->code,$salesarea_fobj->label);}
        $offer_form->addField("select","salesteam",api_text("customers_view-offers_order-modal-ff-salesteam"),(api_parameter_default("salesteam")?api_parameter_default("salesteam"):end($customer_informations->commercialReferents)),api_text("customers_view-offers_order-modal-ff-salesteam-select"),null,null,null,"required");
        foreach(api_crm_rfc_codings("SALES_TEAM") as $salesteam_fobj){$offer_form->addFieldOption($salesteam_fobj->id,strtoupper($salesteam_fobj->label));}
        $offer_form->addField("select","supplier",api_text("customers_view-offers_order-modal-ff-supplier"),($customer_supplier?$customer_supplier:api_parameter_default("supplier")),api_text("customers_view-offers_order-modal-ff-supplier-select"),null,null,null,"required");
        foreach($customer_informations->suppliers_array as $supplier_fobj){$offer_form->addFieldOption($supplier_fobj->code,$supplier_fobj->name." [".ltrim($supplier_fobj->code,0)."]");}
        foreach(api_crm_rfc_supplier_search(null,"FO01") as $supplier_fobj){if($supplier_fobj->deleted){continue;}$offer_form->addFieldOption($supplier_fobj->code,$supplier_fobj->name." [".ltrim($supplier_fobj->code,0)."]");}
        $offer_form->addField("select","receiver",api_text("customers_view-offers_order-modal-ff-receiver"),null,api_text("customers_view-offers_order-modal-ff-receiver-select"),null,null,null,"required");
        foreach($customer_informations->receivers_array as $receiver_fobj){if($receiver_fobj->deleted){continue;}$offer_form->addFieldOption($receiver_fobj->code,$receiver_fobj->name." - ".$receiver_fobj->address." [".ltrim($receiver_fobj->code,"0")."]");}
        $offer_form->addField("select","billing",api_text("customers_view-offers_order-modal-ff-billing"),null,api_text("customers_view-offers_order-modal-ff-billing-select"),null,null,null,"required");
        foreach($customer_informations->billings_array as $billing_fobj){if($billing_fobj->deleted){continue;}$offer_form->addFieldOption($billing_fobj->code,$billing_fobj->name." - ".$billing_fobj->address." [".ltrim($billing_fobj->code,"0")."]");}
        $offer_form->addField("select","payment",api_text("customers_view-offers_order-modal-ff-payment"),substr($customer_informations->companies_array[$selected_offer_division_obj->company]->paymentMethod,0,1),api_text("customers_view-offers_order-modal-ff-payment-select"),null,null,null,"required");
        foreach(api_crm_availablePaymentsMethods() as $payment_fobj){$offer_form->addFieldOption($payment_fobj->id,$payment_fobj->label);}
      }else{
        // selected order
        $offer_form->addField("static","order",api_text("customers_view-offers_order-modal-ff-order"),$_REQUEST['order']);
      }
      $offer_form->addField("static","location",api_text("customers_view-offers_order-modal-ff-location"),$_REQUEST['division']." &rarr; ".$_REQUEST['warehouse']);
      $offer_form->addField("hidden","division",api_text("customers_view-offers_order-modal-ff-division"),$_REQUEST['division']);
      $offer_form->addField("hidden","warehouse",api_text("customers_view-offers_order-modal-ff-warehouse"),$_REQUEST['warehouse']);
      if(!$_REQUEST['warehouse']){$offer_form->addField("text","warehouse",api_text("customers_view-offers_order-modal-ff-warehouse"),$_REQUEST['warehouse'],null,null,null,null,"required maxlength='4'");}
      $offer_form->addField("static","material",api_text("customers_view-offers_order-modal-ff-material"),$_REQUEST['material']);
      $offer_form->addField("text","request_number",api_text("customers_view-offers_order-modal-ff-request_number"),$selected_offer_obj->request_number,null,null,null,null,"required");
      $offer_form->addField("date","request_date",api_text("customers_view-offers_order-modal-ff-request_date"),$selected_offer_obj->request_date,null,null,null,null,"required");
      $offer_form->addField("text","request_material",api_text("customers_view-offers_order-modal-ff-request_material"),$selected_offer_obj->request_material);
      $offer_form->addField("textarea","lot",api_text("customers_view-offers_order-modal-ff-lot"),$_REQUEST['lot'],api_text("customers_view-offers_order-modal-ff-lot-placeholder"));
      $offer_form->addField("date","availability",api_text("customers_view-offers_order-modal-ff-availability"),$selected_offer_obj->availability,null,null,null,null,"required");
      $offer_form->addField("textarea","texts",api_text("customers_view-offers_order-modal-ff-texts"),$selected_offer_obj->texts,api_text("customers_view-offers_order-modal-ff-texts-placeholder"));
      $offer_form->addControl("submit",api_text("customers_view-offers_order-modal-fc-ordain"));
      $offer_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
      // add offer form to offer modal
      $offer_modal->setBody($offer_form->render(2));
      // order buttons disabler jQuery script
      $app->addScript("$(document).on('submit','#form_customers_view-offers',function(){\n $('#form_customers_view-offers_control_submit').attr('disabled',true);\n $('#form_customers_view-offers_control_button_2').attr('disabled',true);\n});");
    }
  }
  // add modal to application
  $app->addModal($offer_modal);
  // modal window opener jQuery scripts
  $app->addScript("$(function(){\$('#modal_customers_view-offers_order-modal').modal({show:true,backdrop:'static',keyboard:false});});");
}
