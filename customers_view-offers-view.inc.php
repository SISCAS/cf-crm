<?php
/**
 * CRM - Customers View (Offers - View)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // get selected offer
 $selected_offer_obj=new cCrmOffer($_REQUEST['idOffer']);
 // make product
 $product_dd=$selected_offer_obj->grade." ".$selected_offer_obj->execution." ".$selected_offer_obj->profile." ".$selected_offer_obj->size." mm ".$selected_offer_obj->heattreath.($selected_offer_obj->length?" ".$selected_offer_obj->length." mm":null);
 if($selected_offer_obj->material){$product_dd=ltrim($selected_offer_obj->material,"0")." &rarr; ".$product_dd;}
 // make confirmation
 if($selected_offer_obj->confirmation){$confirmation_dd=api_tag("samp",$selected_offer_obj->confirmation);}else{$confirmation_dd=null;}
 // build offer description list
 $offer_dl=new strDescriptionList("br","dl-horizontal");
 //$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-offer"),$selected_offer_obj->id." - ".api_timestamp_format($selected_offer_obj->timestamp,api_text("date")));
 if($customer_obj->code=="9999999999"){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-customer_name"),$selected_offer_obj->customer_name);}
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-division"),(new cCrmDivision($selected_offer_obj->fkDivision))->name);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-salesarea"),(new cCrmSalesarea($selected_offer_obj->fkSalesarea))->name);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-request"),$selected_offer_obj->request_number." - ".api_date_format($selected_offer_obj->request_date,api_text("date")));
 if($selected_offer_obj->request_material){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-request_material"),$selected_offer_obj->request_material);}
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-request_grade"),$selected_offer_obj->request_grade);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-product"),$product_dd);
 /*$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-grade"),$selected_offer_obj->grade);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-execution"),$selected_offer_obj->execution." ".api_crm_offers_availableExecutions()[$selected_offer_obj->execution]->text);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-profile"),$selected_offer_obj->profile);
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-size"),$selected_offer_obj->size." mm");*/
 if($selected_offer_obj->heattreath){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-heattreath"),$selected_offer_obj->heattreath);}
 if($selected_offer_obj->tollerance){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-tollerance"),$selected_offer_obj->tollerance);}
 if($selected_offer_obj->length_cutted){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-length_cutted"),$selected_offer_obj->length_cutted." mm (".$selected_offer_obj->length_tollerance." mm)");}
 if($selected_offer_obj->quantity_pieces){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-quantity_pieces"),api_number_format($selected_offer_obj->quantity_pieces,2,null,false,true));}
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-quantity_weight"),api_number_format($selected_offer_obj->quantity_weight,2,null,false,true)." kg");
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-price_list"),api_number_format($selected_offer_obj->price_list,2,"&euro;",true)." x kg");
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-price_offer"),api_number_format($selected_offer_obj->price_offer,2,"&euro;",true)." x kg");
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-price_alloy"),api_text("customers_view-offers_view-modal-dd-price_alloy-".($selected_offer_obj->price_alloy?"yes":"no")));
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-price_cutting"),api_number_format($selected_offer_obj->price_cutting,2,"&euro;",true));
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-availability"),$selected_offer_obj->availability);
 if($selected_offer_obj->note){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-note"),nl2br($selected_offer_obj->note));}
 if($selected_offer_obj->texts){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-texts"),nl2br($selected_offer_obj->texts));}
 if($selected_offer_obj->important){$offer_dl->addElement(api_icon("fa-warning"),api_text("customers_view-offers_view-modal-dd-important"));}
 if($confirmation_dd){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-confirmation"),$confirmation_dd);}
 $offer_dl->addSeparator("hr");
 $offer_dl->addElement(api_text("dl-dt-add"),api_text("dl-dd-add",array((new cUser($selected_offer_obj->addFkUser))->fullname,api_timestamp_format($selected_offer_obj->addTimestamp,api_text("datetime")))));
 if($selected_offer_obj->updTimestamp){$offer_dl->addElement(api_text("dl-dt-upd"),api_text("dl-dd-upd",array((new cUser($selected_offer_obj->updFkUser))->fullname,api_timestamp_format($selected_offer_obj->updTimestamp,api_text("datetime")))));}
 if($selected_offer_obj->cnfTimestamp){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-cnf"),api_text("customers_view-offers_view-modal-dd-cnf",array((new cUser($selected_offer_obj->cnfFkUser))->fullname,api_timestamp_format($selected_offer_obj->cnfTimestamp,api_text("datetime")))));}
 if($selected_offer_obj->status!="ordered" && $selected_offer_obj->expirationDate){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-expirationDate"),api_date_format($selected_offer_obj->expirationDate,api_text("date")));}
 $offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-status"),$selected_offer_obj->getStatus());
 if($selected_offer_obj->issue){$offer_dl->addElement(api_text("customers_view-offers_view-modal-dt-issue"),$selected_offer_obj->getIssue());}
 // make operation buttons
 $operation_buttons=api_link("?mod=".MODULE."&scr=submit&act=customer_offer_remove&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,api_text("customers_view-offers_view-modal-btn-delete"),null,"btn btn-danger",false,api_text("customers_view-offers_view-modal-btn-delete-confirm"));
 if($selected_offer_obj->status!="ordered"){
  $operation_buttons=api_link("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_issue&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,api_text("customers_view-offers_view-modal-btn-issue"),null,"btn btn-info").$operation_buttons;
  if($customer_obj->code=="9999999999"){$operation_buttons=api_link("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_customer&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,api_text("customers_view-offers_view-modal-btn-customer"),null,"btn btn-warning").$operation_buttons;}
  else{$operation_buttons=api_link("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_order&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,api_text("customers_view-offers_view-modal-btn-order"),null,"btn btn-success").$operation_buttons;}
  $operation_buttons=api_link("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_edit&idCustomer=".$customer_obj->id."&idOffer=".$selected_offer_obj->id,api_text("customers_view-offers_view-modal-btn-edit"),null,"btn btn-default").$operation_buttons;
 }
 // build offer view modal window
 $offer_modal=new strModal(api_text("customers_view-offers_view-modal-title",array($selected_offer_obj->id,api_timestamp_format($selected_offer_obj->timestamp,api_text("date")))),null,"customers_view-offers_view-modal");
 $offer_modal->setBody($offer_dl->render());
 $offer_modal->setFooter($operation_buttons);
 // add modal to application
 $app->addModal($offer_modal);
 // jQuery scripts
 $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-offers_view-modal').modal('show');});");

?>