<?php
/**
 * CRM - Customers View (Offers)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build offers table
 $offers_table=new strTable(api_text("customers_view-offers-tr-unvalued"),null,null,"jquery_test"); /*id=table_jquery_test*/
 $offers_table->addHeader(api_link("?mod=".MODULE."&scr=offers_list&filter_customer[]=".$customer_obj->id,api_icon("fa-archive",api_text("customers_view-offers-th-all"),"hidden-link"),null,null,false,null,null,null,"_blank"),"text-center",16);
 $offers_table->addHeader(api_text("customers_view-offers-th-id"),"nowrap");
 //$offers_table->addHeader(api_text("customers_view-offers-th-timestamp"),"nowrap");
 if($customer_obj->code=="9999999999"){$offers_table->addHeader(api_text("customers_view-offers-th-customer_name"),"nowrap");}
 $offers_table->addHeader(api_text("customers_view-offers-th-request"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-grade"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-execution"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-profile"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-size"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-length"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-quantity_pieces"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-quantity_weight"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-price_list"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-price_offer"),"nowrap text-right");
 $offers_table->addHeader(api_text("customers_view-offers-th-availability"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-order"),"nowrap");
 $offers_table->addHeader(api_text("customers_view-offers-th-user"),"text-right","100%");
 $offers_table->addHeader(api_text("customers_view-offers-th-division"),"nowrap text-right");
 $offers_table->addHeader("&nbsp;",null,16);
 // cycle customer offers
 foreach($customer_obj->getOffers(true) as $offer_obj){
  // make table row class
  $tr_class_array=array();
  if($offer_obj->id==$_REQUEST['idOffer']){$tr_class_array[]="currentrow";}
  if($offer_obj->important){$tr_class_array[]="success";}
  if($offer_obj->deleted){$tr_class_array[]="deleted";}
  // make confirmation
  if($offer_obj->confirmation){$confirmation_dd=api_tag("samp",$offer_obj->confirmation);}else{$confirmation_dd=null;}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_view&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id,"fa-info-circle",api_text("customers_view-offers-td-view"));
  if($offer_obj->status!="ordered"){
   $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_edit&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id,"fa-pencil",api_text("customers_view-offers-td-edit"));
   if($customer_obj->code!="9999999999"){$ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_order&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id,"fa-check-circle",api_text("customers_view-offers-td-order"));}
   $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_offer_remove&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id,"fa-trash",api_text("customers_view-offers-td-remove"),true,api_text("customers_view-offers-td-remove-confirm"));
  }
  // add offer row
  $offers_table->addRow(implode(" ",$tr_class_array));
  $offers_table->addRowField($offer_obj->getStatus(true,false),"nowrap");
  $offers_table->addRowField(api_tag("samp",$offer_obj->id),"nowrap");
  //$offers_table->addRowField(api_timestamp_format($offer_obj->timestamp,api_text("date")),"nowrap");
  if($customer_obj->code=="9999999999"){$offers_table->addRowField($offer_obj->customer_name,"nowrap");}
  $offers_table->addRowField($offer_obj->request_number." - ".api_date_format($offer_obj->request_date,api_text("date")),"nowrap");
  $offers_table->addRowField($offer_obj->grade,"nowrap");
  $offers_table->addRowField($offer_obj->execution,"nowrap");
  $offers_table->addRowField($offer_obj->profile,"nowrap");
  $offers_table->addRowField($offer_obj->size,"nowrap text-right");
  $offers_table->addRowField(($offer_obj->length_cutted?$offer_obj->length_cutted:$offer_obj->length),"nowrap text-right");
  $offers_table->addRowField(api_number_format($offer_obj->quantity_pieces,0,null,false,false,"-"),"nowrap text-right");
  $offers_table->addRowField(api_number_format($offer_obj->quantity_weight,2,null,false,true),"nowrap text-right");
  $offers_table->addRowField(api_number_format($offer_obj->price_list,2,"&euro;",true,false,"-"),"nowrap text-right");
  $offers_table->addRowField(api_number_format($offer_obj->price_offer,2,"&euro;",true).($offer_obj->price_alloy?api_link("#","+",api_text("customers_view-offers-td-price_alloy"),null,true):null),"nowrap text-right");
  $offers_table->addRowField($offer_obj->availability,"nowrap");
  $offers_table->addRowField($confirmation_dd,"nowrap");
  $offers_table->addRowField((new cUser($offer_obj->addFkUser))->lastname,"truncate-ellipsis text-right");
  $offers_table->addRowField((new cCrmDivision($offer_obj->fkDivision))->name,"nowrap text-right");
  $offers_table->addRowField($ob_obj->render(),"text-right");
 }

 // view action
 if(ACTION=="offer_view"){require_once(MODULE_PATH."customers_view-offers-view.inc.php");}
 // offer add or edit action
 if(in_array(ACTION,array("offer_add","offer_edit"))){require_once(MODULE_PATH."customers_view-offers-edit.inc.php");}
 // issue action
 if(ACTION=="offer_issue"){require_once(MODULE_PATH."customers_view-offers-issue.inc.php");}
 // offer customer action
 if(ACTION=="offer_customer"){require_once(MODULE_PATH."customers_view-offers-customer.inc.php");}
 // offer order action
 if(ACTION=="offer_order"){require_once(MODULE_PATH."customers_view-offers-order.inc.php");}
 // offer export action
 if(ACTION=="offer_export"){require_once(MODULE_PATH."customers_view-offers-export.inc.php");}

?>