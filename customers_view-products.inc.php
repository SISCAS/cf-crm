<?php
/**
 * CRM - Customers View (Products)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build products table
 $products_table=new strTable(api_text("customers_view-products-tr-unvalued"));
 $products_table->addHeader(api_text("customers_view-products-th-grade"),"nowrap");
 $products_table->addHeader(api_text("customers_view-products-th-execution"),"nowrap");
 $products_table->addHeader(api_text("customers_view-products-th-profile"),"nowrap");
 $products_table->addHeader(api_text("customers_view-products-th-size"),"nowrap text-right");
 $products_table->addHeader(api_text("customers_view-products-th-quantity"),"nowrap text-right");
 $products_table->addHeader(api_text("customers_view-products-th-division"),"text-right","100%");
 $products_table->addHeader("&nbsp;",null,16);
 // cycle customer products
 foreach($customer_obj->getProducts() as $product_obj){
  // make table row class
  $tr_class_array=array();
  if($product_obj->id==$_REQUEST['idProduct']){$tr_class_array[]="currentrow";}
  if($product_obj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=products&act=product_edit&idCustomer=".$customer_obj->id."&idProduct=".$product_obj->id,"fa-pencil",api_text("customers_view-products-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_product_remove&tab=products&idCustomer=".$customer_obj->id."&idProduct=".$product_obj->id,"fa-trash",api_text("customers_view-products-td-remove"),true,api_text("customers_view-products-td-remove-confirm"));
  // add product row
  $products_table->addRow(implode(" ",$tr_class_array));
	$products_table->addRowField($product_obj->grade,"nowrap");
	$products_table->addRowField($product_obj->execution,"nowrap");
	$products_table->addRowField($product_obj->profile,"nowrap");
	$products_table->addRowField($product_obj->size,"nowrap text-right");
	$products_table->addRowField(api_number_format(($product_obj->quantity/1000),0)." ton","nowrap text-right");
  $products_table->addRowField((new cCrmDivision($product_obj->fkDivision))->name,"nowrap text-right");
  $products_table->addRowField($ob_obj->render(),"text-right");
 }

 // check product actions
 if(in_array(ACTION,array("product_add","product_edit"))){
  // get selected product
  $selected_product_obj=new cCrmProduct($_REQUEST['idProduct']);
  // build product add form
  $product_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_product_save&idCustomer=".$customer_obj->id."&idProduct=".$selected_product_obj->id,"POST",null,null,"customers_view-products");
  $product_form->addField("select","fkDivision",api_text("customers_view-products_modal-ff-fkDivision"),($selected_product_obj->fkDivision?$selected_product_obj->fkDivision:api_parameter_default("division")),api_text("customers_view-products_modal-ff-fkDivision-placeholder"),null,null,null,"required");
  foreach(api_crm_availableDivisions(true) as $division_fobj){$product_form->addFieldOption($division_fobj->id,$division_fobj->name);}
  $product_form->addField("select","grade",api_text("customers_view-products_modal-ff-grade"),$selected_product_obj->grade,api_text("customers_view-products_modal-ff-grade-placeholder"),null,null,null,"required");
  foreach(api_crm_offers_availableGrades() as $grade_f){$product_form->addFieldOption($grade_f->code,$grade_f->text);}
  $product_form->addField("select","execution",api_text("customers_view-products_modal-ff-execution"),$selected_product_obj->execution,api_text("customers_view-products_modal-ff-execution-placeholder"));
  foreach(api_crm_offers_availableExecutions() as $execution_f){$product_form->addFieldOption($execution_f->code,$execution_f->code." ".$execution_f->text);}
  $product_form->addField("select","profile",api_text("customers_view-products_modal-ff-profile"),$selected_product_obj->profile,api_text("customers_view-products_modal-ff-profile-placeholder"));
  foreach(api_crm_offers_availableProfiles() as $profile_f){$product_form->addFieldOption($profile_f->code,$profile_f->text);}
	$product_form->addField("number","size",api_text("customers_view-products_modal-ff-size"),$selected_product_obj->size,api_text("customers_view-products_modal-ff-size-placeholder"));
	$product_form->addField("number","quantity",api_text("customers_view-products_modal-ff-quantity"),($selected_product_obj->quantity/1000),api_text("customers_view-products_modal-ff-quantity-placeholder"));
  $product_form->addControl("submit",api_text("form-fc-save"));
  $product_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build product add modal window
  $product_modal=new strModal(($selected_product_obj->id?api_text("customers_view-products_modal-title-edit",$customer_obj->name):api_text("customers_view-products_modal-title-add",$customer_obj->name)),null,"customers_view-products_modal");
  $product_modal->setBody($product_form->render());
  // add modal to application
  $app->addModal($product_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-products_modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>