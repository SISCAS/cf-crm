<?php
/**
 * CRM - Customers View (Relationships)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build relationships table
 $relationships_table=new strTable(api_text("customers_view-relationships-tr-unvalued"));
 $relationships_table->addHeader("&nbsp;",null,16);
 $relationships_table->addHeader(api_text("customers_view-relationships-th-timestamp"),"nowrap");
 $relationships_table->addHeader("&nbsp;",null,16);
 $relationships_table->addHeader(api_text("customers_view-relationships-th-contacts"),"nowrap");
 $relationships_table->addHeader(api_text("customers_view-relationships-th-description"),null,"100%");
 $relationships_table->addHeader(api_text("customers_view-relationships-th-user"),"nowrap text-right");
 $relationships_table->addHeader(api_text("customers_view-relationships-th-division"),"nowrap text-right");
 $relationships_table->addHeader("&nbsp;",null,16);
 // cycle customer relationships
 foreach($customer_obj->getRelationships() as $relationship_obj){
  // make table row class
  $tr_class_array=array();
  if($relationship_obj->id==$_REQUEST['idRelationship']){$tr_class_array[]="currentrow";}
  if($relationship_obj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=relationships&act=relationship_view&idCustomer=".$customer_obj->id."&idRelationship=".$relationship_obj->id,"fa-info-circle",api_text("customers_view-relationships-td-view"));
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=relationships&act=relationship_edit&idCustomer=".$customer_obj->id."&idRelationship=".$relationship_obj->id,"fa-pencil",api_text("customers_view-relationships-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_relationship_remove&tab=relationships&idCustomer=".$customer_obj->id."&idRelationship=".$relationship_obj->id,"fa-trash",api_text("customers_view-relationships-td-remove"),true,api_text("customers_view-relationships-td-remove-confirm"));
  // add relationship row
  $relationships_table->addRow(implode(" ",$tr_class_array));
  $relationships_table->addRowField($relationship_obj->getTypology(true,false),"nowrap");
  $relationships_table->addRowField(api_timestamp_format($relationship_obj->timestamp,api_text("datetime")),"nowrap");
  $relationships_table->addRowField($relationship_obj->getFeeling(true,false),"nowrap text-right");
  $relationships_table->addRowField($relationship_obj->contacts,"nowrap");
  $relationships_table->addRowField($relationship_obj->description,"truncate-ellipsis");
  $relationships_table->addRowField((new cUser($relationship_obj->addFkUser))->lastname,"nowrap text-right");
  $relationships_table->addRowField((new cCrmDivision($relationship_obj->fkDivision))->name,"nowrap text-right");
  $relationships_table->addRowField($ob_obj->render(),"text-right");
 }

 // check relationship actions
 if(ACTION=="relationship_view" && $_REQUEST['idRelationship']){
  // get selected relationship
  $selected_relationship_obj=new cCrmRelationship($_REQUEST['idRelationship']);
  // build relationship description list
  $relationship_dl=new strDescriptionList("br","dl-horizontal");
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-timestamp"),api_timestamp_format($selected_relationship_obj->timestamp,api_text("datetime")));
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-typology"),$selected_relationship_obj->getTypology(true,true));
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-contacts"),$selected_relationship_obj->contacts);
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-description"),nl2br($selected_relationship_obj->description));
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-feeling"),$selected_relationship_obj->getFeeling(true,true));
  $relationship_dl->addSeparator("hr");
  $relationship_dl->addElement(api_text("customers_view-relationships_modal-dt-user"),(new cUser($selected_relationship_obj->addFkUser))->fullname);
  //$relationship_dl->addElement(api_text("dl-dt-add"),api_text("dl-dd-add",array((new cUser($relationship_obj->addFkUser))->fullname,api_timestamp_format($relationship_obj->addTimestamp,api_text("datetime")))));
  //if($relationship_obj->updTimestamp){$relationship_dl->addElement(api_text("dl-dt-upd"),api_text("dl-dd-upd",array((new cUser($relationship_obj->updFkUser))->fullname,api_timestamp_format($relationship_obj->updTimestamp,api_text("datetime")))));}
  // build relationship view modal window
  $relationship_modal=new strModal(api_text("customers_view-relationships_modal-title-view",$customer_obj->name),null,"customers_view-relationships_modal");
  $relationship_modal->setBody($relationship_dl->render());
  // add modal to application
  $app->addModal($relationship_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-relationships_modal').modal('show');});");
 }

 // check relationship actions
 if(in_array(ACTION,array("relationship_add","relationship_edit"))){
  // get selected relationship
  $selected_relationship_obj=new cCrmRelationship($_REQUEST['idRelationship']);
  // build relationship add form
  $relationship_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_relationship_save&idCustomer=".$customer_obj->id."&idRelationship=".$selected_relationship_obj->id,"POST",null,null,"customers_view-relationships");
  $relationship_form->addField("select","fkDivision",api_text("customers_view-relationships_modal-ff-fkDivision"),($selected_relationship_obj->fkDivision?$selected_relationship_obj->fkDivision:api_parameter_default("division")),api_text("customers_view-relationships_modal-ff-fkDivision-placeholder"),null,null,null,"required");
  foreach(api_crm_availableDivisions(true) as $division_fobj){$relationship_form->addFieldOption($division_fobj->id,$division_fobj->name);}
  $relationship_form->addField("datetime","timestamp",api_text("customers_view-relationships_modal-ff-timestamp"),api_timestamp_format($selected_relationship_obj->timestamp,"Y-m-d\TH:i"),null,null,null,null,"required");
  $relationship_form->addField("select","typology",api_text("customers_view-relationships_modal-ff-typology"),$selected_relationship_obj->typology,api_text("customers_view-relationships_modal-ff-typology-placeholder"),null,null,null,"required");
  foreach(api_crm_relationships_availableTypologies() as $typology_f){$relationship_form->addFieldOption($typology_f->code,$typology_f->text);}
  $relationship_form->addField("text","contacts",api_text("customers_view-relationships_modal-ff-contacts"),$selected_relationship_obj->contacts,api_text("customers_view-relationships_modal-ff-contacts-placeholder"));
  $relationship_form->addField("textarea","description",api_text("customers_view-relationships_modal-ff-description"),$selected_relationship_obj->description,api_text("customers_view-relationships_modal-ff-description-placeholder"),null,null,null,"required");
  $relationship_form->addField("radio","feeling",api_text("customers_view-relationships_modal-ff-feeling"),$selected_relationship_obj->feeling,null,null,"radio-inline",null,"required");
  foreach(api_crm_relationships_availableFeelings() as $feeling_f){$relationship_form->addFieldOption($feeling_f->code,$feeling_f->icon." ".$feeling_f->text);}
  $relationship_form->addControl("submit",api_text("form-fc-save"));
  $relationship_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build relationship add modal window
  $relationship_modal=new strModal(($selected_relationship_obj->id?api_text("customers_view-relationships_modal-title-edit",$customer_obj->name):api_text("customers_view-relationships_modal-title-add",$customer_obj->name)),null,"customers_view-relationships_modal");
  $relationship_modal->setBody($relationship_form->render(1));
  // add modal to application
  $app->addModal($relationship_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-relationships_modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>