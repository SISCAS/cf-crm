<?php
/**
 * CRM - Customers View (Specifications)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build specifications table
 $specifications_table=new strTable(api_text("customers_view-specifications-tr-unvalued"));
 $specifications_table->addHeader("&nbsp;",null,16);
 $specifications_table->addHeader(api_text("customers_view-specifications-th-code"),"nowrap");
 $specifications_table->addHeader(api_text("customers_view-specifications-th-date"),"nowrap");
 $specifications_table->addHeader(api_text("customers_view-specifications-th-description"),"nowrap");
 $specifications_table->addHeader(api_text("customers_view-specifications-th-note"),null,"100%");
 $specifications_table->addHeader("&nbsp;",null,16);
 // cycle customer specifications
 foreach($customer_obj->getSpecifications() as $specification_obj){
  // make table row class
  $tr_class_array=array();
  if($specification_obj->id==$_REQUEST['idSpecification']){$tr_class_array[]="currentrow";}
  if($specification_obj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=specifications&act=specification_view&idCustomer=".$customer_obj->id."&idSpecification=".$specification_obj->id,"fa-info-circle",api_text("customers_view-specifications-td-view"));
  $ob_obj->addElement("?mod=".MODULE."&scr=customers_view&tab=specifications&act=specification_edit&idCustomer=".$customer_obj->id."&idSpecification=".$specification_obj->id,"fa-pencil",api_text("customers_view-specifications-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=customer_specification_remove&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_obj->id,"fa-trash",api_text("customers_view-specifications-td-remove"),true,api_text("customers_view-specifications-td-remove-confirm"));
  // add specification row
  $specifications_table->addRow(implode(" ",$tr_class_array));
  $specifications_table->addRowField($specification_obj->getCompatibility(true,false),"nowrap");
  $specifications_table->addRowField(api_tag("samp",$specification_obj->code),"nowrap");
  $specifications_table->addRowField(api_date_format($specification_obj->date,api_text("date")),"nowrap");
  $specifications_table->addRowField($specification_obj->description,"nowrap");
  $specifications_table->addRowField($specification_obj->note,"truncate-ellipsis");
  $specifications_table->addRowField($ob_obj->render(),"text-right");
 }

 // specification view action
 if(ACTION=="specification_view" && $_REQUEST['idSpecification']){
  // get selected specification
  $selected_specification_obj=new cCrmSpecification($_REQUEST['idSpecification']);
  // make attachments
  $attachments_dd_array=array();
  foreach($selected_specification_obj->getAttachments() as $attachment_fobj){
   $row=null;
   if(api_checkAuthorization("crm-customers_manage")){$row=api_link("?mod=".MODULE."&scr=submit&act=customer_specification_attachment_remove&idCustomer=".$customer_obj->id."&idSpecification=".$selected_specification_obj->id."&idAttachment=".$attachment_fobj->id,api_icon("fa-trash",api_text("customers_view-specifications_modal-dd-attachments-delete"),"hidden-link"),null,null,false,api_text("customers_view-specifications_modal-dd-attachments-delete-confirm"))." ";}
   $row.=api_link($attachment_fobj->url."&disposition=inline",$attachment_fobj->name,null,null,false,null,null,null,"_blank");
   $attachments_dd_array[]=$row;
  }
  // build specification description list
  $specification_dl=new strDescriptionList("br","dl-horizontal");
  $specification_dl->addElement(api_text("customers_view-specifications_modal-dt-code"),api_tag("samp",$selected_specification_obj->code));
  $specification_dl->addElement(api_text("customers_view-specifications_modal-dt-date"),api_date_format($selected_specification_obj->date,api_text("date")));
  $specification_dl->addElement(api_text("customers_view-specifications_modal-dt-description"),$selected_specification_obj->description);
  $specification_dl->addElement(api_text("customers_view-specifications_modal-dt-compatibility"),$selected_specification_obj->getCompatibility());
  if($selected_specification_obj->note){$specification_dl->addElement(api_text("customers_view-specifications_modal-dt-note"),nl2br($selected_specification_obj->note));}
  if(count($attachments_dd_array)){
   $specification_dl->addSeparator("hr");
   $specification_dl->addElement(api_text("customers_view-specifications_modal-dt-attachments"),implode("<br>",$attachments_dd_array));
  }
  $specification_dl->addSeparator("hr");
  $specification_dl->addElement(api_text("dl-dt-add"),api_text("dl-dd-add",array((new cUser($selected_specification_obj->addFkUser))->fullname,api_timestamp_format($selected_specification_obj->addTimestamp,api_text("datetime")))));
  if($selected_specification_obj->updTimestamp){$specification_dl->addElement(api_text("dl-dt-upd"),api_text("dl-dd-upd",array((new cUser($selected_specification_obj->updFkUser))->fullname,api_timestamp_format($selected_specification_obj->updTimestamp,api_text("datetime")))));}
  // build specification view modal window
  $specification_modal=new strModal(api_text("customers_view-specifications_modal-title-view",$customer_obj->name),null,"customers_view-specifications_modal");
  $specification_modal->setBody($specification_dl->render());
  // add modal to application
  $app->addModal($specification_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-specifications_modal').modal('show');});");
 }

 // specification add or edit action
 if(in_array(ACTION,array("specification_add","specification_edit"))){
  // get selected specification
  $selected_specification_obj=new cCrmSpecification($_REQUEST['idSpecification']);
  // build specification add form
  $specification_form=new strForm("?mod=".MODULE."&scr=submit&act=customer_specification_save&idCustomer=".$customer_obj->id."&idSpecification=".$selected_specification_obj->id,"POST",null,null,"customers_view-specifications");
  $specification_form->addField("text","code",api_text("customers_view-specifications_modal-ff-code"),$selected_specification_obj->code,api_text("customers_view-specifications_modal-ff-code-placeholder"),null,null,null,"required");
  $specification_form->addField("date","date",api_text("customers_view-specifications_modal-ff-date"),$selected_specification_obj->date,null,null,null,null,"required");
  $specification_form->addField("text","description",api_text("customers_view-specifications_modal-ff-description"),$selected_specification_obj->description,api_text("customers_view-specifications_modal-ff-description-placeholder"),null,null,null,"required");
  $specification_form->addField("radio","compatibility",api_text("customers_view-specifications_modal-ff-compatibility"),$selected_specification_obj->compatibility,null,null,null,null,"required");
  foreach(api_crm_specifications_availableCompatibilities() as $compatibility_f){$specification_form->addFieldOption($compatibility_f->code,$compatibility_f->icon." ".$compatibility_f->text);}
  $specification_form->addField("textarea","note",api_text("customers_view-specifications_modal-ff-note"),$selected_specification_obj->note,api_text("customers_view-specifications_modal-ff-note-placeholder"),null,null,null,"rows='3'");
  $specification_form->addField("file","file",api_text("customers_view-specifications_modal-ff-file"),null,null,null,null,null,"accept='.pdf'");
  $specification_form->addControl("submit",api_text("form-fc-save"));
  $specification_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build specification add modal window
  $specification_modal=new strModal(($selected_specification_obj->id?api_text("customers_view-specifications_modal-title-edit",$customer_obj->name):api_text("customers_view-specifications_modal-title-add",$customer_obj->name)),null,"customers_view-specifications_modal");
  $specification_modal->setBody($specification_form->render(2));
  // add modal to application
  $app->addModal($specification_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_customers_view-specifications_modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>