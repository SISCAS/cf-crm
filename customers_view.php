<?php
/**
 * CRM - Customers View
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorizations
 api_checkAuthorization("crm-customers_view","dashboard");
 // get objects
 $customer_obj=new cCrmCustomer($_REQUEST['idCustomer'],$_REQUEST['customer']);
 // check objects
 if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("customers_view",$customer_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // make map link
 if(!$customer_obj->latitude || !$customer_obj->longitude){$map_link=null;}
 else{$map_link=api_link("?mod=".MODULE."&scr=customers_view&act=map_view&idCustomer=".$customer_obj->id,api_icon("fa-map-marker",api_text("customers_view-dt-map"),"hidden-link"))." ";}
 // build customer description list
 $customer_dl=new strDescriptionList("br","dl-horizontal");
 $customer_dl->addElement(api_text("customers_view-dt-name"),api_tag("samp",ltrim($customer_obj->code,"0"))." ".api_tag("strong",$customer_obj->name));
 $customer_dl->addElement(api_text("customers_view-dt-fiscalName"),$customer_obj->fiscalName);
 if($customer_obj->address){$customer_dl->addElement(api_text("customers_view-dt-address"),$map_link.$customer_obj->address);}
 // include tabs
 require_once(MODULE_PATH."customers_view-informations.inc.php");
 require_once(MODULE_PATH."customers_view-contacts.inc.php");
 require_once(MODULE_PATH."customers_view-relationships.inc.php");
 require_once(MODULE_PATH."customers_view-attachments.inc.php");
 require_once(MODULE_PATH."customers_view-specifications.inc.php");
 require_once(MODULE_PATH."customers_view-products.inc.php");
 require_once(MODULE_PATH."customers_view-offers.inc.php");
 // include map modal window
 if($map_link){require_once(MODULE_PATH."customers_view-map.inc.php");}
 $tab=new strTab();
 $tab->addItem(api_icon("fa-bookmark")." ".api_text("customers_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-group")." ".api_text("customers_view-tab-contacts"),$contacts_table->render(),("contacts"==TAB?"active":null));
 $tab->addItem(api_icon("fa-comments-o")." ".api_text("customers_view-tab-relationships"),$relationships_table->render(),("relationships"==TAB?"active":null));
 $tab->addItem(api_icon("fa-paperclip")." ".api_text("customers_view-tab-attachments"),$attachments_table->render(),("attachments"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-code-o")." ".api_text("customers_view-tab-specifications"),$specifications_table->render(),("specifications"==TAB?"active":null));
 $tab->addItem(api_icon("fa-cubes")." ".api_text("customers_view-tab-products"),$products_table->render(),("products"==TAB?"active":null));
 $tab->addItem(api_icon("fa-clipboard")." ".api_text("customers_view-tab-offers"),$offers_table->render(),("offers"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("customers_view-tab-events"),api_events_table($customer_obj->getEvents(10))->render(),("events"==TAB?"active":null));
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($customer_dl->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 if($selected_contact_obj->id){api_dump($selected_contact_obj,"selected contact");}
 if($selected_relationship_obj->id){api_dump($selected_relationship_obj,"selected relationship");}
 if($selected_attachment_obj->id){api_dump($selected_attachment_obj,"selected attachment");}
 if($selected_specification_obj->id){api_dump($selected_specification_obj,"selected specification");}
 if($selected_product_obj->id){api_dump($selected_product_obj,"selected product");}
 if($selected_offer_obj->id){api_dump($selected_offer_obj,"selected offer");}
 api_dump($customer_obj,"customer");

?>