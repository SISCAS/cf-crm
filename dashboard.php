<?php
/**
 * CRM - Dashboard
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("crm"));
 // build dashboard object
 $dashboard=new strDashboard();
 $dashboard->addTile("?mod=".MODULE."&scr=customers_list",api_text("dashboard-customers-list"),api_text("dashboard-customers-list-description"),(api_checkAuthorization("crm-customers_view")),"1x1","fa-book");
 $dashboard->addTile("?mod=".MODULE."&scr=specifications_list",api_text("dashboard-specifications-list"),api_text("dashboard-specifications-list-description"),(api_checkAuthorization("crm-customers_view")),"1x1","fa-folder-open");
 $dashboard->addTile("?mod=".MODULE."&scr=offers_list",api_text("dashboard-offers-list"),api_text("dashboard-offers-list-description"),(api_checkAuthorization("crm-customers_view")),"1x1","fa-archive");
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dashboard->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();

?>