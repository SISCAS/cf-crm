<?php
/**
 * CRM - Divisions Edit
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorizations
 api_checkAuthorization("crm-manage","dashboard");
 // get objects
 $division_obj=new cCrmDivision($_REQUEST['idDivision']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($division_obj->id?api_text("divisions_edit",$division_obj->name):api_text("divisions_edit-add")));
 // build division form
 $form=new strForm("?mod=".MODULE."&scr=submit&act=division_save&idDivision=".$division_obj->id."&return_scr=".$_REQUEST['return_scr'],"POST",null,null,"divisions_edit");
 $form->addField("text","name",api_text("divisions_edit-ff-name"),$division_obj->name,api_text("divisions_edit-ff-name-placeholder"),null,null,null,"required");
 $form->addField("textarea","description",api_text("divisions_edit-ff-description"),$division_obj->description,api_text("divisions_edit-ff-description-placeholder"),null,null,null,"rows='2'");
 $form->addField("text","company",api_text("divisions_edit-ff-company"),$division_obj->company,api_text("divisions_edit-ff-company-placeholder"),null,null,null,"required");
 $form->addField("text","fiscal_name",api_text("divisions_edit-ff-fiscal_name"),$division_obj->fiscal_name,api_text("divisions_edit-ff-fiscal_name-placeholder"),null,null,null,"required");
 $form->addField("text","fiscal_address",api_text("divisions_edit-ff-fiscal_address"),$division_obj->fiscal_address,api_text("divisions_edit-ff-fiscal_address-placeholder"),null,null,null,"required");
 $form->addField("text","fiscal_code",api_text("divisions_edit-ff-fiscal_code"),$division_obj->fiscal_code,api_text("divisions_edit-ff-fiscal_code-placeholder"),null,null,null,"required");
 $form->addField("text","fiscal_vat",api_text("divisions_edit-ff-fiscal_vat"),$division_obj->fiscal_vat,api_text("divisions_edit-ff-fiscal_vat-placeholder"),null,null,null,"required");
 // controls
 $form->addControl("submit",api_text("form-fc-save"));
 if($division_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=".api_return_script("divisions_view")."&idDivision=".$division_obj->id);
  if(!$division_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),"?mod=".MODULE."&scr=submit&act=division_delete&idDivision=".$division_obj->id,"btn-danger",api_text("divisions_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),"?mod=".MODULE."&scr=submit&act=division_undelete&idDivision=".$division_obj->id,"btn-warning");
   //$form->addControl("button",api_text("form-fc-remove"),"?mod=".MODULE."&scr=submit&act=division_remove&idDivision=".$division_obj->id,"btn-danger",api_text("divisions_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),"?mod=".MODULE."&scr=divisions_list");}
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($division_obj,"division");

?>