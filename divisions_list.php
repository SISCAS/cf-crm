<?php
/**
 * CRM - Divisions List
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorizations
 api_checkAuthorization("crm-manage","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // definitions
 $users_array=array();
 // set application title
 $app->setTitle(api_text("divisions_list"));
 // definitions
 $divisions_array=array();
 // build filter
 $filter=new strFilter();
 $filter->addSearch(array("name","description"));
 // build query object
 $query=new cQuery("crm__divisions",$filter->getQueryWhere());
 $query->addQueryOrderField("name");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$divisions_array[$result_f->id]=new cCrmDivision($result_f);}
 // build table
 $table=new strTable(api_text("divisions_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("divisions_list-th-name"),"nowrap");
 $table->addHeader(api_text("divisions_list-th-description"),null,"100%");
 $table->addHeader("&nbsp;",null,16);
 // cycle all divisions
 foreach($divisions_array as $division_obj){
  // build operation button
  $ob=new strOperationsButton();
  $ob->addElement("?mod=".MODULE."&scr=divisions_edit&idDivision=".$division_obj->id."&return_scr=divisions_list","fa-pencil",api_text("divisions_list-td-edit"),(api_checkAuthorization("crm-manage")));
  if($division_obj->deleted){$ob->addElement("?mod=".MODULE."&scr=submit&act=division_undelete&idDivision=".$division_obj->id,"fa-trash-o",api_text("divisions_list-td-undelete"),true,api_text("divisions_list-td-undelete-confirm"));}
  else{$ob->addElement("?mod=".MODULE."&scr=submit&act=division_delete&idDivision=".$division_obj->id,"fa-trash",api_text("divisions_list-td-delete"),true,api_text("divisions_list-td-delete-confirm"));}
  // make table row class
  $tr_class_array=array();
  if($division_obj->id==$_REQUEST['idDivision']){$tr_class_array[]="currentrow";}
  if($division_obj->deleted){$tr_class_array[]="deleted";}
  // make division row
  $table->addRow(implode(" ",$tr_class_array));
  //$table->addRowFieldAction("?mod=".MODULE."&scr=divisions_view&idDivision=".$division_obj->id,api_icon("fa-search",api_text("divisions_list-td-view"),"hidden-link"));
  $table->addRowFieldAction("?mod=".MODULE."&scr=divisions_view&idDivision=".$division_obj->id,"fa-search",api_text("divisions_list-td-view"));
  $table->addRowField($division_obj->name,"nowrap");
  $table->addRowField($division_obj->description,"truncate-ellipsis");
  $table->addRowField($ob->render(),"text-right");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");

?>