<?php
/**
 * CRM - Divisions View (Informations)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 $informations_dl=new strDescriptionList("br","dl-horizontal");
 $informations_dl->addElement(api_text("divisions_view-informations-dt-fiscal_name"),$division_obj->fiscal_name);
 $informations_dl->addElement(api_text("divisions_view-informations-dt-fiscal_address"),$division_obj->fiscal_address);
 $informations_dl->addElement(api_text("divisions_view-informations-dt-fiscal_code"),$division_obj->fiscal_code);
 $informations_dl->addElement(api_text("divisions_view-informations-dt-fiscal_vat"),$division_obj->fiscal_vat);

?>