<?php
/**
 * CRM - Divisions View (Salesareas)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build salesareas table
 $salesareas_table=new strTable(api_text("divisions_view-salesareas-tr-unvalued"));
 $salesareas_table->addHeader(api_text("divisions_view-salesareas-th-name"),"nowrap");
 $salesareas_table->addHeader(api_text("divisions_view-salesareas-th-code"),null,"100%");
 $salesareas_table->addHeader("&nbsp;",null,16);
 // cycle division salesareas
 foreach($division_obj->getSalesareas() as $salesarea_obj){
  // make table row class
  $tr_class_array=array();
  if($salesarea_obj->id==$_REQUEST['idSalesarea']){$tr_class_array[]="currentrow";}
  if($salesarea_obj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=divisions_view&tab=salesareas&act=salesarea_view&idDivision=".$division_obj->id."&idSalesarea=".$salesarea_obj->id,"fa-info-circle",api_text("divisions_view-salesareas-td-view"));
  $ob_obj->addElement("?mod=".MODULE."&scr=divisions_view&tab=salesareas&act=salesarea_edit&idDivision=".$division_obj->id."&idSalesarea=".$salesarea_obj->id,"fa-pencil",api_text("divisions_view-salesareas-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=division_salesarea_remove&tab=salesareas&idDivision=".$division_obj->id."&idSalesarea=".$salesarea_obj->id,"fa-trash",api_text("divisions_view-salesareas-td-remove"),true,api_text("divisions_view-salesareas-td-remove-confirm"));
  // add salesarea row
  $salesareas_table->addRow(implode(" ",$tr_class_array));
  $salesareas_table->addRowField($salesarea_obj->name,"nowrap");
  $salesareas_table->addRowField(api_tag("samp",$salesarea_obj->code),"truncate-ellipsis");
  $salesareas_table->addRowField($ob_obj->render(),"text-right");
 }

 // check salesarea actions
 if(ACTION=="salesarea_view" && $_REQUEST['idSalesarea']){
  // get selected salesarea
  $selected_salesarea_obj=new cCrmSalesarea($_REQUEST['idSalesarea']);
  // build salesarea description list
  $salesarea_dl=new strDescriptionList("br","dl-horizontal");
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-name"),$selected_salesarea_obj->name);
  if($selected_salesarea_obj->description){$salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-description"),nl2br($selected_salesarea_obj->description));}
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-orderTypology"),api_tag("samp",$selected_salesarea_obj->orderTypology));
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-commercialOrganization"),api_tag("samp",$selected_salesarea_obj->commercialOrganization));
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-distributionChannel"),api_tag("samp",$selected_salesarea_obj->distributionChannel));
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-productSector"),api_tag("samp",$selected_salesarea_obj->productSector));
  $salesarea_dl->addElement(api_text("divisions_view-salesareas-modal-dt-salesOffice"),api_tag("samp",$selected_salesarea_obj->salesOffice));
  $salesarea_dl->addSeparator("hr");
  $salesarea_dl->addElement(api_text("dl-dt-add"),api_text("dl-dd-add",array((new cUser($selected_salesarea_obj->addFkUser))->fullname,api_timestamp_format($selected_salesarea_obj->addTimestamp,api_text("datetime")))));
  if($selected_salesarea_obj->updTimestamp){$salesarea_dl->addElement(api_text("dl-dt-upd"),api_text("dl-dd-upd",array((new cUser($selected_salesarea_obj->updFkUser))->fullname,api_timestamp_format($selected_salesarea_obj->updTimestamp,api_text("datetime")))));}
  // build salesarea view modal window
  $salesarea_modal=new strModal(api_text("divisions_view-salesareas-modal-title-view",$division_obj->name),null,"divisions_view-salesareas-modal");
  $salesarea_modal->setBody($salesarea_dl->render());
  // add modal to application
  $app->addModal($salesarea_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_divisions_view-salesareas-modal').modal('show');});");
 }

 // check salesarea actions
 if(in_array(ACTION,array("salesarea_add","salesarea_edit"))){
  // get selected salesarea
  $selected_salesarea_obj=new cCrmSalesarea($_REQUEST['idSalesarea']);
  // build salesarea add form
  $salesarea_form=new strForm("?mod=".MODULE."&scr=submit&act=division_salesarea_save&idDivision=".$division_obj->id."&idSalesarea=".$selected_salesarea_obj->id,"POST",null,null,"divisions_view-salesareas");
  $salesarea_form->addField("text","name",api_text("divisions_view-salesareas-modal-ff-name"),$selected_salesarea_obj->name,api_text("divisions_view-salesareas-modal-ff-name-placeholder"),null,null,null,"required");
  $salesarea_form->addField("text","description",api_text("divisions_view-salesareas-modal-ff-description"),$selected_salesarea_obj->description,api_text("divisions_view-salesareas-modal-ff-description-placeholder"));
  $salesarea_form->addField("text","orderTypology",api_text("divisions_view-salesareas-modal-ff-orderTypology"),$selected_salesarea_obj->orderTypology,api_text("divisions_view-salesareas-modal-ff-orderTypology-placeholder"),null,null,null,"required");
  $salesarea_form->addField("text","commercialOrganization",api_text("divisions_view-salesareas-modal-ff-commercialOrganization"),$selected_salesarea_obj->commercialOrganization,api_text("divisions_view-salesareas-modal-ff-commercialOrganization-placeholder"),null,null,null,"required");
  $salesarea_form->addField("text","distributionChannel",api_text("divisions_view-salesareas-modal-ff-distributionChannel"),$selected_salesarea_obj->distributionChannel,api_text("divisions_view-salesareas-modal-ff-distributionChannel-placeholder"),null,null,null,"required");
  $salesarea_form->addField("text","productSector",api_text("divisions_view-salesareas-modal-ff-productSector"),$selected_salesarea_obj->productSector,api_text("divisions_view-salesareas-modal-ff-productSector-placeholder"),null,null,null,"required");
  $salesarea_form->addField("text","salesOffice",api_text("divisions_view-salesareas-modal-ff-salesOffice"),$selected_salesarea_obj->salesOffice,api_text("divisions_view-salesareas-modal-ff-salesOffice-placeholder"),null,null,null,"required");
  $salesarea_form->addControl("submit",api_text("form-fc-save"));
  $salesarea_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build salesarea add modal window
  $salesarea_modal=new strModal(($selected_salesarea_obj->id?api_text("divisions_view-salesareas-modal-title-edit",array($selected_salesarea_obj->name,$division_obj->name)):api_text("divisions_view-salesareas-modal-title-add",$division_obj->name)),null,"divisions_view-salesareas-modal");
  $salesarea_modal->setBody($salesarea_form->render(2));
  // add modal to application
  $app->addModal($salesarea_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_divisions_view-salesareas-modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>