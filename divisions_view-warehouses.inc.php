<?php
/**
 * CRM - Divisions View (Warehouses)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // build warehouses table
 $warehouses_table=new strTable(api_text("divisions_view-warehouses-tr-unvalued"));
 $warehouses_table->addHeader(api_text("divisions_view-warehouses-th-name"),"nowrap");
 $warehouses_table->addHeader(api_text("divisions_view-warehouses-th-codes"),null,"100%");
 $warehouses_table->addHeader("&nbsp;",null,16);
 // cycle division warehouses
 foreach($division_obj->getWarehouses() as $warehouse_obj){
  // make table row class
  $tr_class_array=array();
  if($warehouse_obj->id==$_REQUEST['idWarehouse']){$tr_class_array[]="currentrow";}
  if($warehouse_obj->deleted){$tr_class_array[]="deleted";}
  // build operations button
  $ob_obj=new strOperationsButton();
  $ob_obj->addElement("?mod=".MODULE."&scr=divisions_view&tab=warehouses&act=warehouse_view&idDivision=".$division_obj->id."&idWarehouse=".$warehouse_obj->id,"fa-info-circle",api_text("divisions_view-warehouses-td-view"));
  $ob_obj->addElement("?mod=".MODULE."&scr=divisions_view&tab=warehouses&act=warehouse_edit&idDivision=".$division_obj->id."&idWarehouse=".$warehouse_obj->id,"fa-pencil",api_text("divisions_view-warehouses-td-edit"));
  $ob_obj->addElement("?mod=".MODULE."&scr=submit&act=division_warehouse_remove&tab=warehouses&idDivision=".$division_obj->id."&idWarehouse=".$warehouse_obj->id,"fa-trash",api_text("divisions_view-warehouses-td-remove"),true,api_text("divisions_view-warehouses-td-remove-confirm"));
  // add warehouse row
  $warehouses_table->addRow(implode(" ",$tr_class_array));
  $warehouses_table->addRowField($warehouse_obj->name,"nowrap");
  $warehouses_table->addRowField(api_tag("samp",implode(", ",$warehouse_obj->codes_array)),"truncate-ellipsis");
  $warehouses_table->addRowField($ob_obj->render(),"text-right");
 }

 // check warehouse actions
 if(ACTION=="warehouse_view" && $_REQUEST['idWarehouse']){
  // get selected warehouse
  $selected_warehouse_obj=new cCrmWarehouse($_REQUEST['idWarehouse']);
  // build warehouse description list
  $warehouse_dl=new strDescriptionList("br","dl-horizontal");
  $warehouse_dl->addElement(api_text("divisions_view-warehouses-modal-dt-name"),$selected_warehouse_obj->name);
  if($selected_warehouse_obj->description){$warehouse_dl->addElement(api_text("divisions_view-warehouses-modal-dt-description"),nl2br($selected_warehouse_obj->description));}
  $warehouse_dl->addElement(api_text("divisions_view-warehouses-modal-dt-codes"),api_tag("samp",implode(", ",$selected_warehouse_obj->codes_array)));
  $warehouse_dl->addSeparator("hr");
  $warehouse_dl->addElement(api_text("dl-dt-add"),api_text("dl-dd-add",array((new cUser($selected_warehouse_obj->addFkUser))->fullname,api_timestamp_format($selected_warehouse_obj->addTimestamp,api_text("datetime")))));
  if($selected_warehouse_obj->updTimestamp){$warehouse_dl->addElement(api_text("dl-dt-upd"),api_text("dl-dd-upd",array((new cUser($selected_warehouse_obj->updFkUser))->fullname,api_timestamp_format($selected_warehouse_obj->updTimestamp,api_text("datetime")))));}


  // build warehouse view modal window
  $warehouse_modal=new strModal(api_text("divisions_view-warehouses-modal-title-view",$division_obj->name),null,"divisions_view-warehouses-modal");
  $warehouse_modal->setBody($warehouse_dl->render());
  // add modal to application
  $app->addModal($warehouse_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_divisions_view-warehouses-modal').modal('show');});");
 }

 // check warehouse actions
 if(in_array(ACTION,array("warehouse_add","warehouse_edit"))){
  // get selected warehouse
  $selected_warehouse_obj=new cCrmWarehouse($_REQUEST['idWarehouse']);
  // build warehouse add form
  $warehouse_form=new strForm("?mod=".MODULE."&scr=submit&act=division_warehouse_save&idDivision=".$division_obj->id."&idWarehouse=".$selected_warehouse_obj->id,"POST",null,null,"divisions_view-warehouses");
  $warehouse_form->addField("text","name",api_text("divisions_view-warehouses-modal-ff-name"),$selected_warehouse_obj->name,api_text("divisions_view-warehouses-modal-ff-name-placeholder"),null,null,null,"required");
  $warehouse_form->addField("text","description",api_text("divisions_view-warehouses-modal-ff-description"),$selected_warehouse_obj->description,api_text("divisions_view-warehouses-modal-ff-description-placeholder"));
  $warehouse_form->addField("textarea","codes",api_text("divisions_view-warehouses-modal-ff-codes"),implode("\n",$selected_warehouse_obj->codes_array),api_text("divisions_view-warehouses-modal-ff-codes-placeholder"),null,null,null,"rows='5' required");
  $warehouse_form->addControl("submit",api_text("form-fc-save"));
  $warehouse_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build warehouse add modal window
  $warehouse_modal=new strModal(($selected_warehouse_obj->id?api_text("divisions_view-warehouses-modal-title-edit",array($selected_warehouse_obj->name,$division_obj->name)):api_text("divisions_view-warehouses-modal-title-add",$division_obj->name)),null,"divisions_view-warehouses-modal");
  $warehouse_modal->setBody($warehouse_form->render(2));
  // add modal to application
  $app->addModal($warehouse_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$('#modal_divisions_view-warehouses-modal').modal({show:true,backdrop:'static',keyboard:false});});");
 }

?>