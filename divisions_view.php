<?php
/**
 * CRM - Divisions View
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorizations
 api_checkAuthorization("crm-manage","dashboard");
 // get objects
 $division_obj=new cCrmDivision($_REQUEST['idDivision'],$_REQUEST['division']);
 // check objects
 if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
 // deleted alert
 if($division_obj->deleted){api_alerts_add(api_text("divisions_view-alert-deleted"),"warning");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("divisions_view",$division_obj->name));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build division description list
 $division_dl=new strDescriptionList("br","dl-horizontal");
 $division_dl->addElement(api_text("divisions_view-dt-company"),api_tag("samp",$division_obj->company));
 $division_dl->addElement(api_text("divisions_view-dt-name"),api_tag("strong",$division_obj->name));
 if($division_obj->description){$division_dl->addElement(api_text("divisions_view-dt-description"),nl2br($division_obj->description));}
 // build group table
 $table_groups=new strTable(api_text("divisions_view-tr-unvalued"));
 // cycle all authorizations
 foreach($division_obj->getAuthorizations() as $authorization_fobj){
  // get group object
  $authorization_fobj->group_obj=new cGroup($authorization_fobj->fkGroup);
  // add group row
  $table_groups->addRow();
  $table_groups->addRowField(api_link("?mod=framework&scr=groups_view&idGroup=".$authorization_fobj->group_obj->id,$authorization_fobj->group_obj->name." (+".$authorization_fobj->level."&deg;)",$authorization_fobj->group_obj->getPath("string"),"hidden-link",true,null,null,null,"_blank"),"truncate-ellipsis");
  $table_groups->addRowFieldAction("?mod=".MODULE."&scr=submit&act=division_group_remove&idDivision=".$division_obj->id."&idGroup=".$authorization_fobj->group_obj->id,"fa-trash",api_text("divisions_view-td-delete"),api_text("divisions_view-td-delete-confirm"));
 }
 // build groups description list
 $groups_dl=new strDescriptionList("br","dl-horizontal");
 $groups_dl->addElement(api_text("divisions_view-dt-groups"),$table_groups->render());
 // check for group add action
 if(ACTION=="group_add"){
  // build group add form
  $groups_form=new strForm("?mod=".MODULE."&scr=submit&act=division_group_add&idDivision=".$division_obj->id,"POST",null,null,"divisions_view-groups_modal");
  $groups_form->addField("select","fkGroup",api_text("divisions_view-groups_modal-ff-group"),null,api_text("divisions_view-groups_modal-ff-group-placeholder"),null,null,null,"required");
  api_tree_to_array($groups_array,"api_availableGroups","id");
  foreach($groups_array as $group_opt){$groups_form->addFieldOption($group_opt->id,str_repeat("&nbsp;&nbsp;&nbsp;",$group_opt->nesting).$group_opt->fullname);}
  $groups_form->addField("select","level",api_text("divisions_view-groups_modal-ff-level"),$user->level,api_text("divisions_view-groups_modal-ff-level-placeholder"),null,null,null,"required");
  for($level=1;$level<=$GLOBALS['settings']->users_level_max;$level++){$groups_form->addFieldOption($level,api_text("divisions_view-groups_modal-ff-level-fo-level",$level));}
  $groups_form->addControl("submit",api_text("form-fc-save"));
  $groups_form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build group add modal window
  $groups_modal=new strModal(api_text("divisions_view-groups_modal-title",$division_obj->name),null,"divisions_view-groups_modal");
  $groups_modal->setBody($groups_form->render());
  // add modal to application
  $app->addModal($groups_modal);
  // jQuery scripts
  $app->addScript("/* Modal window opener */\n$(function(){\$(\"#modal_divisions_view-groups_modal\").modal({show:true,backdrop:'static',keyboard:false});});");
 }
 // include tabs
 require_once(MODULE_PATH."divisions_view-informations.inc.php");
 require_once(MODULE_PATH."divisions_view-salesareas.inc.php");
 require_once(MODULE_PATH."divisions_view-warehouses.inc.php");
 $tab=new strTab();
 $tab->addItem(api_icon("fa-flag-o")." ".api_text("divisions_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-briefcase")." ".api_text("divisions_view-tab-salesareas"),$salesareas_table->render(),("salesareas"==TAB?"active":null));
 $tab->addItem(api_icon("fa-cubes")." ".api_text("divisions_view-tab-warehouses"),$warehouses_table->render(),("warehouses"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("divisions_view-tab-events"),api_events_table($division_obj->getEvents())->render(),("events"==TAB?"active":null));
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($division_dl->render(),"col-xs-12 col-sm-5");
 $grid->addCol($groups_dl->render(),"col-xs-12 col-sm-7");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 if($selected_salesarea_obj->id){api_dump($selected_salesarea_obj,"salesarea");}
 if($selected_warehouse_obj->id){api_dump($selected_warehouse_obj,"warehouse");}
 api_dump($division_obj,"division");

?>