<?php
/**
 * CRM Functions (RFC)
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

/**
 * CRM - RFC Codings
 *
 * @param string $element Coding element
 * @return object[] Array of trascodings elements
 */
function api_crm_rfc_codings($element){
  // definitions
  $elements_array=array();
  // make input parameters
  $v_input=array("I_ELEMENT"=>strtoupper($element));
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_CODINGS",$v_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_ELEMENTS'] as $element_f){
    // build order
    $element_obj=new stdClass();
    $element_obj->id=trim($element_f['ELKEY']);
    $element_obj->name=trim($element_f['ELTXT']);
    $element_obj->label=strtoupper($element_obj->name." [".$element_obj->id."]");
    // add order to orders array
    $elements_array[$element_obj->id]=$element_obj;
  }
  //api_dump($elements_array,"elements array");
  // return
  return $elements_array;
}

/**
 * CRM - RFC Grades (T58)
 *
 * @return object[] Array of grades
 */
function api_crm_rfc_grades(){
  // definitions
  $grades_array=array();
  // make input parameters
  $v_input=array("VAR_TABLE"=>"T58");
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZVARTABLE",$v_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['TVT58'] as $grade_f){
    // build order
    $grade_obj=new stdClass();
    $grade_obj->code=trim($grade_f['C000400']);
    $grade_obj->text=trim($grade_f['C000400']);
    // add order to orders array
    $grades_array[$grade_obj->code]=$grade_obj;
  }
  sort($grades_array);
  //api_dump($grades_array,"grades array");
  // return
  return $grades_array;
}

/**
 * CRM - RFC Customer Search
 *
 * @param string $search Search string
 * @return object[] Array of customers
 */
function api_crm_rfc_customer_search($search){
  // definitions
  $results_array=array();
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_CUSTOMER_SEARCH",array("I_SEARCH"=>"*".str_replace(" ","*",$search)."*"));
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_CUSTOMERS'] as $result_f){
    // build result row
    $customer_obj=new stdClass();
    // add typology to row
    $customer_obj->code=trim($result_f['KUNNR']);
    $customer_obj->name=trim($result_f['NAME1']);
    if(strlen(trim($result_f['NAME2']))){$customer_obj->name.=" (".trim($result_f['NAME2']).")";}
    $customer_obj->group=trim($result_f['KTOKD']);
    $customer_obj->deleted=($result_f['LOEVM']=="X"?true:false);
    // add row to results array
    $results_array[$customer_obj->code]=$customer_obj;
  }
  //api_dump($results_array,"results array");
  // return
  return $results_array;
}

/**
 * CRM - RFC Customer Informations
 *
 * @param string $customer Customer code
 * @return object Customer informations
 */
function api_crm_rfc_customer_informations($customer,$partners=false){
  // build customer code
  $v_code=str_pad($customer,10,"0",STR_PAD_LEFT);
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_CUSTOMER_INFO",array("I_KUNNR"=>$v_code));
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // check for customer
  if($wsrfc['result']['E_CUSTOMER']['KUNNR']!=$v_code){return false;}
  // build customer object
  $customer_obj=new stdClass();
  $customer_obj->code=$wsrfc['result']['E_CUSTOMER']['KUNNR'];
  $customer_obj->name=trim($wsrfc['result']['E_CUSTOMER']['NAME1']);
  $customer_obj->address=trim($wsrfc['result']['E_CUSTOMER']['STRAS'])." - ".
    trim($wsrfc['result']['E_CUSTOMER']['PSTLZ'])." ".
    trim($wsrfc['result']['E_CUSTOMER']['ORT01'])." (".
    trim($wsrfc['result']['E_CUSTOMER']['REGIO']).")";
  // cycle all companies
  $customer_obj->commercialReferents=array();
  $customer_obj->companies_array=array();
  foreach($wsrfc['result']['T_COMPANIES'] as $company_f){
    // build partner object
    $company_obj=new stdClass();
    $company_obj->code=trim($company_f['BUKRS']);
    $company_obj->paymentMethod=trim($company_f['ZWELS']);
    $company_obj->paymentCondition=trim($company_f['ZTERM']);
    $company_obj->commercialManager=trim($company_f['BUSAB']);
    $company_obj->deleted=($company_f['LOEVM']=="X"?true:false);
    //api_dump($company_obj,"company object");
    $customer_obj->companies_array[$company_obj->code]=$company_obj;
  }
  // cycle all salesareas
  $customer_obj->salesareas_array=array();
  foreach($wsrfc['result']['T_SALESAREAS'] as $salesarea_f){
    // build partner object
    $salesarea_obj=new stdClass();
    $salesarea_obj->code=trim($salesarea_f['VKORG'].'|'.$salesarea_f['VTWEG'].'|'.$salesarea_f['SPART']);
    $salesarea_obj->commercialOrganization=trim($salesarea_f['VKORG']);
    $salesarea_obj->distributionChannel=trim($salesarea_f['VTWEG']);
    $salesarea_obj->commercialReferent=trim($salesarea_f['VKGRP']);
    $salesarea_obj->priceGroup=trim($salesarea_f['KONDA']);
    $salesarea_obj->priceList=trim($salesarea_f['PLTYP']);
    $salesarea_obj->paymentCondition=trim($salesarea_f['ZTERM']);
    $salesarea_obj->transportCondition=trim($salesarea_f['INCO1']);
    $salesarea_obj->transportNote=trim($salesarea_f['INCO2']);
    //api_dump($salesarea_obj,"salesarea object");
    $customer_obj->salesareas_array[$salesarea_obj->code]=$salesarea_obj;
    // set commercial referents
    if($salesarea_obj->commercialReferent){
      $customer_obj->commercialReferents[$salesarea_obj->commercialReferent]=$salesarea_obj->commercialReferent;
    }
  }
  // texts   @todo se ne metterò più di una usare un campo chiave tipo id nota e looppare su quello
  $texts_array=array();
  foreach($wsrfc['result']['T_TEXTS'] as $text_f){$texts_array[$text_f['NRLINE']]=$text_f['TDLINE'];}
  ksort($texts_array);
  $customer_obj->trasports_note=implode("\n",$texts_array);
  // check for suppliers partners
  if($partners){
    $customer_obj->suppliers_array=array();
    foreach($wsrfc['result']['T_SUPPLIERS'] as $supplier_f){
      // build partner object
      $supplier_obj=new stdClass();
      $supplier_obj->code=trim($supplier_f['LIFNR']);
      $supplier_obj->name=trim($supplier_f['NAME1']);
      //api_dump($receiver_obj,"receiver object");
      $customer_obj->suppliers_array[$supplier_obj->code]=$supplier_obj;
    }
  }
  // check for receivers partners
  if($partners){
    $customer_obj->receivers_array=array();
    foreach($wsrfc['result']['T_RECEIVERS'] as $receiver_f){
      // build partner object
      $receiver_obj=new stdClass();
      $receiver_obj->code=trim($receiver_f['KUNNR']);
      $receiver_obj->name=trim($receiver_f['NAME1']);
      $receiver_obj->address=trim($receiver_f['STRAS'])." - ".
        trim($receiver_f['PSTLZ'])." ".
        trim($receiver_f['ORT01'])." (".
        trim($receiver_f['REGIO']).")";
      //api_dump($receiver_obj,"receiver object");
      $customer_obj->receivers_array[$receiver_obj->code]=$receiver_obj;
    }
  }
  // check for receivers partners
  if($partners){
    $customer_obj->billings_array=array();
    foreach($wsrfc['result']['T_BILLINGS'] as $billing_f){
      // build partner object
      $billing_obj=new stdClass();
      $billing_obj->code=trim($billing_f['KUNNR']);
      $billing_obj->name=trim($billing_f['NAME1']);
      $billing_obj->address=trim($billing_f['STRAS'])." - ".
        trim($billing_f['PSTLZ'])." ".
        trim($billing_f['ORT01'])." (".
        trim($billing_f['REGIO']).")";
      //api_dump($billing_obj,"billing object");
      $customer_obj->billings_array[$billing_obj->code]=$billing_obj;
    }
  }
  // cycle all credits
  $customer_obj->credits_array=array();
  foreach($wsrfc['result']['T_CREDITS'] as $credit_f){
    // build credit object
    $credit_obj=new stdClass();
    $credit_obj->controllingArea=trim($credit_f['KKBER']);
    $credit_obj->currency=trim($credit_f['WAERS']);
    $credit_obj->horizon=api_wsrfc_dateToTimestamp(trim($credit_f['HORIZ']));
    $credit_obj->credits=(double)trim($credit_f['SKFOR']);
    $credit_obj->engagements=(double)trim($credit_f['SSOBL']);
    $credit_obj->orders=(double)trim($credit_f['SAUFT']);
    $credit_obj->totals=(double)($credit_obj->credits+$credit_obj->engagements+$credit_obj->orders);
    $credit_obj->limit=(double)trim($credit_f['KLIMK']);
    $credit_obj->percentage=(double)($credit_obj->totals*100/$credit_obj->limit);
    //api_dump($credit_f,"credit object");
    $customer_obj->credits_array[$credit_obj->controllingArea]=$credit_obj;
  }
  // cycle all unsolved invoices
  $customer_obj->unsolveds_array=array();
  foreach($wsrfc['result']['T_UNSOLVEDS'] as $unsolved_f){
    // build unsolved object
    $unsolved_obj=new stdClass();
    $unsolved_obj->company=trim($unsolved_f['BUKRS']);
    $unsolved_obj->fiscalYear=trim($unsolved_f['GJAHR']);
    $unsolved_obj->fiscalDocument=trim($unsolved_f['BELNR']);
    $unsolved_obj->fiscalElement=trim($unsolved_f['BUZEI']);
    $unsolved_obj->currency=trim($unsolved_f['WAERS']);
    $unsolved_obj->importDocument=(double)$unsolved_f['WRBTR'];
    $unsolved_obj->importInternal=(double)$unsolved_f['DMBTR'];
    $unsolved_obj->expiration=api_wsrfc_dateToTimestamp(trim($unsolved_f['NETDT']));
    $unsolved_obj->paymentModality=trim($unsolved_f['ZLSCH']);
    //api_dump($unsolved_f,"unsolved object");
    $customer_obj->unsolveds_array[]=$unsolved_obj;
  }
  // sort unsorted invoices
  $customer_obj->unsolveds_array=api_sortObjectsArray($customer_obj->unsolveds_array,"expiration",true);
  //api_dump($customer_obj,"customer object");
  // return
  return $customer_obj;
}

/**
 * CRM - RFC Customer Orders
 *
 * @param string $customer Customer code
 * @param string[] $salesareas Array of Salesareas codes
 * @return object[] Array of customer orders
 */
function api_crm_rfc_customer_orders($customer,$salesareas=null){
  // check parameters
  if(!is_array($salesareas)){$salesareas=array_filter(array($salesareas),'strlen');}
  // definitions
  $orders_array=array();
  // make customer code
  $v_code=str_pad($customer,10,"0",STR_PAD_LEFT);
  // make input parameters
  $v_input=array(
    "I_KUNNR"=>$v_code,
    "R_ERDAT"=>array(array("SIGN"=>"I","OPTION"=>"GE","LOW"=>api_timestampDifferenceFrom(time(),"-30 days","Ymd")))
  );
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_CUSTOMER_ORDERS",$v_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_ORDERS'] as $order_f){
    // build order
    $order_obj=new stdClass();
    $order_obj->number=trim($order_f['VBELN']);
    $order_obj->timestamp=api_wsrfc_dateToTimestamp($order_f['ERDAT']);
    $order_obj->typology=trim($order_f['AUART']);
    $order_obj->commercialOrganization=trim($order_f['VKORG']);
    $order_obj->distributionChannel=trim($order_f['VTWEG']);
    $order_obj->productSector=trim($order_f['SPART']);
    $order_obj->salesOffice=trim($order_f['VKBUR']);
    $order_obj->salesTeam=trim($order_f['VKGRP']);
    // make salesarea code
    $order_obj->salesarea_code=implode("|",array($order_obj->typology,$order_obj->commercialOrganization,$order_obj->distributionChannel,$order_obj->productSector,$order_obj->salesOffice));
    // check for salesarea
    if(count($salesareas)){if(!in_array($order_obj->salesarea_code,$salesareas)){continue;}}
    // add order to orders array
    $orders_array[$order_obj->number]=$order_obj;
  }
  //api_dump($orders_array);
  // return
  return $orders_array;
}

/**
 * CRM - RFC Supplier Search
 *
 * @param string $search Search string
 * @return object[] Array of suppliers
 */
function api_crm_rfc_supplier_search($search,$groups=null){
  // check parameters
  if(!is_array($groups)){$groups=array_filter(array($groups),'strlen');}
  // definitions
  $results_array=array();
  $wsrfc_input=array();
  // make input parameters
  $wsrfc_input["I_SEARCH"]="*".str_replace(" ","*",$search)."*";
  foreach($groups as $group_f){$wsrfc_input["R_KTOKK"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$group_f);}
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_SUPPLIER_SEARCH",$wsrfc_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_SUPPLIERS'] as $result_f){
    // build result row
    $supplier_obj=new stdClass();
    // add typology to row
    $supplier_obj->code=trim($result_f['LIFNR']);
    $supplier_obj->name=trim($result_f['NAME1']);
    if(strlen(trim($result_f['NAME2']))){$supplier_obj->name.=" (".trim($result_f['NAME2']).")";}
    $supplier_obj->group=trim($result_f['KTOKK']);
    $supplier_obj->deleted=($result_f['LOEVM']=="X"?true:false);
    // add row to results array
    $results_array[$supplier_obj->code]=$supplier_obj;
  }
  //api_dump($results_array,"results array");
  // return
  return $results_array;
}

/**
 * CRM - RFC Material
 *
 * @param string $material SAP material code
 * @return object|false Material
 */
function api_crm_rfc_material($division,$material){
  // check parameters
  if(!strlen($material)){return false;}
  // definitions
  $wsrfc_input=array();
  // make input parameters
  $wsrfc_input["R_WERKS"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$division);
  $wsrfc_input["R_MATNR"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$material);
  //api_dump($wsrfc_input,"wsrfc_input");
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("sap-prd"))->execute("ZCF_LOOKUP_MATERIAL",$wsrfc_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  $material_rfc=$wsrfc['result']['T_MATERIALS'][0];
  // build order
  $material_obj=new stdClass();
  $material_obj->division=trim($material_rfc['WERKS']);
  $material_obj->typology=trim($material_rfc['MTART']);
  $material_obj->material=trim($material_rfc['MATNR']);
  $material_obj->line=trim($material_rfc['C000100']);
  $material_obj->execution=trim($material_rfc['C000200']);
  $material_obj->profile=trim($material_rfc['C000300']);
  $material_obj->grade=trim($material_rfc['C000400']);
  $material_obj->size=$material_rfc['C000700'];
  $material_obj->length_min=ltrim($material_rfc['C001001'],"0");
  $material_obj->length_max=ltrim($material_rfc['C001002'],"0");
  $material_obj->heattreath=trim($material_rfc['C000500']);
  $material_obj->tollerance=trim($material_rfc['C010400']);
  $material_obj->specification=trim($material_rfc['C090000']);
  $material_obj->euronorm=trim($material_rfc['C020300']);
  //api_dump($material_obj,"material_obj");
  // return
  return $material_obj;
}

/**
 * CRM - RFC Lookup Material
 *
 * @param string[] $divisions Array of sap division codes
 * @param string[] $typologies Array of sap typology codes
 * @param string[] $materials Array of sap material codes
 * @param string[] $lines Array of sap line codes
 * @param string[] $executions Array of sap execution codes
 * @param string[] $profiles Array of sap profile codes
 * @param string[] $grades Array of sap grade codes
 * @param string[] $sizes Array of sap size codes
 * @return object[] Array of materials
 */
function api_crm_rfc_lookup_material($divisions=null,$typologies=null,$materials=null,$lines=null,$executions=null,$profiles=null,$grades=null,$sizes=null){
  // check parameters
  if(!is_array($divisions)){$divisions=array_filter(array($divisions),'strlen');}
  if(!is_array($typologies)){$typologies=array_filter(array($typologies),'strlen');}
  if(!is_array($materials)){$materials=array_filter(array($materials),'strlen');}
  if(!is_array($lines)){$lines=array_filter(array($lines),'strlen');}
  if(!is_array($executions)){$executions=array_filter(array($executions),'strlen');}
  if(!is_array($profiles)){$profiles=array_filter(array($profiles),'strlen');}
  if(!is_array($grades)){$grades=array_filter(array($grades),'strlen');}
  if(!is_array($sizes)){$sizes=array_filter(array($sizes),'strlen');}
  // definitions
  $materials_array=array();
  $wsrfc_input=array();
  // make input parameters
  foreach($divisions as $division_f){$wsrfc_input["R_WERKS"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$division_f);}
  foreach($typologies as $typology_f){$wsrfc_input["R_MTART"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$typology_f);}
  foreach($materials as $material_f){$wsrfc_input["R_MATNR"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$material_f);}
  foreach($lines as $line_f){$wsrfc_input["R_C000100"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$line_f);}
  foreach($executions as $execution_f){$wsrfc_input["R_C000200"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$execution_f);}
  foreach($profiles as $profile_f){$wsrfc_input["R_C000300"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$profile_f);}
  foreach($grades as $grade_f){$wsrfc_input["R_C000400"][]=array("SIGN"=>"I","OPTION"=>"CP","LOW"=>$grade_f);}
  //foreach($sizes as $sizes_f){$wsrfc_input["R_C000700"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$sizes_f);}
  if(count($sizes)==1){$wsrfc_input["R_C000700"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$sizes[0]);}
  else{$wsrfc_input["R_C000700"][]=array("SIGN"=>"I","OPTION"=>"BT","LOW"=>$sizes[0],"HIGH"=>$sizes[1]);}
  //api_dump($wsrfc_input,"wsrfc_input");
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("default"))->execute("ZCF_LOOKUP_MATERIAL",$wsrfc_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_MATERIALS'] as $material_f){
    // build order
    $material_obj=new stdClass();
    $material_obj->division=trim($material_f['WERKS']);
    $material_obj->typology=trim($material_f['MTART']);
    $material_obj->material=trim($material_f['MATNR']);
    $material_obj->line=trim($material_f['C000100']);
    $material_obj->execution=trim($material_f['C000200']);
    $material_obj->profile=trim($material_f['C000300']);
    $material_obj->grade=trim($material_f['C000400']);
    $material_obj->size=$material_f['C000700'];
    $material_obj->length_min=ltrim($material_f['C001001'],"0");
    $material_obj->length_max=ltrim($material_f['C001002'],"0");
    $material_obj->heattreath=trim($material_f['C000500']);
    $material_obj->specification=trim($material_f['C090000']);
    // add material to materials array
    $materials_array[]=$material_obj;
  }
  //api_dump($materials_array,"materials_array");
  // return
  return $materials_array;
}

/**
 * CRM - RFC Lookup Stock
 *
 * @param string[] $companies Array of sap company codes
 * @param string[] $divisions Array of sap division codes
 * @param string[] $warehouses Array of sap warehouse codes
 * @param string[] $typologies Array of sap typology codes
 * @param string[] $materials Array of sap material codes
 * @param string[] $lots Array of sap lot codes
 * @param string[] $lines Array of sap line codes
 * @param string[] $executions Array of sap execution codes
 * @param string[] $profiles Array of sap profile codes
 * @param string[] $grades Array of sap grade codes
 * @param string[] $sizes Array of sap size codes
 * @return object[] Array of materials
 */
function api_crm_rfc_lookup_stock($companies=null,$divisions=null,$warehouses=null,$typologies=null,$materials=null,$lots=null,$lines=null,$executions=null,$profiles=null,$grades=null,$sizes=null){
  // check parameters
  if(!is_array($companies)){$companies=array_filter(array($companies),'strlen');}
  if(!is_array($divisions)){$divisions=array_filter(array($divisions),'strlen');}
  if(!is_array($warehouses)){$warehouses=array_filter(array($warehouses),'strlen');}
  if(!is_array($typologies)){$typologies=array_filter(array($typologies),'strlen');}
  if(!is_array($materials)){$materials=array_filter(array($materials),'strlen');}
  if(!is_array($lots)){$lots=array_filter(array($lots),'strlen');}
  if(!is_array($lines)){$lines=array_filter(array($lines),'strlen');}
  if(!is_array($executions)){$executions=array_filter(array($executions),'strlen');}
  if(!is_array($profiles)){$profiles=array_filter(array($profiles),'strlen');}
  if(!is_array($grades)){$grades=array_filter(array($grades),'strlen');}
  if(!is_array($sizes)){$sizes=array_filter(array($sizes),'strlen');}
  // definitions
  $stocks_array=array();
  $wsrfc_input=array();
  // make input parameters
  foreach($companies as $company_f){$wsrfc_input["R_BURKS"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$company_f);}
  foreach($divisions as $division_f){$wsrfc_input["R_WERKS"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$division_f);}
  foreach($warehouses as $warehouse_f){$wsrfc_input["R_LGORT"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$warehouse_f);}
  foreach($typologies as $typology_f){$wsrfc_input["R_MTART"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$typology_f);}
  foreach($materials as $material_f){$wsrfc_input["R_MATNR"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$material_f);}
  foreach($lots as $lot_f){$wsrfc_input["R_CHARG"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$lot_f);}
  foreach($lines as $line_f){$wsrfc_input["R_LINEA"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$line_f);}
  foreach($executions as $execution_f){$wsrfc_input["R_ESECU"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$execution_f);}
  foreach($profiles as $profile_f){$wsrfc_input["R_PROFI"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$profile_f);}
  foreach($grades as $grade_f){$wsrfc_input["R_MARCA"][]=array("SIGN"=>"I","OPTION"=>"CP","LOW"=>$grade_f);}
  //foreach($sizes as $sizes_f){$wsrfc_input["R_DIM1"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$sizes_f);}
  if(count($sizes)==1){$wsrfc_input["R_DIM1"][]=array("SIGN"=>"I","OPTION"=>"EQ","LOW"=>$sizes[0]);}
  else{$wsrfc_input["R_DIM1"][]=array("SIGN"=>"I","OPTION"=>"BT","LOW"=>$sizes[0],"HIGH"=>$sizes[1]);}
  //api_dump($wsrfc_input,"wsrfc_input");
  // execute wsrfc call
  $wsrfc=(new cWsrfcConnection("sap-prd"))->execute("ZCF_LOOKUP_STOCK",$wsrfc_input);
  //api_dump($wsrfc,"wsrfc");
  // check for errors
  if($wsrfc['error']){api_alerts_add(api_text("wsrfc_alert-error")."<hr>".implode("<br>",$wsrfc['errors']),"danger");}
  // cycle all results
  foreach($wsrfc['result']['T_STOCKS'] as $stock_f){
    // build order
    $stock_obj=new stdClass();
    $stock_obj->company=trim($stock_f['BUKRS']);
    $stock_obj->division=trim($stock_f['WERKS']);
    $stock_obj->warehouse=trim($stock_f['LGORT']);
    $stock_obj->typology=trim($stock_f['MTART']);
    $stock_obj->material=trim($stock_f['MATNR']);
    $stock_obj->lot=trim($stock_f['CHARG']);
    $stock_obj->location=trim($stock_f['UBICAZ']);
    $stock_obj->weight_total=$stock_f['BTGEW'];
    $stock_obj->weight_engaged=$stock_f['RSMNG'];
    $stock_obj->weight_free=$stock_f['LABST'];
    $stock_obj->weight_quality=$stock_f['INSME'];
    $stock_obj->weight_return=$stock_f['RETME'];
    $stock_obj->weight_locked=$stock_f['SPEME'];
    $stock_obj->weight_unusable=$stock_f['EINME'];
    $stock_obj->grade=trim($stock_f['MARCA']);
    $stock_obj->line=trim($stock_f['LINEA']);
    $stock_obj->execution=trim($stock_f['ESECU']);
    $stock_obj->profile=trim($stock_f['PROFI']);
    $stock_obj->size=$stock_f['DIM1'];
    $stock_obj->length_min=$stock_f['LUNG_DA'];
    $stock_obj->length_max=ltrim($stock_f['LUNG_A'],"0");
    $stock_obj->heattreath=trim($stock_f['TRATT']);
    $stock_obj->tollerance=trim($stock_f['SIGLAT']);
    $stock_obj->specification=trim($stock_f['CAPIT_CL']);
    // add stock to stocks array
    $stocks_array[]=$stock_obj;
  }
  //api_dump($stocks_array,"stocks_array");
  // return
  return $stocks_array;
}

/**
 * CRM - RFC Order Heading
 *
 * @param string[] $salesarea Array of sales areas sap codes
 * @param string $salesteam Sales group sap code
 * @param string $request_number Customer request number
 * @param string $request_date Customer request date
 * @param string $customer Customer sap code
 * @param string $receiver Receiver sap code
 * @param string $billing Billing sap code
 * @param string $supplier Supplier sap code
 * @param string $availability Availability date in sap format (YYYYMMDD)
 * @param string $payment Payment sap code
 * @param string $currency Currency in 3 digit format
 * @param string $referent Referent name
 * @param boolean $commit False for test
 * @return boolean
 */
function api_crm_rfc_customer_order_heading($salesarea,$salesteam,$request_number,$request_date,$customer,$receiver,$billing,$supplier,$availability,$payment,$currency,$referent,$commit=false){
  // check parameters
  if(!is_array($salesarea)){return false;}
  if(count($salesarea)!=5){return false;}
  /** @tip checks */
  // build wsrfc input
  $input=array(
    "I_HEADER"=>array(
      "AUART"=>$salesarea[0],
      "VKORG"=>$salesarea[1],
      "VTWEG"=>$salesarea[2],
      "SPART"=>$salesarea[3],
      "VKBUR"=>$salesarea[4],
      "VKGRP"=>$salesteam,
      "PURCH_NO_C"=>$request_number,
      "BSTDK"=>$request_date,
      "REQ_DATE_H"=>$availability,
      "ZLSCH"=>$payment,
      "COMMITTENTE"=>$customer,
      "DESTINATARIO_MERCI"=>$receiver,
      "FORNITORE"=>$supplier,
      "DESTINATARIO_FATTURA"=>$billing,
      "WAERK"=>$currency,
      "NAME"=>substr($referent,0,35)
    )
  );
  // check for commit
  if(!$commit){$input['I_TEST']="X";}
  // execute wsrfc
  $wsrfc=(new cWsrfcConnection("default"))->execute("Z_TESTATA_ODV",$input);
  // debug
  api_dump($wsrfc,"wsrfc");
  // get created odv
  $odv_heading=trim($wsrfc['result']['O_ODV']);
  // check for odv
  if(!$odv_heading || strlen($odv_heading)!=10){
    echo api_tag("strong","Customer Offer Error");
    echo "<pre>";
    print_r($wsrfc['errors']);
    print_r($wsrfc['result']['TORETURN']);
    echo "</pre>";
    //return false;
    die();
  }
  // return order number
  return $odv_heading;
}

/**
 * CRM - RFC Order Element
 *
 * @param string $order Order sap number
 * @param string $division Division sap code
 * @param string $warehouse Warehouse sap code
 * @param string $material Material sap code
 * @param integer $quantity Quantity weight
 * @param string $unit Quantity unit
 * @param string $currency Price currency
 * @param double $price Price value
 * @param boolean $alloy True for extra alloy
 * @param integer $cutting Price for cutting
 * @param string $availability Availability date in sap format (YYYYMMDD)
 * @param string $note Note in sap text
 * @param string $lot Lot in sap text
 * @param integer $size Size in sap text
 * @param integer $length Length in sap text
 * @param integer $pieces Pieces in sap text
 * @param boolean $commit False for test
 */
function api_crm_rfc_customer_order_element($order,$division,$warehouse,$material,$quantity,$unit,$currency,$price,$alloy,$cutting,$availability,$customer_material,$note=null,$lot=null,$size=null,$length=null,$pieces=null,$commit=false){
  // check parameters
  /** @tip checks */
  // build wsrfc input
  $input=array(
    "I_ODV"=>$order,
    "I_ITEM"=>array(
      "MATNR"=>$material,
      "KWMENG"=>$quantity,
      "VRKME"=>$unit,
      "DIVISIONE"=>$division,
      "MAGAZZINO"=>$warehouse,
      "DATA_SCHEDULAZIONE"=>$availability,
      "MAT_CLIENTE"=>$customer_material
    ),
    "TCONDIZIONI"=>array(
      array(
        "KSCHL"=>"PR00",
        "KBETR"=>$price,
        "WAERS"=>$currency
      )
    ),
    "TTEXT"=>array(
      array(
        "TEXT_ID"=>"001",
        "TEXT_LINE"=>$note
      ),
      array(
        "TEXT_ID"=>"0001",
        "TEXT_LINE"=>$note
      ),
      array(
        "TEXT_ID"=>"ZMIS",
        "TEXT_LINE"=>$size
      ),
      array(
        "TEXT_ID"=>"ZLUN",
        "TEXT_LINE"=>$length
      )
    )
  );
  // check for pieces
  if($pieces>0){$input['TTEXT'][]=array("TEXT_ID"=>"ZTAG","TEXT_LINE"=>$pieces);}
  // check for cutting
  if($cutting>0){$input['TCONDIZIONI'][]=array("KSCHL"=>"ZCTI","KBETR"=>$cutting,"WAERS"=>$currency);}
  // check for alloy
  if(!$alloy){
    $input['TCONDIZIONI'][]=array("KSCHL"=>"ZAZZ","ZDELETE"=>"X");
    $input['TCONDIZIONI'][]=array("KSCHL"=>"ZEDL","ZDELETE"=>"X");
  }
  // check for commit
  if(!$commit){$input['I_TEST']="X";}
  // execute wsrfc
  $wsrfc=(new cWsrfcConnection("default"))->execute("Z_POSIZIONI_ODV",$input);
  // debug
  api_dump($wsrfc);
  // get created position
  $odv_element=trim($wsrfc['result']['OPOSNR']);
  // check for position
  if(!$odv_element || strlen($odv_element)!=6 || $odv_element=='000000'){
    echo api_tag("strong","Customer offer error");
    echo "<pre>";
    print_r($wsrfc['result']['TORETURN']);
    echo "</pre>";
    //return false;
    die();
  }
  // return position number
  return $odv_element;
}
