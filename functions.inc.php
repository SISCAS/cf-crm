<?php
/**
 * CRM Functions
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

// include classes
require_once(DIR."modules/crm/classes/cCrmDivision.class.php");
require_once(DIR."modules/crm/classes/cCrmSalesarea.class.php");
require_once(DIR."modules/crm/classes/cCrmWarehouse.class.php");
require_once(DIR."modules/crm/classes/cCrmCustomer.class.php");
require_once(DIR."modules/crm/classes/cCrmContact.class.php");
require_once(DIR."modules/crm/classes/cCrmRelationship.class.php");
require_once(DIR."modules/crm/classes/cCrmSpecification.class.php");
require_once(DIR."modules/crm/classes/cCrmProduct.class.php");
require_once(DIR."modules/crm/classes/cCrmOffer.class.php");

// include rfc functions
require_once(DIR."modules/crm/functions-rfc.inc.php");

/**
 * CRM - Authorized Divisions
 *
 * @return string Authorization query where
 */
function api_crm_authorizedDivisionsQuery(){
  // definitions
  $authorized_divisions_array=array();
  // get all authorized divisions
  foreach(api_crm_availableDivisions(true) as $division_fobj){$authorized_divisions_array[]=$division_fobj->id;}
  // return
  return "\n AND ( `fkDivision` IN ('".implode("','",$authorized_divisions_array)."') )";
}

/**
 * CRM - Available Tags
 *
 * @return object[] Array of available tags
 */
function api_crm_availableTags(){
  // definitions
  $tags_array=array();
  // get available tags
  $results=$GLOBALS['database']->queryObjects("SELECT DISTINCT(tag) AS tag FROM `crm__customers__tags` ORDER BY `tag` ASC");
  foreach($results as $result){
    // add tag to tags array
    $tags_array[]=stripslashes($result->tag);
  }
  // return
  return $tags_array;
}

/**
 * CRM - Available Divisions
 *
 * @return object[] Array of available divisions
 */
function api_crm_availableDivisions($authorized_only=false){
  // definitions
  $divisions_array=array();
  // get available divisions
  $results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__divisions` WHERE 1 ORDER BY `name` ASC");
  foreach($results as $result){
    // get division
    $division_obj=new cCrmDivision($result);
    // check for authorization
    if($authorized_only && !$division_obj->checkAuthorizations()){continue;}
    // add division to divisions array
    $divisions_array[$division_obj->id]=$division_obj;
  }
  // return
  return $divisions_array;
}

/**
 * CRM - Available Customers
 *
 * @return object[] Array of available customers
 */
function api_crm_availableCustomers(){
  // definitions
  $customers_array=array();
  // get available customers
  $results=$GLOBALS['database']->queryObjects("SELECT * FROM `crm__customers` WHERE 1 ORDER BY `name` ASC");
  foreach($results as $result){
    // get customer
    $customer_obj=new cCrmCustomer($result);
    // add customer to customers array
    $customers_array[$customer_obj->id]=$customer_obj;
  }
  // return
  return $customers_array;
}

/**
 * CRM - Available Payment Methods
 *
 * @return object[] Array of available payment methods
 */
function api_crm_availablePaymentsMethods(){
  // get codings
  $paymentsMethods_array=api_crm_rfc_codings("PAYMENT_METHOD");
  // return
  return $paymentsMethods_array;
}

/**
 * CRM - Available Payment Conditions
 *
 * @return object[] Array of available payment conditions
 */
function api_crm_availablePaymentsConditions(){
  // get codings
  $paymentsConditions_array=api_crm_rfc_codings("PAYMENT_CONDITION");
  // return
  return $paymentsConditions_array;
}

/**
 * CRM - Specifications - Available Compatibilities
 *
 * @return object[] Array of available compatibilities
 */
function api_crm_specifications_availableCompatibilities(){
  // definitions
  $compatibilities_array=array(
    "undefined"=>(object)array("code"=>"undefined","text"=>api_text("crm_specification_result-undefined"),"icon"=>api_icon("fa-minus-circle",api_text("crm_specification_result-undefined"))),
    "compatible"=>(object)array("code"=>"compatible","text"=>api_text("crm_specification_result-compatible"),"icon"=>api_icon("fa-check-circle",api_text("crm_specification_result-compatible"),"text-success")),
    "derogable"=>(object)array("code"=>"derogable","text"=>api_text("crm_specification_result-derogable"),"icon"=>api_icon("fa-plus-circle",api_text("crm_specification_result-derogable"),"text-warning")),
    "incompatible"=>(object)array("code"=>"incompatible","text"=>api_text("crm_specification_result-incompatible"),"icon"=>api_icon("fa-times-circle",api_text("crm_specification_result-incompatible"),"text-danger"))
  );
  // return
  return $compatibilities_array;
}

/**
 * CRM - Relationships - Available Typologies
 *
 * @return object[] Array of available typologies
 */
function api_crm_relationships_availableTypologies(){
  // definitions
  $typologies_array=array(
    "generic"=>(object)array("code"=>"generic","text"=>api_text("crm_relationship_typology-generic"),"icon"=>api_icon("fa-comment-o",api_text("crm_relationship_typology-generic"))),
    "meeting"=>(object)array("code"=>"meeting","text"=>api_text("crm_relationship_typology-meeting"),"icon"=>api_icon("fa-handshake-o",api_text("crm_relationship_typology-meeting"))),
    "claim"=>(object)array("code"=>"claim","text"=>api_text("crm_relationship_typology-claim"),"icon"=>api_icon("fa-file-excel-o",api_text("crm_relationship_typology-claim"))),
    "other"=>(object)array("code"=>"other","text"=>api_text("crm_relationship_typology-other"),"icon"=>api_icon("fa-hand-spock-o",api_text("crm_relationship_typology-other")))
  );
  // return
  return $typologies_array;
}

/**
 * CRM - Relationships - Available Feelings
 *
 * @return object[] Array of available feelings
 */
function api_crm_relationships_availableFeelings(){
  // definitions
  $feelings_array=array(
    "positive"=>(object)array("code"=>"positive","text"=>api_text("crm_relationship_feeling-positive"),"icon"=>api_icon("fa-smile-o",api_text("crm_relationship_feeling")." ".strtolower(api_text("crm_relationship_feeling-positive")),"text-success")),
    "regular"=>(object)array("code"=>"regular","text"=>api_text("crm_relationship_feeling-regular"),"icon"=>api_icon("fa-meh-o",api_text("crm_relationship_feeling")." ".strtolower(api_text("crm_relationship_feeling-regular")),"text-warning")),
    "negative"=>(object)array("code"=>"negative","text"=>api_text("crm_relationship_feeling-negative"),"icon"=>api_icon("fa-frown-o",api_text("crm_relationship_feeling")." ".strtolower(api_text("crm_relationship_feeling-negative")),"text-danger"))
  );
  // return
  return $feelings_array;
}

/**
 * CRM - Offers - Available Status
 *
 * @return object[] Array of available status
 */
function api_crm_offers_availableStatus(){
  // definitions
  $status_array=array(
    "inserted"=>(object)array("code"=>"inserted","text"=>api_text("crm_offer_status-inserted"),"icon"=>api_icon("fa-minus-circle",api_text("crm_offer_status-inserted"))),
    "ordered"=>(object)array("code"=>"ordered","text"=>api_text("crm_offer_status-ordered"),"icon"=>api_icon("fa-check-circle",api_text("crm_offer_status-ordered"),"text-success")),
    "expired"=>(object)array("code"=>"expired","text"=>api_text("crm_offer_status-expired"),"icon"=>api_icon("fa-plus-circle",api_text("crm_offer_status-expired"),"text-danger")),
    "rejected"=>(object)array("code"=>"rejected","text"=>api_text("crm_offer_status-rejected"),"icon"=>api_icon("fa-times-circle",api_text("crm_offer_status-rejected"),"text-danger"))
  );
  // return
  return $status_array;
}

/**
 * CRM - Offers - Available Issues
 *
 * @return object[] Array of available issues
 */
function api_crm_offers_availableIssues(){
  // definitions
  $issues_array=array(
    "price"=>(object)array("code"=>"price","text"=>api_text("crm_offer_issue-price"),"icon"=>api_icon("fa-money",api_text("crm_offer_issue-price"))),
    "quality"=>(object)array("code"=>"quality","text"=>api_text("crm_offer_issue-quality"),"icon"=>api_icon("fa-certificate",api_text("crm_offer_issue-quality"))),
    "availability"=>(object)array("code"=>"availability","text"=>api_text("crm_offer_issue-availability"),"icon"=>api_icon("fa-calendar-times-o",api_text("crm_offer_issue-availability"))),
    "preparation"=>(object)array("code"=>"preparation","text"=>api_text("crm_offer_issue-preparation"),"icon"=>api_icon("fa-clock-o",api_text("crm_offer_issue-preparation"))),
    "unneeded"=>(object)array("code"=>"unneeded","text"=>api_text("crm_offer_issue-unneeded"),"icon"=>api_icon("fa-chain-broken",api_text("crm_offer_issue-unneeded")))
  );
  // return
  return $issues_array;
}

/**
 * CRM - Offers - Available Grades
 *
 * @return object[] Array of available grades
 */
function api_crm_offers_availableGrades(){
  // definitions
  $grades_array=array(
    "286"=>(object)array("code"=>"*286*","text"=>"*286*"),
    "303"=>(object)array("code"=>"*303*","text"=>"*303*"),
    "304"=>(object)array("code"=>"*304*","text"=>"*304*"),
    "309"=>(object)array("code"=>"*309*","text"=>"*309*"),
    "310"=>(object)array("code"=>"*310*","text"=>"*310*"),
    "314"=>(object)array("code"=>"*314*","text"=>"*314*"),
    "F316"=>(object)array("code"=>"*F316*","text"=>"*F316*"),
    "316L"=>(object)array("code"=>"*316L*","text"=>"*316L*"),
    "316LM"=>(object)array("code"=>"316LM*","text"=>"316LM*"),
    "316T"=>(object)array("code"=>"*316T*","text"=>"*316T*"),
    "321"=>(object)array("code"=>"*321*","text"=>"*321*"),
    "329A"=>(object)array("code"=>"*329A*","text"=>"*329A*"),
    "329S"=>(object)array("code"=>"*329S*","text"=>"*329S*"),
    "347"=>(object)array("code"=>"*347*","text"=>"*347*"),
    "350"=>(object)array("code"=>"*350*","text"=>"*350*"),
    "354"=>(object)array("code"=>"*354*","text"=>"*354*"),
    "410"=>(object)array("code"=>"*410*","text"=>"*410*"),
    "415M"=>(object)array("code"=>"*415M*","text"=>"*415M*"),
    "E415"=>(object)array("code"=>"*E415*","text"=>"*E415*"),
    "416"=>(object)array("code"=>"*416*","text"=>"*416*"),
    "420A"=>(object)array("code"=>"*420A*","text"=>"*420A*"),
    "420B"=>(object)array("code"=>"*420B*","text"=>"*420B*"),
    "420C"=>(object)array("code"=>"*420C*","text"=>"*420C*"),
    "420D"=>(object)array("code"=>"*420D*","text"=>"*420D*"),
    "430"=>(object)array("code"=>"*430*","text"=>"*430*"),
    "430F"=>(object)array("code"=>"*430F*","text"=>"*430F*"),
    "431"=>(object)array("code"=>"*431*","text"=>"*431*"),
    "434"=>(object)array("code"=>"*434*","text"=>"*434*"),
    "440B"=>(object)array("code"=>"*440B*","text"=>"*440B*"),
    "440C"=>(object)array("code"=>"*440C*","text"=>"*440C*"),
    "616"=>(object)array("code"=>"*616*","text"=>"*616*"),
    "616C"=>(object)array("code"=>"*616C*","text"=>"*616C*"),
    "625"=>(object)array("code"=>"*625*","text"=>"*625*"),
    "630"=>(object)array("code"=>"*630*","text"=>"*630*"),
    "718"=>(object)array("code"=>"*718*","text"=>"*718*"),
    "SN625"=>(object)array("code"=>"SN625*","text"=>"SN625*"),
    "SN825"=>(object)array("code"=>"SN825*","text"=>"SN825*"),
    "904"=>(object)array("code"=>"*904*","text"=>"*904*"),
	  "------"=>(object)array("code"=>"------","text"=>"----------------------------------------------------------------")
	);
  // return
  return array_merge($grades_array,api_crm_rfc_grades());
}

/**
 * CRM - Offers - Available Euronorms
 *
 * @return object[] Array of available wn
 */
function api_crm_offers_availableEuronorms(){
  // definitions
  $return_array=array();
  $wn_array=array(
    "1.4005","1.4006","1.4016","1.4021","1.4028","1.4029","1.4031","1.4034","1.4035","1.4037","1.4057",
    "1.4104","1.4105","1.4112","1.4113","1.4122","1.4125","1.4301","1.4305","1.4306","1.4307","1.4313",
    "1.4401","1.4404","1.4410","1.4418","1.4435","1.4460","1.4462","1.4501","1.4539","1.4541","1.4542",
    "1.4547","1.4567","1.4570","1.4550","1.4571","1.4578","1.4828","1.4845","1.4922","1.4935","1.4980"
  );
  // sort
  sort($wn_array);
  // build objects
  foreach($wn_array as $wm){$return_array[$wm]=(object)array("code"=>$wm,"text"=>$wm);}
  // return
  return $return_array;
}

/**
 * CRM - Offers - Available Executions
 *
 * @return object[] Array of available executions
 */
function api_crm_offers_availableExecutions(){
  // definitions
  $executions_array=array(
    "LAM"=>(object)array("code"=>"LAM","text"=>"Laminati","exp"=>"Laminati"),
    "LMA"=>(object)array("code"=>"LMA","text"=>"Lavorati di Macchina","exp"=>"Pelati"),
    "PEL"=>(object)array("code"=>"PEL","text"=>"Pelati","exp"=>"Pelati"),
    "PES"=>(object)array("code"=>"PES","text"=>"Pelati","exp"=>"Pelati"),
    "RET"=>(object)array("code"=>"RET","text"=>"Rettificati","exp"=>"Rettificati"),
    "SPE"=>(object)array("code"=>"SPE","text"=>"Sgrossati di Pelatura","exp"=>"Pelati"),
    "SRE"=>(object)array("code"=>"SRE","text"=>"Sgrossati di Rettifica","exp"=>"Rettificati"),
    "TRA"=>(object)array("code"=>"TRA","text"=>"Trafilati","exp"=>"Trafilati"),
    "FUC"=>(object)array("code"=>"FUC","text"=>"Fucinati","exp"=>"Fucinati"),
    "COL"=>(object)array("code"=>"COL","text"=>"Colati","exp"=>"Colati"),
    "DEC"=>(object)array("code"=>"DEC","text"=>"Declassati","exp"=>"Declassati"),
    "COR"=>(object)array("code"=>"COR","text"=>"Corrugati","exp"=>"Corrugati"),
    "CBR"=>(object)array("code"=>"CBR","text"=>"CBR","exp"=>"CBR"),
    "CBT"=>(object)array("code"=>"CBT","text"=>"CBT","exp"=>"CBT"),
    "LAP"=>(object)array("code"=>"LAP","text"=>"Laminati x Pelatura","exp"=>"LAM x Pelatura"),
    "OGP"=>(object)array("code"=>"OGP","text"=>"OGP","exp"=>"One Pass Ground"),
    "SPT"=>(object)array("code"=>"SPT","text"=>"Tubi Sgrossati","exp"=>"Tubi Sgrossati"),
  );
  // return
  return $executions_array;
}

/**
 * CRM - Offers - Available Profiles
 *
 * @return object[] Array of available profiles
 */
function api_crm_offers_availableProfiles(){
  // definitions
  $profiles_array=array(
    "ESAGONI"=>(object)array("code"=>"ESAGONI","text"=>"ESAGONI","exp"=>"Esagoni"),
    "TONDI"=>(object)array("code"=>"TONDI","text"=>"TONDI","exp"=>"Tondi"),
    "BILL.TON"=>(object)array("code"=>"BILL.TON","text"=>"BILL.TON","exp"=>"Billette Tonde"),
    "BLUMI"=>(object)array("code"=>"BLUMI","text"=>"BLUMI","exp"=>"Blumi"),
    "BRAMME"=>(object)array("code"=>"BRAMME","text"=>"BRAMME","exp"=>"Bramme"),
    "DISEGNO"=>(object)array("code"=>"DISEGNO","text"=>"DISEGNO","exp"=>"Pezzi a Disegno"),
    "ESAG.ROT"=>(object)array("code"=>"ESAG.ROT","text"=>"ESAG.ROT","exp"=>"Esagoni in Rotolo"),
    "ESAGONIR"=>(object)array("code"=>"ESAGONIR","text"=>"ESAGONIR","exp"=>"Esagoni R"),
    "FILI"=>(object)array("code"=>"FILI","text"=>"FILI","exp"=>"Fili"),
    "LIN.OTT."=>(object)array("code"=>"LIN.OTT.","text"=>"LIN.OTT.","exp"=>"Lingotti Ottagonali"),
    "LIN.QUA."=>(object)array("code"=>"LIN.QUA.","text"=>"LIN.QUA.","exp"=>"Lingotti QUA"),
    "LIN.TON."=>(object)array("code"=>"LIN.TON.","text"=>"LIN.TON.","exp"=>"Lingotti Ton"),
    "MANDRINI"=>(object)array("code"=>"MANDRINI","text"=>"MANDRINI","exp"=>"Madrini"),
    "PIAT.S.V"=>(object)array("code"=>"PIAT.S.V","text"=>"PIAT.S.V","exp"=>"Piatti Spigolo Vivo"),
    "PIATTI"=>(object)array("code"=>"PIATTI","text"=>"PIATTI","exp"=>"Piatti"),
    "QUADRI"=>(object)array("code"=>"QUADRI","text"=>"QUADRI","exp"=>"Quadri"),
    "SBOZ.TON"=>(object)array("code"=>"SBOZ.TON","text"=>"SBOZ.TON","exp"=>"Sbozzati Tondi"),
    "TON.1020"=>(object)array("code"=>"TON.1020","text"=>"TON.1020","exp"=>"TON.1020"),
    "TON.500"=>(object)array("code"=>"TON.500","text"=>"TON.500","exp"=>"TON.500"),
    "TON.800"=>(object)array("code"=>"TON.800","text"=>"TON.800","exp"=>"TON.800"),
    "TON.CCO"=>(object)array("code"=>"TON.CCO","text"=>"TON.CCO","exp"=>"TON.CCO"),
    "TON.ROT."=>(object)array("code"=>"TON.ROT.","text"=>"TON.ROT.","exp"=>"Tondi in Rotolo"),
    "TON.S.S."=>(object)array("code"=>"TON.S.S.","text"=>"TON.S.S.","exp"=>"TON.S.S.")
  );
  // return
  return $profiles_array;
}

/**
 * CRM - Offers - Available Heat Treatments
 *
 * @return object[] Array of available heat treatments
 */
function api_crm_offers_availableHeatTreatments(){
  // definitions
  $tollerances_array=array(
    "BN"=>(object)array("code"=>"BN","text"=>"Bonificato [BN]"),
    "RL"=>(object)array("code"=>"RL","text"=>"Ricotto [RL]"),
    "RP"=>(object)array("code"=>"RP","text"=>"Ricotto [RP]"),
    "RS"=>(object)array("code"=>"RS","text"=>"Ricotto [RS]"),
    "SI"=>(object)array("code"=>"SI","text"=>"Invecchiato [SI]"),
  );
  // return
  return $tollerances_array;
}

/**
 * CRM - Offers - Available Tollerances
 *
 * @return object[] Array of available tollerances
 */
function api_crm_offers_availableTollerances(){
  // definitions
  $tollerances_array=array(
    "015"=>(object)array("code"=>"015","text"=>"-0/+1,5"),
    "020"=>(object)array("code"=>"020","text"=>"-0/+2"),
    "025"=>(object)array("code"=>"025","text"=>"-0/+2,5"),
    "030"=>(object)array("code"=>"030","text"=>"-0/+3"),
    "H8"=>(object)array("code"=>"H8","text"=>"H8"),
    "H9"=>(object)array("code"=>"H9","text"=>"H9"),
    "H10"=>(object)array("code"=>"H10","text"=>"H10"),
    "H11"=>(object)array("code"=>"H11","text"=>"H11"),
    "K11"=>(object)array("code"=>"K11","text"=>"K11"),
    "-----"=>(object)array("code"=>"-----","text"=>"----------------------------------------------------------------"),
    "ECLIEN"=>(object)array("code"=>"ECLIEN","text"=>"ECLIEN"),
    "HSTD"=>(object)array("code"=>"HSTD","text"=>"HSTD"),
    "ISOE6"=>(object)array("code"=>"ISOE6","text"=>"ISOE6"),
    "ISOE7"=>(object)array("code"=>"ISOE7","text"=>"ISOE7"),
    "ISOE8"=>(object)array("code"=>"ISOE8","text"=>"ISOE8"),
    "ISOE9"=>(object)array("code"=>"ISOE9","text"=>"ISOE9"),
    "ISOF6"=>(object)array("code"=>"ISOF6","text"=>"ISOF6"),
    "ISOF7"=>(object)array("code"=>"ISOF7","text"=>"ISOF7"),
    "ISOF8"=>(object)array("code"=>"ISOF8","text"=>"ISOF8"),
    "ISOF9"=>(object)array("code"=>"ISOF9","text"=>"ISOF9"),
    "ISOG6"=>(object)array("code"=>"ISOG6","text"=>"ISOG6"),
    "ISOH10"=>(object)array("code"=>"ISOH10","text"=>"ISOH10"),
    "ISOH11"=>(object)array("code"=>"ISOH11","text"=>"ISOH11"),
    "ISOH12"=>(object)array("code"=>"ISOH12","text"=>"ISOH12"),
    "ISOH13"=>(object)array("code"=>"ISOH13","text"=>"ISOH13"),
    "ISOH14"=>(object)array("code"=>"ISOH14","text"=>"ISOH14"),
    "ISOH6"=>(object)array("code"=>"ISOH6","text"=>"ISOH6"),
    "ISOH7"=>(object)array("code"=>"ISOH7","text"=>"ISOH7"),
    "ISOH8"=>(object)array("code"=>"ISOH8","text"=>"ISOH8"),
    "ISOH9"=>(object)array("code"=>"ISOH9","text"=>"ISOH9"),
    "ISOJ6"=>(object)array("code"=>"ISOJ6","text"=>"ISOJ6"),
    "ISOJ9"=>(object)array("code"=>"ISOJ9","text"=>"ISOJ9"),
    "ISOK10"=>(object)array("code"=>"ISOK10","text"=>"ISOK10"),
    "ISOK11"=>(object)array("code"=>"ISOK11","text"=>"ISOK11"),
    "ISOK12"=>(object)array("code"=>"ISOK12","text"=>"ISOK12"),
    "ISOK13"=>(object)array("code"=>"ISOK13","text"=>"ISOK13"),
    "ISOK14"=>(object)array("code"=>"ISOK14","text"=>"ISOK14"),
    "ISOK15"=>(object)array("code"=>"ISOK15","text"=>"ISOK15"),
    "ISOK6"=>(object)array("code"=>"ISOK6","text"=>"ISOK6"),
    "ISOK8"=>(object)array("code"=>"ISOK8","text"=>"ISOK8"),
    "ISOK9"=>(object)array("code"=>"ISOK9","text"=>"ISOK9"),
    "JS6"=>(object)array("code"=>"JS6","text"=>"JS6"),
    "JS8"=>(object)array("code"=>"JS8","text"=>"JS8"),
    "JS10"=>(object)array("code"=>"JS10","text"=>"JS10"),
    "JS11"=>(object)array("code"=>"JS11","text"=>"JS11"),
    "NSA"=>(object)array("code"=>"NSA","text"=>"NSA"),
    "PVED.C"=>(object)array("code"=>"PVED.C","text"=>"PVED.C"),
    "USA"=>(object)array("code"=>"USA","text"=>"USA"),
  );
  // return
  return $tollerances_array;
}

/**
 * CRM - Divisions - Event Save
 *
 * @param object|integer $division Division object or ID
 * @param string $event Event to log
 * @param string $level Level [information|warning|error|debug]
 * @param string $note Note
 * @return boolean true or false
 */
function api_crm_divisions_event_save($division,$event,$level="information",$note=null){
  // check parameters
  $division_obj=new cCrmDivision($division);
  //if(!($division_obj instanceof cCrmDivision)){return false;}
  if(!$division_obj->id){return false;}
  if(!$event){return false;}
  // build event query objects
  $event_qobj=new stdClass();
  $event_qobj->fkDivision=$division_obj->id;
  if($GLOBALS['session']->user->id){$event_qobj->fkUser=$GLOBALS['session']->user->id;}
  $event_qobj->timestamp=time();
  $event_qobj->event=$event;
  $event_qobj->level=$level;
  $event_qobj->note=$note;
  // debug
  api_dump($event_qobj,"event query object");
  // insert event
  $event_qobj->id=$GLOBALS['database']->queryInsert("crm__divisions__events",$event_qobj);
  // check for event
  if(!in_array($event,array("divisionCreated","divisionUpdated","divisionDeleted","divisionUndeleted","divisionRemoved"))){
    // update division
    api_object_update("crm__divisions",$division_obj->id);
  }
  // check and return
  if($event_qobj->id){return true;}
  else{return false;}
}

/**
 * CRM - Customers - Event Save
 *
 * @param object|integer $customer Customer object or ID
 * @param string $event Event to log
 * @param string $level Level [information|warning|error|debug]
 * @param string $note Note
 * @return boolean true or false
 */
function api_crm_customers_event_save($customer,$event,$level="information",$note=null){
  // check parameters
  $customer_obj=new cCrmCustomer($customer);
  //if(!($customer_obj instanceof cCrmCustomer)){return false;}
  if(!$customer_obj->id){return false;}
  if(!$event){return false;}
  // build event query objects
  $event_qobj=new stdClass();
  $event_qobj->fkCustomer=$customer_obj->id;
  if($GLOBALS['session']->user->id){$event_qobj->fkUser=$GLOBALS['session']->user->id;}
  $event_qobj->timestamp=time();
  $event_qobj->event=$event;
  $event_qobj->level=$level;
  $event_qobj->note=$note;
  // debug
  api_dump($event_qobj,"event query object");
  // insert event
  $event_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__events",$event_qobj);
  // check for event
  if(!in_array($event,array("customerCreated","customerUpdated","customerDeleted","customerUndeleted","customerRemoved"))){
    // update division
    api_object_update("crm__customers",$customer_obj->id);
  }
  // check and return
  if($event_qobj->id){return true;}
  else{return false;}
}
