<?php
/**
 * CRM
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
// definitions
$module_name="crm";
$module_repository_url="https://bitbucket.org/SISCAS/cf-crm/";
$module_repository_version_url="https://bitbucket.org/SISCAS/cf-crm/raw/master/VERSION.txt";
$module_required_modules=array("wsrfc");
