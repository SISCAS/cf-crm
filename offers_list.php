<?php
/**
 * CRM - Offers List
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorization
 api_checkAuthorization("crm-customers_view","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("offers_list"));
 // definitions
 $offers_array=array();
 $divisions_array=array();
 $customers_array=array();
 $users_array=array();
 // get authorized divisions
 foreach(api_crm_availableDivisions(true) as $division_fobj){$divisions_array[$division_fobj->id]=$division_fobj->name;}
 // get customers
 $customers_results=$GLOBALS['database']->queryObjects("SELECT `id`,`code`,`name` FROM `crm__customers` ORDER BY `name`");
 foreach($customers_results as $result_f){$customers_array[$result_f->id]=$result_f->code." ".$result_f->name;}
 // get users
 foreach(cUser::availables() as $user_fobj){$users_array[$user_fobj->id]=$user_fobj->fullname;}
 // build filter
 $filter=new strFilter();
 $filter->addSearch(array("id","request_number","salesorder_number","note"));
 $filter->addItem(api_text("offers_list-filter-user"),$users_array,"addFkUser",null,"user");
 $filter->addItem(api_text("offers_list-filter-status"),api_crm_offers_availableStatus(),"status",null,"status");
 $filter->addItem(api_text("offers_list-filter-division"),$divisions_array,"fkDivision");
 $filter->addItem(api_text("offers_list-filter-customer"),$customers_array,"fkCustomer",null,"customer");
 $filter->addItem(api_text("offers_list-filter-important"),array("1"=>api_text("offers_list-filter-important")),"important",null,"important");
 $filter->addItem(api_text("offers_list-filter-grade"),api_crm_offers_availableGrades(),"grade",null,"grade");
 $filter->addItem(api_text("offers_list-filter-execution"),api_crm_offers_availableExecutions(),"execution",null,"execution");
 $filter->addItem(api_text("offers_list-filter-profile"),api_crm_offers_availableProfiles(),"profile",null,"profile");
 $filter->addItemRange(api_text("offers_list-filter-size"),array(1,2,3),array(4,5,6),"size",null,"size");
 // make query where
 $where_query=$filter->getQueryWhere();
 if(!$where_query){$where_query="1";}
 $where_query.=api_crm_authorizedDivisionsQuery();
 // build query object
 $query=new cQuery("crm__customers__offers",$where_query);
 $query->addQueryOrderField("id","DESC");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$offers_array[$result_f->id]=new cCrmOffer($result_f);}
 // build table
 $table=new strTable(api_text("offers_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("offers_list-th-id"),"nowrap");
 $table->addHeader(api_text("offers_list-th-customer"),null,"100%");
 $table->addHeader(api_text("offers_list-th-request"),"nowrap");
 $table->addHeader(api_text("offers_list-th-grade"),"nowrap");
 $table->addHeader(api_text("offers_list-th-execution"),"nowrap");
 $table->addHeader(api_text("offers_list-th-profile"),"nowrap");
 $table->addHeader(api_text("offers_list-th-size"),"nowrap");
 $table->addHeader(api_text("offers_list-th-length"),"nowrap");
 $table->addHeader(api_text("offers_list-th-quantity_pieces"),"nowrap");
 $table->addHeader(api_text("offers_list-th-quantity_weight"),"nowrap");
 $table->addHeader(api_text("offers_list-th-price_list"),"nowrap text-right");
 $table->addHeader(api_text("offers_list-th-price_offer"),"nowrap text-right");
 $table->addHeader(api_text("offers_list-th-confirmation"),"nowrap");
 $table->addHeader(api_text("offers_list-th-user"),"nowrap text-right");
 $table->addHeader(api_text("offers_list-th-division"),"nowrap text-right");
 $table->addHeader("&nbsp;",null,16);
 $table->addHeader("&nbsp;",null,16);
 // cycle all offers
 foreach($offers_array as $offer_fobj){
  // make table row class
  $tr_class_array=array();
  if($offer_fobj->id==$_REQUEST['idOffer']){$tr_class_array[]="currentrow";}
  if($offer_fobj->important){$tr_class_array[]="success";}
  if($offer_fobj->deleted){$tr_class_array[]="deleted";}
  // make confirmation
  if($offer_fobj->salesorder_number){$confirmation_dd=api_tag("samp",ltrim($offer_fobj->salesorder_number,"0")."-".ltrim($offer_fobj->salesorder_position,"0"));}else{$confirmation_dd=null;}
  // make offer row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_view&idCustomer=".$offer_fobj->fkCustomer."&idOffer=".$offer_fobj->id,"fa-search",api_text("offers_list-td-view"),null,null,null,null,"_blank");
  $table->addRowField(api_tag("samp",$offer_fobj->id),"nowrap");
  $table->addRowField($offer_fobj->customer_name,"truncate-ellipsis");
  $table->addRowField($offer_fobj->request_number." - ".api_date_format($offer_fobj->request_date,api_text("date")),"nowrap");
  $table->addRowField($offer_fobj->grade,"nowrap");
  $table->addRowField($offer_fobj->execution,"nowrap");
  $table->addRowField($offer_fobj->profile,"nowrap");
  $table->addRowField($offer_fobj->size,"nowrap text-right");
  $table->addRowField(($offer_fobj->length_cutted?$offer_fobj->length_cutted:$offer_fobj->length),"nowrap text-right");
  $table->addRowField(api_number_format($offer_fobj->quantity_pieces,0,null,false,false,"-"),"nowrap text-right");
  $table->addRowField(api_number_format($offer_fobj->quantity_weight,2,null,false,true),"nowrap text-right");
  $table->addRowField(api_number_format($offer_fobj->price_list,2,"&euro;",true,false,"-"),"nowrap text-right");
  $table->addRowField(api_number_format($offer_fobj->price_offer,2,"&euro;",true),"nowrap text-right");
  $table->addRowField($confirmation_dd,"nowrap");
  $table->addRowField((new cUser($offer_fobj->addFkUser))->lastname,"nowrap text-right");
  $table->addRowField((new cCrmDivision($offer_fobj->fkDivision))->name,"nowrap text-right");
  $table->addRowField($offer_fobj->getStatus(true,false),"nowrap");
  $table->addRowField($offer_fobj->getIssue(true,false),"nowrap");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");

?>