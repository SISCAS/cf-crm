<?php
/**
 * CRM - Parameters
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("parameters"));
 // build parameters form
 $parameters_form=new strParametersForm();
 // division
 $parameters_form->addParameter("division",api_text("parameters-ff-division"),api_text("parameters-ff-division-select"));
 foreach(api_crm_availableDivisions(true) as $division_fobj){$parameters_form->addParameterOption($division_fobj->id,$division_fobj->label);}
 // salesarea
 $parameters_form->addParameter("salesarea",api_text("parameters-ff-salesarea"),api_text("parameters-ff-salesarea-select"));
 foreach(api_crm_availableDivisions(true) as $division_fobj){foreach($division_fobj->getSalesareas() as $salesarea_fobj){$parameters_form->addParameterOption($salesarea_fobj->id,$division_fobj->name." &rarr; ".$salesarea_fobj->label);}}
 // salesteam
 $parameters_form->addParameter("salesteam",api_text("parameters-ff-salesteam"),api_text("parameters-ff-salesteam-select"));
 foreach(api_crm_rfc_codings("SALES_TEAM") as $salesteam_fobj){$parameters_form->addParameterOption($salesteam_fobj->id,strtoupper($salesteam_fobj->label));}
 // supplier
 $parameters_form->addParameter("supplier",api_text("parameters-ff-supplier"),api_text("parameters-ff-supplier-select"));
 foreach(api_crm_rfc_supplier_search(null,"FO01") as $supplier_fobj){$parameters_form->addParameterOption($supplier_fobj->code,$supplier_fobj->name." [".ltrim($supplier_fobj->code,0)."]");}
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($parameters_form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($division_obj,"division");
