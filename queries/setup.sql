--
-- Setup module CRM
--
-- Version 1.0.0
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `crm__divisions`
--

CREATE TABLE IF NOT EXISTS `crm__divisions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `fiscal_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fiscal_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fiscal_code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fiscal_vat` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__divisions__authorizations`
--

CREATE TABLE IF NOT EXISTS `crm__divisions__authorizations` (
  `fkDivision` int(11) unsigned NOT NULL,
  `fkGroup` int(11) unsigned NOT NULL,
  `level` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`fkDivision`,`fkGroup`),
  KEY `fkGroup` (`fkGroup`),
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__divisions__authorizations_ibfk_1` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crm__divisions__authorizations_ibfk_2` FOREIGN KEY (`fkGroup`) REFERENCES `framework__groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__divisions__salesareas`
--

CREATE TABLE IF NOT EXISTS `crm__divisions__salesareas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkDivision` int(11) unsigned NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderTypology` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `commercialOrganization` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `distributionChannel` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `productSector` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `salesOffice` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__divisions__salesareas_ibfk_1` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__divisions__warehouses`
--

CREATE TABLE IF NOT EXISTS `crm__divisions__warehouses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkDivision` int(11) unsigned NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codes_json` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__divisions__warehouses_ibfk_1` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__divisions__events`
--

CREATE TABLE IF NOT EXISTS `crm__divisions__events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkDivision` int(11) unsigned NOT NULL,
  `fkUser` int(11) unsigned DEFAULT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__divisions__events_ibfk_1` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers`
--

CREATE TABLE IF NOT EXISTS `crm__customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fiscalName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__offers`
--

CREATE TABLE IF NOT EXISTS `crm__customers__offers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkCustomer` int(11) unsigned NOT NULL,
  `fkDivision` int(11) unsigned NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `request_number` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `request_date` date NOT NULL,
  `request_grade` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `material` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `execution` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `size` double unsigned NOT NULL,
  `length` double unsigned DEFAULT NULL,
  `quantity_pieces` double unsigned DEFAULT NULL,
  `quantity_weight` double unsigned NOT NULL,
  `price_list` double unsigned NOT NULL,
  `price_offer` double unsigned NOT NULL,
  `price_alloy` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `price_cutting` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `availabilityDate` date DEFAULT NULL,
  `expirationDate` date DEFAULT NULL,
  `salesorder_number` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salesorder_position` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `status` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkCompany` (`fkCustomer`),
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__customers__offers_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crm__customers__offers_ibfk_2` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=100000001;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__relationships`
--

CREATE TABLE IF NOT EXISTS `crm__customers__relationships` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkCustomer` int(11) unsigned NOT NULL,
  `fkDivision` int(11) unsigned NOT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `typology` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `feeling` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkCompany` (`fkCustomer`),
  CONSTRAINT `crm__customers__relationships_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `crm__customers__specifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkCustomer` int(11) unsigned NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `compatibility` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `addTimestamp` int(11) unsigned NOT NULL,
  `addFkUser` int(11) unsigned NOT NULL,
  `updTimestamp` int(11) unsigned DEFAULT NULL,
  `updFkUser` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkCompany` (`fkCustomer`),
  CONSTRAINT `crm__customers__specifications_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__specifications__attachments`
--

CREATE TABLE IF NOT EXISTS `crm__customers__specifications__attachments` (
  `fkSpecification` int(11) unsigned NOT NULL,
  `fkAttachment` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fkSpecification`,`fkAttachment`),
  KEY `fkSpecification` (`fkSpecification`),
  KEY `fkAttachment` (`fkAttachment`),
  CONSTRAINT `crm__customers__specifications__attachments_ibfk_1` FOREIGN KEY (`fkSpecification`) REFERENCES `crm__customers__specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `crm__customers__specifications__attachments_ibfk_2` FOREIGN KEY (`fkAttachment`) REFERENCES `framework__attachments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__events`
--

CREATE TABLE IF NOT EXISTS `crm__customers__events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fkCustomer` int(11) unsigned NOT NULL,
  `fkUser` int(11) unsigned DEFAULT NULL,
  `timestamp` int(11) unsigned NOT NULL,
  `level` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `event` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkCustomer` (`fkCustomer`),
  CONSTRAINT `crm__customers__events_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Authorizations
--

INSERT IGNORE INTO `framework__modules__authorizations` (`id`,`fkModule`,`order`) VALUES
('crm-manage','crm',1),
('crm-customers_view','crm',2),
('crm-customers_manage','crm',3);

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
