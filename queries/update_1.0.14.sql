--
-- CRM - Update (1.0.14)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `crm__customers__offers`
--

ALTER TABLE `crm__customers__offers` ADD `fkSalesarea` INT(11) UNSIGNED NOT NULL AFTER `fkDivision`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
