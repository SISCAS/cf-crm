--
-- CRM - Update (1.0.15)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `crm__customers__offers`
--

ALTER TABLE `crm__customers__offers` ADD INDEX(`fkSalesarea`);
ALTER TABLE `crm__customers__offers` ADD  FOREIGN KEY (`fkSalesarea`) REFERENCES `crm__divisions__salesareas`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
