--
-- CRM - Update (1.0.22)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `crm__customers__offers`
--                                                                                                     availabilityDate   modificare ovunque il campo da date a text

ALTER TABLE `crm__customers__offers` CHANGE `availabilityDate` `availability` VARCHAR(16) NULL DEFAULT NULL;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
