--
-- CRM - Update (1.0.35)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `crm__customers__offers`
--

ALTER TABLE `crm__customers__offers` CHANGE `length` `length_cutted` DOUBLE UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `crm__customers__offers` ADD `length` VARCHAR(16) NOT NULL AFTER `size`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
