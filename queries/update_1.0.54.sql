--
-- CRM - Update (1.0.54)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__attachments`
--

CREATE TABLE IF NOT EXISTS `crm__customers__attachments` (
	`fkCustomer` int(11) unsigned NOT NULL,
	`fkAttachment` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY (`fkCustomer`,`fkAttachment`),
	KEY `fkCustomer` (`fkCustomer`),
	KEY `fkAttachment` (`fkAttachment`),
	CONSTRAINT `crm__customers__attachments_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `crm__customers__attachments_ibfk_2` FOREIGN KEY (`fkAttachment`) REFERENCES `framework__attachments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
