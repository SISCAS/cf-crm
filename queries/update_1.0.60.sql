--
-- CRM - Update (1.0.60)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table structure for table `crm__customers__offers`
--

ALTER TABLE `crm__customers__offers`
  ADD `cnfTimestamp` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `updFkUser`,
  ADD `cnfFkUser` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `cnfTimestamp`
;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
