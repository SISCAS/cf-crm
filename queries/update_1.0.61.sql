--
-- CRM - Update (1.0.61)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__products`
--

CREATE TABLE IF NOT EXISTS `crm__customers__products` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`fkCustomer` int(11) unsigned NOT NULL,
	`fkDivision` int(11) unsigned NOT NULL,
	`grade` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
	`execution` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
	`profile` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
	`size` double unsigned NOT NULL,
	`quantity` int(10) unsigned NOT NULL,
	`addTimestamp` int(11) unsigned NOT NULL,
	`addFkUser` int(11) unsigned NOT NULL,
	`updTimestamp` int(11) unsigned DEFAULT NULL,
	`updFkUser` int(11) unsigned DEFAULT NULL,
	`deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	KEY `fkCustomer` (`fkCustomer`),
	KEY `fkDivision` (`fkDivision`),
	CONSTRAINT `crm__customers__products_ibfk_2` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `crm__customers__products_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
