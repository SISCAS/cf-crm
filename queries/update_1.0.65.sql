--
-- CRM - Update (1.0.65)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

CREATE TABLE `crm__customers__tags` (
	`fkCustomer` INT(11) UNSIGNED NOT NULL,
	`tag` VARCHAR(128) NOT NULL COLLATE 'utf8_unicode_ci',
	INDEX `fkCustomer` (`fkCustomer`) USING BTREE,
	CONSTRAINT `crm__customers__tags_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) COLLATE='utf8_unicode_ci' ENGINE=InnoDB;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
