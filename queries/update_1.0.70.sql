--
-- CRM - Update (1.0.70)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

ALTER TABLE `crm__customers__contacts` ADD COLUMN `acquisition` DATE NULL AFTER `note`;

UPDATE `crm__customers__contacts` SET acquisition=DATE_FORMAT(FROM_UNIXTIME(addTimestamp),'%Y-%m-%d');

ALTER TABLE `crm__customers__contacts` CHANGE COLUMN `acquisition` `acquisition` DATE NOT NULL AFTER `note`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
