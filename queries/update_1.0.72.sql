--
-- CRM - Update (1.0.72)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

ALTER TABLE `crm__customers` ADD COLUMN `potentialQuantity` INT(11) UNSIGNED NULL DEFAULT NULL AFTER `classification`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
