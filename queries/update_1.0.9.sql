--
-- CRM - Update (1.0.9)
--
-- @package Coordinator\Modules\CRM
-- @company Cogne Acciai Speciali s.p.a
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

--
-- Alter table `crm__customers`
--

ALTER TABLE `crm__customers` ADD `classification` VARCHAR(1) NULL AFTER `longitude`;

-- --------------------------------------------------------

--
-- Alter table `crm__customers__offers`
--

ALTER TABLE `crm__customers__offers` ADD `issue` VARCHAR(32) NULL DEFAULT NULL AFTER `status`;

-- --------------------------------------------------------

--
-- Alter table `crm__customers__relationships`
--

ALTER TABLE `crm__customers__relationships` ADD `contacts` VARCHAR(255) NULL AFTER `typology`;

-- --------------------------------------------------------

--
-- Alter table `crm__customers__relationships`
--

ALTER TABLE `crm__customers__offers` ADD `heattreath` VARCHAR(8) NULL AFTER `profile`;
ALTER TABLE `crm__customers__offers` ADD `tollerance` VARCHAR(8) NULL AFTER `length`;

-- --------------------------------------------------------

--
-- Table structure for table `crm__customers__contacts`
--

CREATE TABLE IF NOT EXISTS `crm__customers__contacts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fkCustomer` int(11) UNSIGNED NOT NULL,
  `fkDivision` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `addTimestamp` int(11) UNSIGNED NOT NULL,
  `addFkUser` int(11) UNSIGNED NOT NULL,
  `updTimestamp` int(11) UNSIGNED DEFAULT NULL,
  `updFkUser` int(11) UNSIGNED DEFAULT NULL,
  `deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fkCustomer` (`fkCustomer`) USING BTREE,
  KEY `fkDivision` (`fkDivision`),
  CONSTRAINT `crm__customers__contacts_ibfk_1` FOREIGN KEY (`fkCustomer`) REFERENCES `crm__customers` (`id`),
  CONSTRAINT `crm__customers__contacts_ibfk_2` FOREIGN KEY (`fkDivision`) REFERENCES `crm__divisions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
