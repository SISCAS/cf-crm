<?php
/**
 * CRM - Specifications List
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

 // check authorization
 api_checkAuthorization("crm-customers_view","dashboard");
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("specifications_list"));
 // definitions
 $specifications_array=array();
 $customers_array=array();
 // get customers
 $customers_results=$GLOBALS['database']->queryObjects("SELECT `id`,`code`,`name` FROM `crm__customers` ORDER BY `name`");
 foreach($customers_results as $result_f){$customers_array[$result_f->id]=$result_f->code." ".$result_f->name;}
 // build filter
 $filter=new strFilter();
 $filter->addSearch(array("id","code","description","note"));
 $filter->addItem(api_text("specifications_list-filter-customer"),$customers_array,"fkCustomer",null,"customer");
 // build query object
 $query=new cQuery("crm__customers__specifications",$filter->getQueryWhere());
 $query->addQueryOrderField("id","DESC");
 // build pagination object
 $pagination=new strPagination($query->getRecordsCount());
 // cycle all results
 foreach($query->getRecords($pagination->getQueryLimits()) as $result_f){$specifications_array[$result_f->id]=new cCrmSpecification($result_f);}
 // build table
 $table=new strTable(api_text("specifications_list-tr-unvalued"));
 $table->addHeader($filter->link(api_icon("fa-filter",api_text("filters-modal-link"),"hidden-link")),"text-center",16);
 $table->addHeader(api_text("specifications_list-th-customer"),"nowrap");
 $table->addHeader(api_text("specifications_list-th-code"),"nowrap");
 $table->addHeader(api_text("specifications_list-th-date"),"nowrap");
 $table->addHeader(api_text("specifications_list-th-description"),"nowrap");
 $table->addHeader(api_text("specifications_list-th-note"),null,"100%");
 $table->addHeader("&nbsp;",null,16);
 // cycle all specifications
 foreach($specifications_array as $specification_fobj){
  // make table row class
  $tr_class_array=array();
  if($specification_fobj->id==$_REQUEST['idSpecification']){$tr_class_array[]="currentrow";}
  if($specification_fobj->deleted){$tr_class_array[]="deleted";}
  // make specification row
  $table->addRow(implode(" ",$tr_class_array));
  $table->addRowFieldAction("?mod=".MODULE."&scr=customers_view&tab=specifications&act=specification_view&idCustomer=".$specification_fobj->fkCustomer."&idSpecification=".$specification_fobj->id,"fa-search",api_text("specifications_list-td-view"),null,null,null,null,"_blank");
  $table->addRowField((new cCrmCustomer($specification_fobj->fkCustomer))->name,"nowrap");
  $table->addRowField(api_tag("samp",$specification_fobj->code),"nowrap");
  $table->addRowField(api_date_format($specification_fobj->date,api_text("date")),"nowrap");
  $table->addRowField($specification_fobj->description,"nowrap");
  $table->addRowField($specification_fobj->note,"truncate-ellipsis");
  $table->addRowField($specification_fobj->getCompatibility(true,false),"nowrap");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($filter->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($table->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($pagination->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($query,"query");

?>