<?php
/**
 * CRM - Submit
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 */

// debug
api_dump($_REQUEST,"_REQUEST");
// check for actions
if(!defined('ACTION')){die("ERROR EXECUTING SCRIPT: The action was not defined");}
// switch action
switch(ACTION){
  // divisions
  case "division_save":division_save();break;
  case "division_delete":division_delete(true);break;
  case "division_undelete":division_delete(false);break;
  case "division_remove":division_remove();break;
  case "division_group_add":division_group_add();break;
  case "division_group_remove":division_group_remove();break;
  case "division_salesarea_save":division_salesarea_save();break;
  case "division_salesarea_remove":division_salesarea_remove();break;
  case "division_warehouse_save":division_warehouse_save();break;
  case "division_warehouse_remove":division_warehouse_remove();break;
  // customers
  case "customer_save":customer_save();break;
  case "customer_delete":customer_delete(true);break;
  case "customer_undelete":customer_delete(false);break;
  case "customer_remove":customer_remove();break;
  case "customer_contact_save":customer_contact_save();break;
  case "customer_contact_remove":customer_contact_remove();break;
  case "customer_relationship_save":customer_relationship_save();break;
  case "customer_relationship_remove":customer_relationship_remove();break;
  case "customer_attachment_save":customer_attachment_save();break;
  case "customer_attachment_remove":customer_attachment_remove();break;
  case "customer_specification_save":customer_specification_save();break;
  case "customer_specification_remove":customer_specification_remove();break;
  case "customer_specification_attachment_remove":customer_specification_attachment_remove();break;
  case "customer_product_save":customer_product_save();break;
  case "customer_product_remove":customer_product_remove();break;
  case "customer_offer_save":customer_offer_save();break;
  case "customer_offer_remove":customer_offer_remove();break;
  case "customer_offer_order":customer_offer_order();break;
  case "customer_offer_issue":customer_offer_issue();break;
  case "customer_offer_customer":customer_offer_customer();break;
  // default
  default:
    api_alerts_add(api_text("alert_submitFunctionNotFound",array(MODULE,SCRIPT,ACTION)),"danger");
    api_redirect("?mod=".MODULE);
}

/**
 * Division Save
 */
function division_save(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get division object
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  api_dump($division_obj,"division object");
  // build division query object
  $division_qobj=new stdClass();
  $division_qobj->id=$division_obj->id;
  $division_qobj->name=addslashes($_REQUEST['name']);
  $division_qobj->description=addslashes($_REQUEST['description']);
  $division_qobj->company=addslashes($_REQUEST['company']);
  $division_qobj->fiscal_name=addslashes($_REQUEST['fiscal_name']);
  $division_qobj->fiscal_address=addslashes($_REQUEST['fiscal_address']);
  $division_qobj->fiscal_code=addslashes($_REQUEST['fiscal_code']);
  $division_qobj->fiscal_vat=addslashes($_REQUEST['fiscal_vat']);
  // check division
  if($division_obj->id){
    // update division
    $division_qobj->updTimestamp=time();
    $division_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($division_qobj,"division query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__divisions",$division_qobj);
    // log event
    api_crm_divisions_event_save($division_qobj,"divisionUpdated");
    // alert
    api_alerts_add(api_text("crm_alert-divisionUpdated"),"success");
  }else{
    // insert division
    $division_qobj->addTimestamp=time();
    $division_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($division_qobj,"division query object");
    // execute query
    $division_qobj->id=$GLOBALS['database']->queryInsert("crm__divisions",$division_qobj);
    // log event
    api_crm_divisions_event_save($division_qobj,"divisionCreated");
    // alert
    api_alerts_add(api_text("crm_alert-divisionCreated"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=".api_return_script("divisions_view")."&idDivision=".$division_qobj->id);
}

/**
 * Division Deleted
 *
 * @param boolean $deleted Deleted or Undeleted
 */
function division_delete($deleted){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get division object
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  api_dump($division_obj,"division object");
  // check object
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  // build division query objects
  $division_qobj=new stdClass();
  $division_qobj->id=$division_obj->id;
  $division_qobj->deleted=($deleted?1:0);
  $division_qobj->updTimestamp=time();
  $division_qobj->updFkUser=$GLOBALS['session']->user->id;
  api_dump($division_qobj,"division query object");
  // update division
  $GLOBALS['database']->queryUpdate("crm__divisions",$division_qobj);
  // make action
  if($deleted){$action="divisionDeleted";}else{$action="divisionUndeleted";}
  // log event
  api_crm_divisions_event_save($division_obj,$action,"warning");
  // redirect
  api_alerts_add(api_text("crm_alert-".$action),"warning");
  api_redirect("?mod=".MODULE."&scr=divisions_list&idDivision=".$division_obj->id);
}

/**
 * Division Remove
 */
function division_remove(){
  // disable function
  die("SUBMIT FUNCTION DISABLED");
  /*
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get division object
  $division_obj=new cCrmDivisionsDivision($_REQUEST['idDivision']);
  api_dump($division_obj,"division object");
  // check object
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_division");}
  // remove division
  $GLOBALS['database']->queryDelete("crm__divisions",$division_obj->id);
  // @tip save event in framework
  // redirect
  api_alerts_add(api_text("crm_alert-divisionRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=divisions_division");
  */
}

/**
 * Division Group Add
 */
function division_group_add(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get division object
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  api_dump($division_obj,"division object");
  // get group object
  $group_obj=new cGroup($_REQUEST['fkGroup']);
  api_dump($group_obj,"group object");
  // check objects
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  if(!$group_obj->id){api_alerts_add(api_text("framework_alert_groupNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_view&idDivision=".$division_obj->id);}
  // remove previous group authorization if exist
  $GLOBALS['database']->queryExecute("DELETE FROM `crm__divisions__authorizations` WHERE `fkDivision`='".$division_obj->id."' AND `fkGroup`='".$group_obj->id."'");
  // build group query object
  $authorized_group_qobj=new stdClass();
  $authorized_group_qobj->fkDivision=$division_obj->id;
  $authorized_group_qobj->fkGroup=$group_obj->id;
  $authorized_group_qobj->level=$_REQUEST['level'];
  // debug
  api_dump($authorized_group_qobj,"authorized group query object");
  // execute query
  $GLOBALS['database']->queryInsert("crm__divisions__authorizations",$authorized_group_qobj);
  // log event
  api_crm_divisions_event_save($division_obj,"divisionGroupAdded","information",$group_obj->name);
  // alert and redirect
  api_alerts_add(api_text("crm_alert-divisionGroupAdded"),"success");
  api_redirect("?mod=".MODULE."&scr=".api_return_script("divisions_view")."&idDivision=".$division_obj->id);
}

/**
 * Division Group Remove
 */
function division_group_remove(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get objects
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  $group_obj=new cGroup($_REQUEST['idGroup']);
  // debug
  api_dump($division_obj,"division object");
  api_dump($group_obj,"group object");
  // check object
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  if(!$group_obj->id){api_alerts_add(api_text("framework_alert_groupNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_view&idDivision=".$division_obj->id);}
  // remove group
  $GLOBALS['database']->queryExecute("DELETE FROM `crm__divisions__authorizations` WHERE `fkDivision`='".$division_obj->id."' AND `fkGroup`='".$group_obj->id."'");
  // log event
  api_crm_divisions_event_save($division_obj,"divisionGroupRemoved","warning",$group_obj->name);
  // redirect
  api_alerts_add(api_text("crm_alert-divisionGroupRemoved",$group_obj->name),"warning");
  api_redirect("?mod=".MODULE."&scr=divisions_view&idDivision=".$division_obj->id);
}

/**
 * Division Salesarea Save
 */
function division_salesarea_save(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get objects
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  $salesarea_obj=new cCrmSalesarea($_REQUEST['idSalesarea']);
  // check
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  // debug
  api_dump($division_obj,"division object");
  api_dump($salesarea_obj,"salesarea object");
  // build division salesarea query object
  $salesarea_qobj=new stdClass();
  $salesarea_qobj->id=$salesarea_obj->id;
  $salesarea_qobj->fkDivision=$division_obj->id;
  $salesarea_qobj->name=addslashes($_REQUEST['name']);
  $salesarea_qobj->description=addslashes($_REQUEST['description']);
  $salesarea_qobj->orderTypology=addslashes($_REQUEST['orderTypology']);
  $salesarea_qobj->commercialOrganization=addslashes($_REQUEST['commercialOrganization']);
  $salesarea_qobj->distributionChannel=addslashes($_REQUEST['distributionChannel']);
  $salesarea_qobj->productSector=addslashes($_REQUEST['productSector']);
  $salesarea_qobj->salesOffice=addslashes($_REQUEST['salesOffice']);
  // check salesarea
  if($salesarea_qobj->id){
    // update salesarea
    $salesarea_qobj->updTimestamp=time();
    $salesarea_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($salesarea_qobj,"salesarea query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__divisions__salesareas",$salesarea_qobj);
    // log event
    api_crm_divisions_event_save($division_obj,"divisionSalesareaUpdated","information",$salesarea_qobj->name);
    // alert
    api_alerts_add(api_text("crm_alert-divisionSalesareaUpdated"),"success");
  }else{
    // insert salesarea
    $salesarea_qobj->addTimestamp=time();
    $salesarea_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($salesarea_qobj,"salesarea query object");
    // execute query
    $salesarea_qobj->id=$GLOBALS['database']->queryInsert("crm__divisions__salesareas",$salesarea_qobj);
    // log event
    api_crm_divisions_event_save($division_obj,"divisionSalesareaAdded","information",$salesarea_qobj->name);
    // alert
    api_alerts_add(api_text("crm_alert-divisionSalesareaAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=divisions_view&tab=salesareas&idDivision=".$division_obj->id."&idSalesarea=".$salesarea_qobj->id);
}

/**
 * Division Salesarea Remove
 */
function division_salesarea_remove(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get objects
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  $salesarea_obj=new cCrmSalesarea($_REQUEST['idSalesarea']);
  // debug
  api_dump($division_obj,"division object");
  api_dump($salesarea_obj,"salesarea object");
  // check object
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  if(!$salesarea_obj->id){api_alerts_add(api_text("crm_alert-divisionSalesareaNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_view&tab=salesareas&idDivision=".$division_obj->id);}
  // remove salesarea
  $GLOBALS['database']->queryDelete("crm__divisions__salesareas",$salesarea_obj->id);
  // log event
  api_crm_divisions_event_save($division_obj,"divisionSalesareaRemoved","warning",$salesarea_obj->name);
  // redirect
  api_alerts_add(api_text("crm_alert-divisionSalesareaRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=divisions_view&tab=salesareas&idDivision=".$division_obj->id);
}

/**
 * Division Warehouse Save
 */
function division_warehouse_save(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get objects
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  $warehouse_obj=new cCrmWarehouse($_REQUEST['idWarehouse']);
  // check
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  // debug
  api_dump($division_obj,"division object");
  api_dump($warehouse_obj,"warehouse object");
  // build division warehouse query object
  $warehouse_qobj=new stdClass();
  $warehouse_qobj->id=$warehouse_obj->id;
  $warehouse_qobj->fkDivision=$division_obj->id;
  $warehouse_qobj->name=addslashes($_REQUEST['name']);
  $warehouse_qobj->description=addslashes($_REQUEST['description']);
  $warehouse_qobj->codes_json=json_encode(explode("\n",str_replace("\r","",addslashes($_REQUEST['codes']))));
  // check warehouse
  if($warehouse_qobj->id){
    // update warehouse
    $warehouse_qobj->updTimestamp=time();
    $warehouse_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($warehouse_qobj,"warehouse query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__divisions__warehouses",$warehouse_qobj);
    // log event
    api_crm_divisions_event_save($division_obj,"divisionWarehouseUpdated","information",$warehouse_qobj->name);
    // alert
    api_alerts_add(api_text("crm_alert-divisionWarehouseUpdated"),"success");
  }else{
    // insert warehouse
    $warehouse_qobj->addTimestamp=time();
    $warehouse_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($warehouse_qobj,"warehouse query object");
    // execute query
    $warehouse_qobj->id=$GLOBALS['database']->queryInsert("crm__divisions__warehouses",$warehouse_qobj);
    // log event
    api_crm_divisions_event_save($division_obj,"divisionWarehouseAdded","information",$warehouse_qobj->name);
    // alert
    api_alerts_add(api_text("crm_alert-divisionWarehouseAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=divisions_view&tab=warehouses&idDivision=".$division_obj->id."&idWarehouse=".$warehouse_qobj->id);
}

/**
 * Division Warehouse Remove
 */
function division_warehouse_remove(){
  // check authorizations
  api_checkAuthorization("crm-manage","dashboard");
  // get objects
  $division_obj=new cCrmDivision($_REQUEST['idDivision']);
  $warehouse_obj=new cCrmWarehouse($_REQUEST['idWarehouse']);
  // debug
  api_dump($division_obj,"division object");
  api_dump($warehouse_obj,"warehouse object");
  // check object
  if(!$division_obj->id){api_alerts_add(api_text("crm_alert-divisionNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_list");}
  if(!$warehouse_obj->id){api_alerts_add(api_text("crm_alert-divisionWarehouseNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=divisions_view&tab=warehouses&idDivision=".$division_obj->id);}
  // remove warehouse
  $GLOBALS['database']->queryDelete("crm__divisions__warehouses",$warehouse_obj->id);
  // log event
  api_crm_divisions_event_save($division_obj,"divisionWarehouseRemoved","warning",$warehouse_obj->name);
  // redirect
  api_alerts_add(api_text("crm_alert-divisionWarehouseRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=divisions_view&tab=warehouses&idDivision=".$division_obj->id);
}

/**
 * Customer Save
 */
function customer_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get customer object
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  api_dump($customer_obj,"customer object");
  // build customer query object
  $customer_qobj=new stdClass();
  $customer_qobj->id=$customer_obj->id;
  $customer_qobj->code=addslashes($_REQUEST['code']);
  $customer_qobj->name=addslashes($_REQUEST['name']);
  $customer_qobj->fiscalName=addslashes($_REQUEST['fiscalName']);
  $customer_qobj->address=addslashes($_REQUEST['address']);
  $customer_qobj->latitude=(double)$_REQUEST['latitude'];
  $customer_qobj->longitude=(double)$_REQUEST['longitude'];
  $customer_qobj->classification=addslashes($_REQUEST['classification']);
  $customer_qobj->potentialQuantity=(int)$_REQUEST['potentialQuantity']*1000;
  $customer_qobj->note=addslashes($_REQUEST['note']);
  // check for code
  if(strlen($customer_qobj->code)!=10){$customer_qobj->code=str_pad($customer_qobj->code,10,"0",STR_PAD_LEFT);}
  // check customer
  if($customer_obj->id){
    // update customer
    $customer_qobj->updTimestamp=time();
    $customer_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($customer_qobj,"customer query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers",$customer_qobj);
    // log event
    api_crm_customers_event_save($customer_qobj,"customerUpdated");
    // alert
    api_alerts_add(api_text("crm_alert-customerUpdated"),"success");
  }else{
    // insert customer
    $customer_qobj->addTimestamp=time();
    $customer_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($customer_qobj,"customer query object");
    // execute query
    $customer_qobj->id=$GLOBALS['database']->queryInsert("crm__customers",$customer_qobj);
    // log event
    api_crm_customers_event_save($customer_qobj,"customerCreated");
    // alert
    api_alerts_add(api_text("crm_alert-customerCreated"),"success");
  }
  // remove all tags
  $GLOBALS['database']->queryExecute("DELETE FROM `crm__customers__tags` WHERE `fkCustomer`='".$customer_obj->id."'");
  // check for new tags
  if(isset($_REQUEST['tags']) && is_array($_REQUEST['tags'])){
    foreach($_REQUEST['tags'] as $tag_f){
      // build tag query object
      $tag_qobj=new stdClass();
      $tag_qobj->fkCustomer=$customer_obj->id;
      $tag_qobj->tag=addslashes($tag_f);
      // execute query
      $GLOBALS['database']->queryInsert("crm__customers__tags",$tag_qobj);
    }
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=".api_return_script("customers_view")."&idCustomer=".$customer_qobj->id);
}

/**
 * Customer Deleted
 *
 * @param boolean $deleted Deleted or Undeleted
 */
function customer_delete($deleted){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get customer object
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  api_dump($customer_obj,"customer object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // build customer query objects
  $customer_qobj=new stdClass();
  $customer_qobj->id=$customer_obj->id;
  $customer_qobj->deleted=($deleted?1:0);
  $customer_qobj->updTimestamp=time();
  $customer_qobj->updFkUser=$GLOBALS['session']->user->id;
  api_dump($customer_qobj,"customer query object");
  // update customer
  $GLOBALS['database']->queryUpdate("crm__customers",$customer_qobj);
  // make action
  if($deleted){$action="customerDeleted";}else{$action="customerUndeleted";}
  // log event
  api_crm_customers_event_save($customer_obj,$action,"warning");
  // redirect
  api_alerts_add(api_text("crm_alert-".$action),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_list&idCustomer=".$customer_obj->id);
}

/**
 * Customer Remove
 */
function customer_remove(){
  // disable function
  die("SUBMIT FUNCTION DISABLED");
  /*
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get customer object
  $customer_obj=new cCrmCustomersCustomer($_REQUEST['idCustomer']);
  api_dump($customer_obj,"customer object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_customer");}
  // remove customer
  $GLOBALS['database']->queryDelete("crm__customers",$customer_obj->id);
  // @tip save event in framework
  // redirect
  api_alerts_add(api_text("crm_alert-customerRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_customer");
  */
}

/**
 * Customer Contact Save
 */
function customer_contact_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $contact_obj=new cCrmContact($_REQUEST['idContact']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($contact_obj,"contact object");
  // build customer contact query object
  $contact_qobj=new stdClass();
  $contact_qobj->id=$contact_obj->id;
  $contact_qobj->fkCustomer=$customer_obj->id;
  $contact_qobj->fkDivision=$_REQUEST['fkDivision'];
  $contact_qobj->firstname=addslashes($_REQUEST['firstname']);
  $contact_qobj->lastname=addslashes($_REQUEST['lastname']);
  $contact_qobj->role=addslashes($_REQUEST['role']);
  $contact_qobj->mail=addslashes($_REQUEST['mail']);
  $contact_qobj->phone=addslashes($_REQUEST['phone']);
  $contact_qobj->note=addslashes($_REQUEST['note']);
  $contact_qobj->acquisition=$_REQUEST['acquisition'];
  // check contact
  if($contact_qobj->id){
    // update contact
    $contact_qobj->updTimestamp=time();
    $contact_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($contact_qobj,"contact query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers__contacts",$contact_qobj);
    // alert
    api_alerts_add(api_text("crm_alert-customerContactUpdated"),"success");
  }else{
    // insert contact
    $contact_qobj->addTimestamp=time();
    $contact_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($contact_qobj,"contact query object");
    // execute query
    $contact_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__contacts",$contact_qobj);
    // alert
    api_alerts_add(api_text("crm_alert-customerContactAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=contacts&idCustomer=".$customer_obj->id."&idContact=".$contact_qobj->id);
}

/**
 * Customer Contact Remove
 */
function customer_contact_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $contact_obj=new cCrmContact($_REQUEST['idContact']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($contact_obj,"contact object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$contact_obj->id){api_alerts_add(api_text("crm_alert-customerContactNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=contacts&idCustomer=".$customer_obj->id);}
  // remove contact
  $GLOBALS['database']->queryDelete("crm__customers__contacts",$contact_obj->id);
  // log event
  api_crm_customers_event_save($customer_obj,"customerContactRemoved","warning");
  // redirect
  api_alerts_add(api_text("crm_alert-customerContactRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=contacts&idCustomer=".$customer_obj->id);
}

/**
 * Customer Relationship Save
 */
function customer_relationship_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $relationship_obj=new cCrmRelationship($_REQUEST['idRelationship']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($relationship_obj,"relationship object");
  // build customer relationship query object
  $relationship_qobj=new stdClass();
  $relationship_qobj->id=$relationship_obj->id;
  $relationship_qobj->fkCustomer=$customer_obj->id;
  $relationship_qobj->fkDivision=$_REQUEST['fkDivision'];
  $relationship_qobj->timestamp=api_timestamp($_REQUEST['timestamp']);
  $relationship_qobj->typology=addslashes($_REQUEST['typology']);
  $relationship_qobj->contacts=addslashes($_REQUEST['contacts']);
  $relationship_qobj->description=addslashes($_REQUEST['description']);
  $relationship_qobj->feeling=addslashes($_REQUEST['feeling']);
  // check relationship
  if($relationship_qobj->id){
    // update relationship
    $relationship_qobj->updTimestamp=time();
    $relationship_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($relationship_qobj,"relationship query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers__relationships",$relationship_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerRelationshipUpdated","information");
    // alert
    api_alerts_add(api_text("crm_alert-customerRelationshipUpdated"),"success");
  }else{
    // insert relationship
    $relationship_qobj->addTimestamp=time();
    $relationship_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($relationship_qobj,"relationship query object");
    // execute query
    $relationship_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__relationships",$relationship_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerRelationshipAdded","information","{crm_relationship_typology-".$relationship_qobj->typology."}");
    // alert
    api_alerts_add(api_text("crm_alert-customerRelationshipAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=relationships&idCustomer=".$customer_obj->id."&idRelationship=".$relationship_qobj->id);
}

/**
 * Customer Relationship Remove
 */
function customer_relationship_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $relationship_obj=new cCrmRelationship($_REQUEST['idRelationship']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($relationship_obj,"relationship object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$relationship_obj->id){api_alerts_add(api_text("crm_alert-customerRelationshipNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=relationships&idCustomer=".$customer_obj->id);}
  // remove relationship
  $GLOBALS['database']->queryDelete("crm__customers__relationships",$relationship_obj->id);
  // log event
  api_crm_customers_event_save($customer_obj,"customerRelationshipRemoved","warning");
  // redirect
  api_alerts_add(api_text("crm_alert-customerRelationshipRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=relationships&idCustomer=".$customer_obj->id);
}

/**
 * Customer Attachment Save
 */
function customer_attachment_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $attachment_obj=new cAttachment($_REQUEST['idAttachment']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($attachment_obj,"attachment object");
  // acquire variables
  $_description=$_REQUEST["description"];
  // check specification
  if($attachment_obj->id){
    // update attachment
    $attachment_qobj=new stdClass();
    $attachment_qobj->id=$attachment_obj->id;
    $attachment_qobj->description=$_REQUEST["description"];
    $attachment_qobj->updTimestamp=time();
    $attachment_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($attachment_qobj,"attachment query object");
    // execute query
    $GLOBALS['database']->queryUpdate("framework__attachments",$attachment_qobj);
    // alert
    api_alerts_add(api_text("crm_alert-customerAttachmentUpdated"),"success");
  }else{
    // check for file
    if(!intval($_FILES['file']['size'])){api_alerts_add(api_text("crm_alert-customerAttachmentError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id);}
    // check typology
    if(strtolower($_FILES['file']['type'])!="application/pdf"){api_alerts_add(api_text("crm_alert-customerAttachmentError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id);}
    // upload specification file
    $attachment_id=api_attachment_upload($_FILES['file'],null,$_description);
    api_dump($attachment_id);
    // check for upload
    if(!$attachment_id){api_alerts_add(api_text("crm_alert-customerAttachmentError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id);}
    // build attachment query object
    $attachment_qobj=new stdClass();
    $attachment_qobj->fkCustomer=$customer_obj->id;
    $attachment_qobj->fkAttachment=$attachment_id;
    // debug
    api_dump($attachment_qobj,"attachment query object");
    // execute query
    $GLOBALS['database']->queryInsert("crm__customers__attachments",$attachment_qobj);
    // alert
    api_alerts_add(api_text("crm_alert-customerAttachmentAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id."&idAttachment=".($attachment_obj->id?$attachment_obj->id:$attachment_id));
}

/**
 * Customer Attachment Remove
 */
function customer_attachment_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $attachment_obj=new cAttachment($_REQUEST['idAttachment']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($attachment_obj,"attachment object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$attachment_obj->id){api_alerts_add(api_text("crm_alert-customerAttachmentNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id);}
  // remove attachment
  if(api_attachment_remove($attachment_obj)){
    // remove attachment join
    $GLOBALS['database']->queryExecute("DELETE FROM `crm__customers__attachments` WHERE `fkCustomer`='".$customer_obj->id."' AND `fkAttachment`='".$attachment_obj->id."'");
    // alert
    api_alerts_add(api_text("crm_alert-customerAttachmentRemoved"),"warning");
  }else{
    // alert
    api_alerts_add(api_text("crm_alert-customerAttachmentError"),"warning");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=attachments&idCustomer=".$customer_obj->id);
}

/**
 * Customer Specification Save
 */
function customer_specification_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $specification_obj=new cCrmSpecification($_REQUEST['idSpecification']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($specification_obj,"specification object");
  // build customer specification query object
  $specification_qobj=new stdClass();
  $specification_qobj->id=$specification_obj->id;
  $specification_qobj->fkCustomer=$customer_obj->id;
  $specification_qobj->code=addslashes($_REQUEST['code']);
  $specification_qobj->description=addslashes($_REQUEST['description']);
  $specification_qobj->date=addslashes($_REQUEST['date']);
  $specification_qobj->compatibility=addslashes($_REQUEST['compatibility']);
  $specification_qobj->note=addslashes($_REQUEST['note']);
  // check specification
  if($specification_qobj->id){
    // update specification
    $specification_qobj->updTimestamp=time();
    $specification_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($specification_qobj,"specification query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers__specifications",$specification_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerSpecificationUpdated","information",$specification_qobj->code);
    // alert
    api_alerts_add(api_text("crm_alert-customerSpecificationUpdated"),"success");
  }else{
    // insert specification
    $specification_qobj->addTimestamp=time();
    $specification_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($specification_qobj,"specification query object");
    // execute query
    $specification_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__specifications",$specification_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerSpecificationAdded","information",$specification_qobj->code);
    // alert
    api_alerts_add(api_text("crm_alert-customerSpecificationAdded"),"success");
  }
  // check for file
  if(intval($_FILES['file']['size'])>0){
    // check typology
    if(strtolower($_FILES['file']['type'])!="application/pdf"){api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_qobj->id);}
    // upload specification file
    $attachment_id=api_attachment_upload($_FILES['file'],null,"CRM - Customer ".$customer_obj->code." Specification ".$specification_qobj->code);
    api_dump($attachment_id);
    // check for upload
    if(!$attachment_id){api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_qobj->id);}
    // build attachment query object
    $attachment_qobj=new stdClass();
    $attachment_qobj->fkSpecification=$specification_qobj->id;
    $attachment_qobj->fkAttachment=$attachment_id;
    // debug
    api_dump($attachment_qobj,"attachment query object");
    // execute query
    $GLOBALS['database']->queryInsert("crm__customers__specifications__attachments",$attachment_qobj);
    // alert
    api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_qobj->id);
}

/**
 * Customer Specification Remove
 */
function customer_specification_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $specification_obj=new cCrmSpecification($_REQUEST['idSpecification']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($specification_obj,"specification object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$specification_obj->id){api_alerts_add(api_text("crm_alert-customerSpecificationNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id);}
  // cycle and remove attachments
  foreach($specification_obj->getAttachments() as $attachment_fobj){api_attachment_remove($attachment_fobj);}
  // remove attachments join
  $GLOBALS['database']->queryExecute("DELETE FROM `crm__customers__specifications__attachments` WHERE `fkSpecification`='".$specification_obj->id."'");
  // remove specification
  $GLOBALS['database']->queryDelete("crm__customers__specifications",$specification_obj->id);
  // log event
  api_crm_customers_event_save($customer_obj,"customerSpecificationRemoved","warning",$specification_obj->code);
  // redirect
  api_alerts_add(api_text("crm_alert-customerSpecificationRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id);
}

/**
 * Customer Specification Attachment Remove
 */
function customer_specification_attachment_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $specification_obj=new cCrmSpecification($_REQUEST['idSpecification']);
  $attachment_obj=new cAttachment($_REQUEST['idAttachment']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($specification_obj,"specification object");
  api_dump($attachment_obj,"attachment object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$specification_obj->id){api_alerts_add(api_text("crm_alert-customerSpecificationNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id);}
  if(!$attachment_obj->id){api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_obj->id);}
  // remove attachment
  if(api_attachment_remove($attachment_obj)){
    // remove attachment join
    $GLOBALS['database']->queryExecute("DELETE FROM `crm__customers__specifications__attachments` WHERE `fkSpecification`='".$specification_obj->id."' AND `fkAttachment`='".$attachment_obj->id."'");
    // log event
    api_crm_customers_event_save($customer_obj,"customerSpecificationUpdated","information",$specification_obj->code);
    // alert
    api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentRemoved"),"warning");
  }else{
    // alert
    api_alerts_add(api_text("crm_alert-customerSpecificationAttachmentError"),"warning");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=specifications&idCustomer=".$customer_obj->id."&idSpecification=".$specification_obj->id);
}

/**
 * Customer Product Save
 */
function customer_product_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $product_obj=new cCrmProduct($_REQUEST['idProduct']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($product_obj,"product object");
  // build customer product query object
  $product_qobj=new stdClass();
  $product_qobj->id=$product_obj->id;
  $product_qobj->fkCustomer=$customer_obj->id;
  $product_qobj->fkDivision=$_REQUEST['fkDivision'];
  $product_qobj->grade=addslashes($_REQUEST['grade']);
  $product_qobj->execution=addslashes($_REQUEST['execution']);
  $product_qobj->profile=addslashes($_REQUEST['profile']);
  $product_qobj->size=$_REQUEST['size'];
  $product_qobj->quantity=($_REQUEST['quantity']*1000);
  // check product
  if($product_qobj->id){
    // update product
    $product_qobj->updTimestamp=time();
    $product_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($product_qobj,"product query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers__products",$product_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerProductUpdated","information",$product_qobj->grade." ".$product_qobj->execution." ".$product_qobj->profile." ".$product_qobj->size);
    // alert
    api_alerts_add(api_text("crm_alert-customerProductUpdated"),"success");
  }else{
    // insert product
    $product_qobj->addTimestamp=time();
    $product_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($product_qobj,"product query object");
    // execute query
    $product_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__products",$product_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerProductAdded","information",$product_qobj->grade." ".$product_qobj->execution." ".$product_qobj->profile." ".$product_qobj->size);
    // alert
    api_alerts_add(api_text("crm_alert-customerProductAdded"),"success");
  }
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=products&idCustomer=".$customer_obj->id."&idProduct=".$product_qobj->id);
}

/**
 * Customer Product Remove
 */
function customer_product_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $product_obj=new cCrmProduct($_REQUEST['idProduct']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($product_obj,"product object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$product_obj->id){api_alerts_add(api_text("crm_alert-customerProductNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=products&idCustomer=".$customer_obj->id);}
  // remove product
  $GLOBALS['database']->queryDelete("crm__customers__products",$product_obj->id);
  // log event
  api_crm_customers_event_save($customer_obj,"customerProductRemoved","warning",$product_obj->grade." ".$product_obj->execution." ".$product_obj->profile." ".$product_obj->size);
  // redirect
  api_alerts_add(api_text("crm_alert-customerProductRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=products&idCustomer=".$customer_obj->id);
}

/**
 * Customer Offer Save
 */
function customer_offer_save(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $offer_obj=new cCrmOffer($_REQUEST['idOffer']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($offer_obj,"offer object");
  // build customer offer query object
  $offer_qobj=new stdClass();
  $offer_qobj->id=$offer_obj->id;
  $offer_qobj->fkCustomer=$customer_obj->id;
  $offer_qobj->fkDivision=addslashes($_REQUEST['fkDivision']);
  $offer_qobj->fkSalesarea=addslashes($_REQUEST['fkSalesarea']);
  $offer_qobj->customer_name=addslashes($_REQUEST['customer_name']);
  $offer_qobj->request_number=addslashes($_REQUEST['request_number']);
  $offer_qobj->request_date=$_REQUEST['request_date'];
  $offer_qobj->request_grade=addslashes($_REQUEST['request_grade']);
  $offer_qobj->request_material=addslashes($_REQUEST['request_material']);
  $offer_qobj->request_wn=addslashes($_REQUEST['request_wn']);
  $offer_qobj->material=addslashes($_REQUEST['material']);
  $offer_qobj->grade=addslashes($_REQUEST['grade']);
  $offer_qobj->execution=addslashes($_REQUEST['execution']);
  $offer_qobj->profile=addslashes($_REQUEST['profile']);
  $offer_qobj->heattreath=addslashes($_REQUEST['heattreath']);
  $offer_qobj->tollerance=addslashes($_REQUEST['tollerance']);
  $offer_qobj->size=$_REQUEST['size'];
  $offer_qobj->length=addslashes($_REQUEST['length']);
  $offer_qobj->length_cutted=$_REQUEST['length_cutted'];
  $offer_qobj->length_tollerance=addslashes($_REQUEST['length_tollerance']);
  $offer_qobj->quantity_pieces=$_REQUEST['quantity_pieces'];
  $offer_qobj->quantity_weight=$_REQUEST['quantity_weight'];
  $offer_qobj->price_list=$_REQUEST['price_list'];
  $offer_qobj->price_offer=$_REQUEST['price_offer'];
  $offer_qobj->price_alloy=(boolean)$_REQUEST['price_alloy'];
  $offer_qobj->price_cutting=$_REQUEST['price_cutting'];
  $offer_qobj->quantity_pieces=$_REQUEST['quantity_pieces'];
  $offer_qobj->availability=addslashes($_REQUEST['availability']);
  $offer_qobj->expirationDate=$_REQUEST['expirationDate'];
  $offer_qobj->important=(boolean)$_REQUEST['important'];
  $offer_qobj->note=addslashes($_REQUEST['note']);
  $offer_qobj->texts=addslashes($_REQUEST['texts']);
  $offer_qobj->status="inserted";
  // check customer name
  if($customer_obj->code!="9999999999"){$offer_qobj->customer_name=$customer_obj->fiscalName;}
  // check offer
  if($offer_qobj->id){
    // update offer
    $offer_qobj->updTimestamp=time();
    $offer_qobj->updFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($offer_qobj,"offer query object");
    // execute query
    $GLOBALS['database']->queryUpdate("crm__customers__offers",$offer_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerOfferUpdated","information",$offer_qobj->id);
    // alert
    api_alerts_add(api_text("crm_alert-customerOfferUpdated"),"success");
  }else{
    // insert offer
    $offer_qobj->timestamp=time();
    $offer_qobj->addTimestamp=time();
    $offer_qobj->addFkUser=$GLOBALS['session']->user->id;
    // debug
    api_dump($offer_qobj,"offer query object");
    // execute query
    $offer_qobj->id=$GLOBALS['database']->queryInsert("crm__customers__offers",$offer_qobj);
    // log event
    api_crm_customers_event_save($customer_obj,"customerOfferAdded","information",$offer_qobj->id);
    // alert
    api_alerts_add(api_text("crm_alert-customerOfferAdded"),"success");
  }
  // store customer request number and date into cookie (per testare i cookie disattivare il debug perche non li salva se c'è già stato un output)
  if($customer_obj->code=="9999999999"){setcookie("crm-customer_name",$offer_qobj->customer_name,time()+(60*5));}
  setcookie("crm-request_number",$offer_qobj->request_number,time()+(60*5));
  setcookie("crm-request_date",$offer_qobj->request_date,time()+(60*5));
  setcookie("crm-request_material",$offer_qobj->request_material,time()+(60*5));
  setcookie("crm-request_grade",$offer_qobj->request_grade,time()+(60*5));
  // redirect
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_qobj->id);
}

/**
 * Customer Offer Remove
 */
function customer_offer_remove(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $offer_obj=new cCrmOffer($_REQUEST['idOffer']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($offer_obj,"offer object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$offer_obj->id){api_alerts_add(api_text("crm_alert-customerOfferNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);}
  // remove offer
  $GLOBALS['database']->queryDelete("crm__customers__offers",$offer_obj->id);
  // log event
  api_crm_customers_event_save($customer_obj,"customerOfferRemoved","warning",$offer_obj->id);
  // redirect
  api_alerts_add(api_text("crm_alert-customerOfferRemoved"),"warning");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);
}

/**
 * Customer Offer Order
 */
function customer_offer_order(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $offer_obj=new cCrmOffer($_REQUEST['idOffer']);
  // check
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$offer_obj->id){api_alerts_add(api_text("crm_alert-customerOfferNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);}
  if($offer_obj->status=="ordered"){api_alerts_add(api_text("crm_alert-customerOfferError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);}
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($offer_obj,"offer object");
  // acquire variables
  $r_order=$_REQUEST['order'];
  // check for new order
  if($r_order=="new"){
    // acquire and make variables
    $r_salesarea_array=explode("|",$_REQUEST['salesarea']);
    $r_salesteam=$_REQUEST['salesteam'];
    $v_request_number=$_REQUEST['request_number'];
    $v_request_date=api_date_format($_REQUEST['request_date'],"Ymd");
    $v_customer=$customer_obj->code;
    $r_supplier=str_pad($_REQUEST['supplier'],10,"0",STR_PAD_LEFT);
    $r_receiver=str_pad($_REQUEST['receiver'],10,"0",STR_PAD_LEFT);
    $r_billing=str_pad($_REQUEST['billing'],10,"0",STR_PAD_LEFT);
    $r_availability=api_wsrfc_timestampToDate(api_timestamp($_REQUEST['availability']));
    $r_payment=$_REQUEST['payment'];
    $v_currency="EUR";
    // check varaibles
    if(count($r_salesarea_array)!=5){api_alerts_add(api_text("crm_alert-customerOfferError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);}
    // make new order
    $r_order=api_crm_rfc_customer_order_heading($r_salesarea_array,$r_salesteam,$v_request_number,$v_request_date,
                                                $v_customer,$r_receiver,$r_billing,$r_supplier,$r_availability,$r_payment,$v_currency,
                                                (new cUser($offer_obj->addFkUser))->fullname,true);
  }
  // check for order
  if(!$r_order){api_alerts_add(api_text("crm_alert-customerOfferError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);}
  // acquire and make variables
  $r_division=$_REQUEST['division'];
  $r_warehouse=$_REQUEST['warehouse'];
  $v_material=$_REQUEST['material'];
  $v_quantity=number_format($offer_obj->quantity_weight,3,".","");
  $v_unit="KG";
  $v_currency="EUR";
  $v_price=number_format($offer_obj->price_offer,2,".","");
  $v_alloy=$offer_obj->price_alloy;
  $v_cutting=number_format($offer_obj->price_cutting,2,".","");
  $r_availability=api_wsrfc_timestampToDate(api_timestamp($_REQUEST['availability']));
  $r_request_material=$_REQUEST['request_material'];
  $r_note=$_REQUEST['texts'];
  $r_lot=$_REQUEST['lot'];
  $v_size=$offer_obj->size;
  $v_length=($offer_obj->length_cutted?$offer_obj->length_cutted." (".$offer_obj->length_tollerance.")":$offer_obj->length);
  $v_pieces=$offer_obj->quantity_pieces;
  // make new position
  $r_position=api_crm_rfc_customer_order_element( $r_order,$r_division,$r_warehouse,$v_material,$v_quantity,$v_unit,
                                                  $v_currency,$v_price,$v_alloy,$v_cutting,$r_availability,$r_request_material,
                                                  $r_note,$r_lot,$v_size,$v_length,$v_pieces,true );
  // check for position
  if(!$r_position){api_alerts_add(api_text("crm_alert-customerOfferError"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);}
  // build offer query object
  $offer_qobj=new stdClass();
  $offer_qobj->id=$offer_obj->id;
  $offer_qobj->request_number=$_REQUEST['request_number'];
  $offer_qobj->request_date=$_REQUEST['request_date'];
  $offer_qobj->salesorder_number=$r_order;
  $offer_qobj->salesorder_position=$r_position;
  $offer_qobj->status="ordered";
  $offer_qobj->issue="";
  $offer_qobj->updTimestamp=time();
  $offer_qobj->updFkUser=$GLOBALS['session']->user->id;
  $offer_qobj->cnfTimestamp=time();
  $offer_qobj->cnfFkUser=$GLOBALS['session']->user->id;
  // debug
  api_dump($offer_qobj,"offer query object");
  // execute query
  $GLOBALS['database']->queryUpdate("crm__customers__offers",$offer_qobj);
  // log event
  api_crm_customers_event_save($customer_obj,"customerOfferOrdered","information",$offer_qobj->id);
  // alert and redirect
  api_alerts_add(api_text("crm_alert-customerOfferOrdered"),"success");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_qobj->id);
}

/**
 * Customer Offer Issue
 */
function customer_offer_issue(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $offer_obj=new cCrmOffer($_REQUEST['idOffer']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($offer_obj,"offer object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$offer_obj->id){api_alerts_add(api_text("crm_alert-customerOfferNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);}
  // build offer query object
  $offer_qobj=new stdClass();
  $offer_qobj->id=$offer_obj->id;
  $offer_qobj->status="rejected";
  $offer_qobj->issue=$_REQUEST['issue'];
  $offer_qobj->updTimestamp=time();
  $offer_qobj->updFkUser=$GLOBALS['session']->user->id;
  // debug
  api_dump($offer_qobj,"offer query object");
  // execute query
  $GLOBALS['database']->queryUpdate("crm__customers__offers",$offer_qobj);
  // log event
  api_crm_customers_event_save($customer_obj,"customerOfferUpdated","information",$offer_obj->id);
  // redirect
  api_alerts_add(api_text("crm_alert-customerOfferUpdated"),"success");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);
}

/**
 * Customer Offer Customer
 */
function customer_offer_customer(){
  // check authorizations
  api_checkAuthorization("crm-customers_manage","dashboard");
  // get objects
  $customer_obj=new cCrmCustomer($_REQUEST['idCustomer']);
  $offer_obj=new cCrmOffer($_REQUEST['idOffer']);
  $customer_new_obj=new cCrmCustomer($_REQUEST['fkCustomer']);
  // debug
  api_dump($customer_obj,"customer object");
  api_dump($offer_obj,"offer object");
  // check object
  if(!$customer_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_list");}
  if(!$offer_obj->id){api_alerts_add(api_text("crm_alert-customerOfferNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&idCustomer=".$customer_obj->id);}
  if(!$customer_new_obj->id){api_alerts_add(api_text("crm_alert-customerNotFound"),"danger");api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_view&idCustomer=".$customer_obj->id."&idOffer=".$offer_obj->id);}
  // build offer query object
  $offer_qobj=new stdClass();
  $offer_qobj->id=$offer_obj->id;
  $offer_qobj->fkCustomer=$customer_new_obj->id;
  $offer_qobj->customer_name=$customer_new_obj->fiscalName;
  $offer_qobj->updTimestamp=time();
  $offer_qobj->updFkUser=$GLOBALS['session']->user->id;
  // debug
  api_dump($offer_qobj,"offer query object");
  // execute query
  $GLOBALS['database']->queryUpdate("crm__customers__offers",$offer_qobj);
  // log event
  api_crm_customers_event_save($customer_obj,"customerOfferUpdated","information",$offer_obj->id);
  // redirect
  api_alerts_add(api_text("crm_alert-customerOfferUpdated"),"success");
  api_redirect("?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_view&idCustomer=".$customer_new_obj->id."&idOffer=".$offer_obj->id);
}
