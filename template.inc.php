<?php
/**
 * CRM - Template
 *
 * @package Coordinator\Modules\CRM
 * @company Cogne Acciai Speciali s.p.a
 *
 * @var cCrmCustomer $customer_obj
 * @var cCrmDivision $division_obj
 */

// build application
$app=new strApplication();
// build nav object
$nav=new strNav("nav-tabs");
//$nav->setTitle(api_text(MODULE));
// dashboard
$nav->addItem(api_icon("fa-th-large",null,"hidden-link"),"?mod=".MODULE."&scr=dashboard");
// customers
if(substr(SCRIPT,0,9)=="customers"){
	$nav->addItem(api_text("nav-customers-list"),"?mod=".MODULE."&scr=customers_list");
	// operations
	if($customer_obj->id && in_array(SCRIPT,array("customers_view","customers_edit"))){
		$nav->addItem(api_text("nav-operations"),null,null,"active");
		$nav->addSubItem(api_text("nav-customers-operations-edit"),"?mod=".MODULE."&scr=customers_edit&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubSeparator();
		$nav->addSubItem(api_text("nav-customers-operations-credit_check"),"?mod=".MODULE."&scr=customers_view&tab=informations&act=credit_check&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_view")));
		$nav->addSubItem(api_text("nav-customers-operations-contact_add"),"?mod=".MODULE."&scr=customers_view&tab=contacts&act=contact_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-relationship_add"),"?mod=".MODULE."&scr=customers_view&tab=relationships&act=relationship_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-attachment_add"),"?mod=".MODULE."&scr=customers_view&tab=attachments&act=attachment_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-specification_add"),"?mod=".MODULE."&scr=customers_view&tab=specifications&act=specification_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-product_add"),"?mod=".MODULE."&scr=customers_view&tab=products&act=product_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-offer_add"),"?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_add&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
		$nav->addSubItem(api_text("nav-customers-operations-offer_export"),"?mod=".MODULE."&scr=customers_view&tab=offers&act=offer_export&idCustomer=".$customer_obj->id,(api_checkAuthorization("crm-customers_manage")));
	}else{
		$nav->addItem(api_text("nav-customers-add"),"?mod=".MODULE."&scr=customers_edit",(api_checkAuthorization("crm-customers_manage")));
		$nav->addItem(api_text("nav-customers-import"),"?mod=".MODULE."&scr=customers_import",(api_checkAuthorization("crm-customers_manage")));
	}
}
// specifications
if(substr(SCRIPT,0,14)=="specifications"){
	$nav->addItem(api_text("nav-specifications-list"),"?mod=".MODULE."&scr=specifications_list");
}
// offers
if(substr(SCRIPT,0,6)=="offers"){
	$nav->addItem(api_text("nav-offers-list"),"?mod=".MODULE."&scr=offers_list");
}
// check for dashboard
if(in_array(SCRIPT,array("dashboard","parameters"))){
	// personal parameters
	$nav->addItem(api_text("nav-parameters"),"?mod=".MODULE."&scr=parameters");
}
// check for manage authorization
if(api_checkAuthorization("crm-manage")){
	// management
	if(SCRIPT=="dashboard"){
		$nav->addItem(api_text("nav-management"));
		$nav->addSubItem(api_text("nav-management-divisions"),"?mod=".MODULE."&scr=divisions_list");
	}
	// divisions
	if(substr(SCRIPT,0,9)=="divisions"){
		$nav->addItem(api_text("nav-divisions-list"),"?mod=".MODULE."&scr=divisions_list");
		// operations
		if($division_obj->id && in_array(SCRIPT,array("divisions_view","divisions_edit"))){
			$nav->addItem(api_text("nav-operations"),null,null,"active");
			$nav->addSubItem(api_text("nav-divisions-operations-edit"),"?mod=".MODULE."&scr=divisions_edit&idDivision=".$division_obj->id);
			$nav->addSubSeparator();
			$nav->addSubItem(api_text("nav-divisions-operations-group_add"),"?mod=".MODULE."&scr=divisions_view&act=group_add&idDivision=".$division_obj->id);
			$nav->addSubItem(api_text("nav-divisions-operations-salesarea_add"),"?mod=".MODULE."&scr=divisions_view&tab=salesareas&act=salesarea_add&idDivision=".$division_obj->id);
			$nav->addSubItem(api_text("nav-divisions-operations-warehouse_add"),"?mod=".MODULE."&scr=divisions_view&tab=warehouses&act=warehouse_add&idDivision=".$division_obj->id);
		}else{$nav->addItem(api_text("nav-divisions-add"),"?mod=".MODULE."&scr=divisions_edit");}
	}
}
// add nav to html
$app->addContent($nav->render(false));
